import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../reducers/index';

export default () => {
  const sagaMiddleware = createSagaMiddleware();

  const enhancer =
    process.env.NODE_ENV === 'production'
      ? compose(applyMiddleware(sagaMiddleware))
      : compose(applyMiddleware(sagaMiddleware), window.devToolsExtension ? window.devToolsExtension() : f => f);

  return { ...createStore(rootReducer, enhancer), runSaga: sagaMiddleware.run };
};
