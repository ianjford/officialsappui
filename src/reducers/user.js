import * as actions from './../actions/actionConstants.js';
//import findIndex from 'lodash.findindex';

const initialState = {
  userId: '',
  memberId: '',
  userName: '',
  email: '',
  phone: '',
  userType: '',
  umpireLevel: null,
  token: '',
  checkingToken: false,
  isLoggedIn: false,
  loginError: '',
  updatingProfile: false,
  updateProfileError: '',
  updatingPassword: false,
  updatePasswordError: '',
  teamsCoaching: [],
  fetchingCoachTeamsStatus: '',
  teamsCoachingStatus: '',
  teamsVolunteering: [],
  teamsVolunteeringStatus: '',
  isMentor: false,
  isMentee: false,
  mentorInfo: {
    mentorId: null,
    nextGame: {
      gameId: null
    },
    fetchingNextGame: ''
  },
  menteesInfo: {
    mentee1: null,
    mentee2: null,
    mentee3: null,
    mentee4: null,
    mentee5: null,
    nextGame: {
      mentee1: null,
      mentee2: null,
      mentee3: null,
      mentee4: null,
      mentee5: null
    },
    fetchingNextGame: {
      mentee1: '',
      mentee2: '',
      mentee3: '',
      mentee4: '',
      mentee5: ''
    }
  },
  mentorMenteeInfoStatus: ''
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.USER_LOGIN_SUCCESS:
      return {
        ...state,
        userId: action.user.userId,
        memberId: action.user.memberId,
        email: action.user.email,
        userName: action.user.name,
        userType: action.user.userType,
        umpireLevel: action.user.umpireLevel,
        phone: action.user.phoneNumber,
        token: action.token,
        isLoggedIn: true,
        checkingToken: false
      };
    case actions.USER_LOGIN_ERROR:
      return {
        ...state,
        email: '',
        password: '',
        loginError: action.error
      };
    case actions.USER_LOGOUT:
      return {
        ...state,
        userId: '',
        memberId: '',
        userName: '',
        email: '',
        phone: '',
        password: '',
        userType: '',
        umpireLevel: null,
        token: '',
        checkingToken: false,
        isLoggedIn: false,
        loginError: '',
        updatingProfile: false,
        updateProfileError: '',
        updatingPassword: false,
        updatePasswordError: '',
        teamsCoaching: [],
        teamsCoachingStatus: '',
        teamsVolunteering: [],
        teamsVolunteeringStatus: '',
        isMentor: false,
        isMentee: false,
        mentorInfo: {
          mentorId: null,
          nextGame: {
            gameId: null
          },
          fetchingNextGame: ''
        },
        menteesInfo: {
          mentee1: null,
          mentee2: null,
          mentee3: null,
          mentee4: null,
          mentee5: null,
          nextGame: {
            mentee1: null,
            mentee2: null,
            mentee3: null,
            mentee4: null,
            mentee5: null
          },
          fetchingNextGame: {
            mentee1: '',
            mentee2: '',
            mentee3: '',
            mentee4: '',
            mentee5: ''
          }
        },
        mentorMenteeInfoStatus: ''
      };
    case actions.USER_CHECK_JWT:
      return {
        ...state,
        checkingToken: true
      };
    case actions.USER_PROFILE_UPDATE:
      return {
        ...state,
        updatingProfile: true
      };
    case actions.USER_PROFILE_UPDATE_SUCCESS:
      return {
        ...state,
        userName: action.name,
        phone: action.phone
      };
    case actions.USER_PROFILE_UPDATE_ERROR:
      return {
        ...state,
        updatingProfile: false,
        updateProfileError: action.error
      };
    case actions.USER_PASSWORD_CHANGE:
      return {
        ...state,
        updatingPassword: true
      };
    case actions.USER_PASSWORD_CHANGE_SUCCESS:
      return {
        ...state,
        updatingPassword: false,
        updatePasswordError: ''
      };
    case actions.USER_PASSWORD_CHANGE_ERROR:
      return {
        ...state,
        updatingPassword: false,
        updatePasswordError: action.error
      };
    case actions.ADD_COACH_TO_TEAM:
      return {
        ...state,
        teamsCoachingStatus: 'Loading'
      };
    case actions.ADD_COACH_TO_TEAM_SUCCESS:
      const { teamId, teamName, divisionName } = action;
      return {
        ...state,
        teamsCoaching: [...state.teamsCoaching, { teamId, teamName, divisionName }],
        teamsCoachingStatus: 'Loaded'
      };
    case actions.ADD_COACH_TO_TEAM_ERROR:
      return {
        ...state,
        teamsCoachingStatus: 'Error'
      };
    case actions.REMOVE_COACH_FROM_TEAM:
      return {
        ...state,
        teamsCoachingStatus: 'Loading'
      };
    case actions.REMOVE_COACH_FROM_TEAM_SUCCESS:
      return {
        ...state,
        //teamsCoaching: [...state.teamsCoaching, { teamId, teamName, divisionName }],
        teamsCoachingStatus: 'Loaded'
      };
    case actions.REMOVE_COACH_FROM_TEAM_ERROR:
      return {
        ...state,
        teamsCoachingStatus: 'Error'
      };
    case actions.GET_COACHES_TEAMS:
      return {
        ...state,
        fetchingCoachTeamsStatus: 'Loading'
      };
    case actions.GET_COACHES_TEAMS_SUCCESS:
      return {
        ...state,
        teamsCoaching: [...action.teamsList],
        fetchingCoachTeamsStatus: 'Loaded'
      };
    case actions.GET_COACHES_TEAMS_ERROR:
      return {
        ...state,
        teamsCoaching: [...action.teamsList],
        fetchingCoachTeamsStatus: 'Error'
      };
    case actions.GET_MENTOR_MENTEE_INFO:
      return {
        ...state,
        mentorMenteeInfoStatus: 'Loading'
      };
    case actions.GET_MENTOR_MENTEE_INFO_SUCCESS:
      return {
        ...state,
        isMentor: action.mentorMenteeInfo.isMentor,
        isMentee: action.mentorMenteeInfo.isMentee,
        mentorInfo: {
          ...state.mentorInfo,
          mentorId: action.mentorMenteeInfo.mentorId
        },
        menteesInfo: {
          ...state.menteesInfo,
          mentee1: action.mentorMenteeInfo.mentee1,
          mentee2: action.mentorMenteeInfo.mentee2,
          mentee3: action.mentorMenteeInfo.mentee3,
          mentee4: action.mentorMenteeInfo.mentee4,
          mentee5: action.mentorMenteeInfo.mentee5
        },
        mentorMenteeInfoStatus: 'Loaded'
      };
    case actions.GET_MENTOR_MENTEE_INFO_ERROR:
      return {
        ...state,
        mentorMenteeInfoStatus: 'Error'
      };
    case actions.GET_UMPS_NEXT_GAME:
      if (action.umpType === 'mentor') {
        return {
          ...state,
          mentorInfo: {
            ...state.mentorInfo,
            fetchingNextGame: 'Loading'
          }
        };
      } else {
        return {
          ...state,
          menteesInfo: {
            ...state.menteesInfo,
            fetchingNextGame: {
              ...state.menteesInfo.fetchingNextGame,
              [action.umpType]: 'Loading'
            }
          }
        };
      }
    case actions.GET_UMPS_NEXT_GAME_SUCCESS:
      if (action.umpType === 'mentor') {
        return {
          ...state,
          mentorInfo: {
            ...state.mentorInfo,
            nextGame: action.nextGame,
            fetchingNextGame: 'Loaded'
          }
        };
      } else {
        return {
          ...state,
          menteesInfo: {
            ...state.menteesInfo,
            nextGame: {
              ...state.menteesInfo.nextGame,
              [action.umpType]: action.nextGame
            },
            fetchingNextGame: {
              ...state.menteesInfo.fetchingNextGame,
              [action.umpType]: 'Loaded'
            }
          }
        };
      }
    case actions.GET_UMPS_NEXT_GAME_ERROR:
      if (action.umpType === 'mentor') {
        return {
          ...state,
          mentorInfo: {
            ...state.mentorInfo,
            fetchingNextGame: 'Error'
          }
        };
      } else {
        return {
          ...state,
          menteesInfo: {
            ...state.menteesInfo,
            fetchingNextGame: {
              ...state.menteesInfo.fetchingNextGame,
              [action.umpType]: 'Error'
            }
          }
        };
      }
    case actions.GET_TEAMS_VOLUNTEERING:
      return {
        ...state,
        teamsVolunteeringStatus: 'Loading'
      };
    case actions.GET_TEAMS_VOLUNTEERING_SUCCESS:
      return {
        ...state,
        teamsVolunteering: [...action.teamsList],
        teamsVolunteeringStatus: 'Loaded'
      };
    case actions.GET_TEAMS_VOLUNTEERING_ERROR:
      return {
        ...state,
        teamsVolunteeringStatus: 'Error'
      };
    case actions.UPDATE_TEAM_VOLUNTEER:
      return {
        ...state,
        teamsVolunteeringStatus: 'Updating'
      };
    case actions.UPDATE_TEAM_VOLUNTEER_SUCCESS:
      return {
        ...state,
        teamsVolunteering: [...action.teamsList],
        teamsVolunteeringStatus: 'Updated'
      };
    case actions.UPDATE_TEAM_VOLUNTEER_ERROR:
      return {
        ...state,
        teamsVolunteeringStatus: 'Error'
      };
    default:
      return state;
  }
};

export default userReducer;
