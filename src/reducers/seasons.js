import * as actions from './../actions/actionConstants.js';

const initialState = {
  seasonsList: [],
  seasons: {},
  fetchingSeasonsStatus: ''
};

const seasonsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_SEASONS:
      return {
        ...state,
        fetchingSeasonsStatus: 'Loading'
      };
    case actions.GET_SEASONS_SUCCESS:
      return {
        ...state,
        seasonsList: [...action.seasonsList],
        seasons: { ...action.seasons },
        fetchingSeasonsStatus: 'Loaded'
      };
    case actions.GET_SEASONS_ERROR:
      return {
        ...state,
        fetchingSeasonsStatus: 'Error'
      };

    case actions.GET_SEASONS_BY_ACTIVE:
      return {
        ...state,
        fetchingSeasonsStatus: 'Loading'
      };
    case actions.USER_LOGOUT:
      return {
        seasonsList: [],
        seasons: {},
        fetchingSeasonsStatus: ''
      };
    /*case actions.GET_SEASONS_BY_ACTIVE_SUCCESS:
      return {
        ...state,
        seasonsList: [...action.seasonsList],
        seasons: { ...action.seasons },
        fetchingSeasonsStatus: 'Loaded'
      };
    case actions.GET_SEASONS_BY_ACTIVE_ERROR:
      return {
        ...state,
        fetchingSeasonsStatus: 'Error'
      };*/
    default:
      return state;
  }
};

export default seasonsReducer;
