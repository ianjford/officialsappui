import * as actions from './../actions/actionConstants.js';

const initialState = {
  teamsList: [],
  teams: {},
  fetchingTeamsStatus: '',
};

const teamsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_TEAMS:
      return {
        ...state,
        fetchingTeamsStatus: 'Loading',
      };
    case actions.GET_TEAMS_SUCCESS:
      return {
        ...state,
        teamsList: [...action.teamsList],
        teams: { ...action.teams },
        fetchingTeamsStatus: 'Loaded',
      };
    case actions.GET_TEAMS_ERROR:
      return {
        ...state,
        fetchingTeamsStatus: 'Error',
      };
    default:
      return state;
  }
};

export default teamsReducer;
