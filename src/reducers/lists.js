import * as actions from './../actions/actionConstants';
import findIndex from 'lodash.findindex';

const initialState = {
  membersList: [],
  fetchingMembersList: '',
  umpiresList: [],
  fetchingUmpireList: '',
  coachesList: [],
  fetchingCoachesList: '',
  playersList: [],
  fetchingPlayersList: '',
  mentorsList: [],
  mentorsListErrors: {},
  fetchingMentorsList: '',
  dailySummary: [],
  fetchingDailySummary: ''
};

const listsReducer = (state = initialState, action) => {
  let index;
  let updatedMembersList;
  switch (action.type) {
    case actions.GET_DAILY_SUMMARY:
      return {
        ...state,
        fetchingDailySummary: 'Loading'
      };
    case actions.GET_DAILY_SUMMARY_SUCCESS:
      return {
        ...state,
        dailySummary: [...action.dailySummary],
        fetchingDailySummary: 'Loaded'
      };
    case actions.GET_DAILY_SUMMARY_ERROR:
      return {
        ...state,
        fetchingDailySummary: 'Error'
      };
    case actions.GET_MEMBERS_LIST:
      return {
        ...state,
        fetchingMembersList: 'Loading'
      };
    case actions.GET_MEMBERS_LIST_SUCCESS:
      return {
        ...state,
        membersList: [...action.membersList],
        fetchingMembersList: 'Loaded'
      };
    case actions.GET_MEMBERS_LIST_ERROR:
      return {
        ...state,
        fetchingMembersList: 'Error'
      };
    case actions.UPDATE_UMP_SUCCESS:
      index = findIndex(state.membersList, memberObj => memberObj.memberId === action.updatedUmp.memberId);
      let updatedUmp = { memberId: action.memberId, isUmpire: action.updatedUmp.isActive };
      updatedMembersList = [
        ...state.membersList.slice(0, index),
        { ...state.membersList[index], ...updatedUmp },
        ...state.membersList.slice(index + 1)
      ];
      return {
        ...state,
        membersList: updatedMembersList
      };
    case actions.UPDATE_USER_ROLE_SUCCESS:
      index = findIndex(state.membersList, memberObj => memberObj.memberId === action.user.memberId);
      let isAdmin = action.user.userType === 'Admin' ? true : false;
      updatedMembersList = [
        ...state.membersList.slice(0, index),
        { ...state.membersList[index], isAdmin },
        ...state.membersList.slice(index + 1)
      ];
      return {
        ...state,
        membersList: updatedMembersList
      };
    case actions.GET_UMPIRES_LIST:
      return {
        ...state,
        fetchingUmpireList: 'Loading'
      };
    case actions.GET_UMPIRES_LIST_SUCCESS:
      return {
        ...state,
        umpiresList: [...action.umpiresList],
        fetchingUmpireList: 'Loaded'
      };
    case actions.GET_UMPIRES_LIST_ERROR:
      return {
        ...state,
        fetchingUmpireList: 'Error'
      };
    case actions.GET_COACHES_LIST:
      return {
        ...state,
        fetchingCoachesList: 'Loading'
      };
    case actions.GET_COACHES_LIST_SUCCESS:
      return {
        ...state,
        coachesList: [...action.coachesList],
        fetchingCoachesList: 'Loaded'
      };
    case actions.GET_COACHES_LIST_ERROR:
      return {
        ...state,
        fetchingCoachesList: 'Error'
      };
    case actions.GET_PLAYERS_LIST:
      return {
        ...state,
        fetchingPlayersList: 'Loading'
      };
    case actions.GET_PLAYERS_LIST_SUCCESS:
      return {
        ...state,
        playersList: [...action.playersList],
        fetchingPlayersList: 'Loaded'
      };
    case actions.GET_PLAYERS_LIST_ERROR:
      return {
        ...state,
        fetchingPlayersList: 'Error'
      };
    case actions.GET_MENTORS_LIST:
      return {
        ...state,
        fetchingMentorsList: 'Loading'
      };
    case actions.GET_MENTORS_LIST_SUCCESS:
      return {
        ...state,
        mentorsList: [...action.mentorsList],
        fetchingMentorsList: 'Loaded'
      };
    case actions.GET_MENTORS_LIST_ERROR:
      return {
        ...state,
        fetchingMentorsList: 'Error'
      };
    case actions.UPDATE_MENTEES_SUCCESS:
      index = findIndex(state.mentorsList, mentorObj => mentorObj.memberId === action.updatedMentees.memberId);
      let updatedMentorsList = [
        ...state.mentorsList.slice(0, index),
        action.updatedMentees,
        ...state.mentorsList.slice(index + 1)
      ];
      return {
        ...state,
        mentorsList: updatedMentorsList
      };
    case actions.UPDATE_MENTEES_ERROR: {
      return {
        ...state,
        mentorsListErrors: {
          ...state.mentorsListErrors,
          [action.mentorId]: action.error
        }
      };
    }
    default:
      return state;
  }
};

export default listsReducer;
