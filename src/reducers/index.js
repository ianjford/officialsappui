import { combineReducers } from 'redux';
import userReducer from './user';
import gamesReducer from './games';
import umpireReducer from './umpires';
import divisionsReducer from './divisions';
import seasonsReducer from './seasons';
import teamsReducer from './teams';
import listsReducer from './lists';
import errorsReducer from './errors';
import reportReducer from './reports';
import mobileReducer from './mobile';

const rootReducer = combineReducers({
  user: userReducer,
  games: gamesReducer,
  umpires: umpireReducer,
  divisions: divisionsReducer,
  seasons: seasonsReducer,
  teams: teamsReducer,
  lists: listsReducer,
  errors: errorsReducer,
  reports: reportReducer,
  mobile: mobileReducer
});

export default rootReducer;
