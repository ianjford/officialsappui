import * as actions from './../actions/actionConstants.js';

const initialState = {
  divisionsList: [],
  parentDivisionsList: [],
  divisions: {},
  fetchingDivisionsStatus: ''
};

const divisionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_DIVISIONS:
      return {
        ...state,
        fetchingDivisionsStatus: 'Loading'
      };
    case actions.GET_DIVISIONS_SUCCESS:
      return {
        ...state,
        divisionsList: [...action.divisionsList],
        parentDivisionsList: [...action.parentDivisionsList],
        divisions: { ...action.divisions },
        fetchingDivisionsStatus: 'Loaded'
      };
    case actions.GET_DIVISIONS_ERROR:
      return {
        ...state,
        fetchingDivisionsStatus: 'Error'
      };
    case actions.USER_LOGOUT:
      return {
        divisionsList: [],
        parentDivisionsList: [],
        divisions: {},
        fetchingDivisionsStatus: ''
      };
    default:
      return state;
  }
};

export default divisionsReducer;
