import * as actions from './../actions/actionConstants.js';

const initialState = {
  mobile: {}
};

const mobileReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.UPDATE_MOBILE_STATE:
      return{
        ...state
      };
    case actions.UPDATE_MOBILE_STATE_SUCCESS:
      return{
        ...state,
        mobile: {
          ...state.mobile,
          ...action.mobile
        }
      };
    case actions.UPDATE_MOBILE_STATE_ERROR:
      return{
        ...state,
        mobile: {
          error: action.error
        }
      };
    default:
      return state;
  }
};

export default mobileReducer;
