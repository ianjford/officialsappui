import * as actions from './../actions/actionConstants.js';

const initialState = {
  error: null
};

const errorsReducer = (state = initialState, action) => {
	switch(action.type){
		// Umpire actions
		case actions.ASSIGN_UMP_TO_GAME_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.GET_UMP_INFO_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.REMOVE_UMP_FROM_GAME_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.GET_UMPIRES_LIST_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.GET_MENTOR_MENTEE_INFO_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.UPDATE_UMP_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.UPDATE_MENTEES_ERROR:
			return {
				...state,
				error: action.error
			};
		///

		// Games actions
		case actions.GET_GAMES_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.GET_MY_GAMES_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.GET_GAMES_BY_DATE_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.FILTER_GAMES_ERROR:
			return {
				...state,
				error: action.error
			};
		///

		// season actions
		case actions.GET_SEASONS_ERROR:
			return {
				...state,
				error: action.error
			};
		///

		// Division actions
		case actions.GET_DIVISIONS_ERROR:
			return {
				...state,
				error: action.error
			};
		///

		// Coaches actions
		case actions.GET_COACHES_TEAMS_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.GET_COACHES_LIST_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.ADD_COACH_TO_TEAM_ERROR:
			return {
				...state,
				error: action.error
			};
		///

		// User actions
		case actions.USER_PROFILE_UPDATE_ERROR:
			return {
				...state,
				error: action.error
			};
		case actions.USER_PASSWORD_CHANGE_ERROR:
			return {
				...state,
				error: action.error
			};
		///

		// Player action
		case actions.GET_PLAYERS_LIST_ERROR:
			return {
				...state,
				error: action.error
			};
		///

		// Member actions
		case actions.GET_MEMBERS_LIST_ERROR:
			return {
				...state,
				error: action.error
			};
		///
		
		case actions.CLEAR_ERRORS:
			return {
				...state,
				error: null
			}; 
		default: 
			return state;
	}
};

export default errorsReducer;