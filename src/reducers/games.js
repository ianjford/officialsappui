import * as actions from './../actions/actionConstants.js';

const initialState = {
  gamesList: [],
  filteredGamesList: [],
  calendarGamesList: {},
  games: {},
  myGamesList: [],
  fetchingGamesStatus: '',
  fetchingCalGamesStatus: '',
  fetchingFilteredGamesStatus: '',
  fetchingMyGamesStatus: '',
  updateUmpireStatus: {},
  updatedGame: {},
};

const gamesReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_GAMES:
      return {
        ...state,
        fetchingGamesStatus: 'Loading'
      };
    case actions.GET_GAMES_SUCCESS:
      return {
        ...state,
        gamesList: [...action.gamesList],
        games: {
          ...state.games,
          ...action.games
        },
        fetchingGamesStatus: 'Loaded'
      };
    case actions.GET_GAMES_ERROR:
      return {
        ...state,
        fetchingGamesStatus: 'Error'
      };
    case actions.UPDATED_GAMES:
      return {
        ...state,
        fetchingGamesStatus: 'Loading'
      };
    case actions.UPDATED_GAMES_SUCCESS:
      return {
        ...state,
        gamesList: [...action.gamesList],
        games: {
          ...state.games,
          ...action.games
        },
        fetchingGamesStatus: 'Loaded'
      };
    case actions.GET_MY_GAMES:
      return {
        ...state,
        fetchingMyGamesStatus: 'Loading'
      };
    case actions.GET_MY_GAMES_SUCCESS:
      return {
        ...state,
        myGamesList: [...action.myGamesList],
        games: {
          ...state.games,
          ...action.games
        },
        fetchingMyGamesStatus: 'Loaded'
      };
    case actions.GET_MY_GAMES_ERROR:
      return {
        ...state,
        fetchingMyGamesStatus: 'Error'
      };
    case actions.UPDATED_MY_GAMES:
      return {
        ...state,
        fetchingMyGamesStatus: 'Loading'
      };
    case actions.UPDATED_MY_GAMES_SUCCESS:
      return {
        ...state,
        myGamesList: [...action.myGamesList],
        games: {
          ...state.games,
          ...action.games
        },
        fetchingMyGamesStatus: 'Loaded'
      };
    case actions.GET_GAMES_BY_DATE:
      if (state.calendarGamesList[action.startDate] === undefined) {
        return {
          ...state,
          calendarGamesList: {
            [action.startDate]: []
          },
          fetchingCalGamesStatus: 'Loading'
        };
      } else {
        return {
          ...state,
          fetchingCalGamesStatus: 'Loading'
        };
      }
    case actions.GET_GAMES_BY_DATE_SUCCESS:
      return {
        ...state,
        calendarGamesList: {
          ...state.calendarGamesList,
          [action.startDate]: [...state.calendarGamesList[action.startDate], ...action.gamesList]
        },
        games: {
          ...state.games,
          ...action.games
        },
        fetchingCalGamesStatus: 'Loaded'
      };
    case actions.GET_GAMES_BY_DATE_ERROR:
      return {
        ...state,
        fetchingCalGamesStatus: 'Error'
      };
    case actions.ASSIGN_UMP_TO_GAME:
      return {
        ...state,
        updateUmpireStatus: {
          ...state.updateUmpireStatus,
          [action.umpireId]: {
            ...state.updateUmpireStatus[action.umpireId],
            [action.gameId]: {
              [action.plate]: 'Assigning'
            }
          }
        }
      };
    case actions.ASSIGN_UMP_TO_GAME_SUCCESS:
      return {
        ...state,
        games: {
          ...state.games,
          [action.gameId]: { ...state.games[action.gameId], ...action.updatedGame }
        },
        updateUmpireStatus: {
          ...state.updateUmpireStatus,
          [action.umpireId]: {
            ...state.updateUmpireStatus[action.umpireId],
            [action.gameId]: {
              [action.plate]: 'Done'
            }
          }
        }
      };
    case actions.ASSIGN_UMP_TO_GAME_ERROR:
      return {
        ...state,
        games: {
          ...state.games,
          [action.gameId]: { ...state.games[action.gameId], ...action.updatedGame }
        },
        updateUmpireStatus: {
          ...state.updateUmpireStatus,
          [action.umpireId]: {
            ...state.updateUmpireStatus[action.umpireId],
            [action.gameId]: {
              [action.plate]: `Error: ${action.error}`
            }
          }
        }
      };
    case actions.REMOVE_UMP_FROM_GAME:
      return {
        ...state,
        updateUmpireStatus: {
          ...state.updateUmpireStatus,
          [action.umpireId]: {
            ...state.updateUmpireStatus[action.umpireId],
            [action.gameId]: {
              [action.plate]: 'Removing'
            }
          }
        }
      };
    case actions.REMOVE_UMP_FROM_GAME_SUCCESS:
      return {
        ...state,
        games: {
          ...state.games,
          [action.gameId]: { ...state.games[action.gameId], ...action.updatedGame }
        },
        updateUmpireStatus: {
          ...state.updateUmpireStatus,
          [action.umpireId]: {
            ...state.updateUmpireStatus[action.umpireId],
            [action.gameId]: {
              [action.plate]: 'Done'
            }
          }
        }
      };
    case actions.REMOVE_UMP_FROM_GAME_ERROR:
      return {
        ...state,
        games: {
          ...state.games,
          [action.gameId]: { ...state.games[action.gameId], ...action.updatedGame }
        },
        updateUmpireStatus: {
          ...state.updateUmpireStatus,
          [action.umpireId]: {
            ...state.updateUmpireStatus[action.umpireId],
            [action.gameId]: {
              [action.plate]: `Error: ${action.error}`
            }
          }
        }
      };
    case actions.USER_LOGOUT:
      return {
        ...state,
        gamesList: [],
        filteredGamesList: [],
        calendarGamesList: {},
        games: {},
        fetchingGamesStatus: '',
        fetchingCalGamesStatus: '',
        fetchingFilteredGamesStatus: '',
        updateUmpireStatus: {}
      };
    case actions.FILTER_GAMES:
      return {
        ...state,
        filteredGamesList: [],
        fetchingFilteredGamesStatus: 'Loading'
      };
    case actions.FILTER_GAMES_SUCCESS:
      return {
        ...state,
        games: {
          ...state.games,
          ...action.games
        },
        filteredGamesList: [...action.filteredGamesList],
        fetchingFilteredGamesStatus: 'Loaded'
      };
    case actions.FILTER_GAMES_ERROR:
      return {
        ...state,
        fetchingFilteredGamesStatus: 'Error'
      };
    case actions.UPDATE_UMP_AVAILABLE_FOR_GAME:
      return{
        ...state
      };
    case actions.UPDATE_UMP_AVAILABLE_FOR_GAME_SUCCESS:
      return{
        ...state,
        games: {
          ...state.games,
          [action.gameId]: { 
            ...state.games[action.gameId], 
            umpAvailable: action.umpAvailable }
        },
      };
    case actions.UPDATE_UMP_AVAILABLE_FOR_GAME_ERROR:
      return{
        ...state,
        updatedGame: {
          error: action.error
        }
      };
    default:
      return state;
  }
};

export default gamesReducer;
