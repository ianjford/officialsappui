import * as actions from './../actions/actionConstants.js';

const initialState = {
  umpires : {},
  umpiresList: [],
  fetchingAllUmpireDataStatus: '',
  volunteers : {},
  volunteersList: [],
  fetchingAllVolunteerDataStatus: '',
  teams: {},
  teamsList: [],
  fetchingAllTeamDataStatus: '',
  seasons: {},
  seasonsList: [],
  fetchingYtdSummaryDataStatus: '',
  forecasts: {},
  forecastsList: [],
  fetchingWeatherData: '',
  error: ''
};

const reportsReducer = (state = initialState, action) => {
	switch(action.type){
		case actions.GET_ALL_UMPIRE_DATA:
			return{
				...state,
				fetchingAllUmpireDataStatus: 'Loading'
			};
		case actions.GET_ALL_UMPIRE_DATA_SUCCESS:
			return{
				...state,
				umpires: {
		            ...action.umpires		            
		          },
		        umpiresList: [
		        	...action.umpiresList
		        ],
				fetchingAllUmpireDataStatus: 'Loaded'
			};
		case actions.GET_ALL_UMPIRE_DATA_ERROR:
			return{
				...state,
				error: action.error,
				fetchingAllUmpireDataStatus: 'Error'
			};
		case actions.GET_ALL_VOLUNTEER_DATA:
			return{
				...state,
				fetchingAllVolunteerDataStatus: 'Loading'
			};
		case actions.GET_ALL_VOLUNTEER_DATA_SUCCESS:
			return{
				...state,
				volunteers: {
		            ...action.volunteers		            
		          },
		        volunteersList: [
		        	...action.volunteersList
		        ],
				fetchingAllVolunteerDataStatus: 'Loaded'
			};
		case actions.GET_ALL_VOLUNTEER_DATA_ERROR:
			return{
				...state,
				error: action.error,
				fetchingAllVolunteerDataStatus: 'Error'
			};
		case actions.GET_ALL_TEAM_DATA:
			return{
				...state,
				fetchingAllTeamDataStatus: 'Loading'
			};
		case actions.GET_ALL_TEAM_DATA_SUCCESS:
			return{
				...state,
				teams: {
		            ...action.teams		            
		          },
		        teamsList: [
		        	...action.teamsList
		        ],
				fetchingAllTeamDataStatus: 'Loaded'
			};
		case actions.GET_ALL_TEAM_DATA_ERROR:
			return{
				...state,
				error: action.error,
				fetchingAllTeamDataStatus: 'Error'
			};
		case actions.GET_YTD_DATA:
			return{
				...state,
				fetchingYtdSummaryDataStatus: 'Loading'
			};
		case actions.GET_YTD_DATA_SUCCESS:
			return{
				...state,
				seasons: {
					...action.seasons
				},           
		        seasonsList: [
		        	...action.seasonsList
        		],
				fetchingYtdSummaryDataStatus: 'Loaded'
			};
		case actions.GET_YTD_DATA_ERROR:
			return{
				...state,
				error: action.error,
				fetchingYtdSummaryDataStatus: 'Error'
			};
		case actions.GET_DAILY_WEATHER_FORECAST:
			return{
				...state,
				fetchingWeatherData: 'Loading'
			};
		case actions.GET_DAILY_WEATHER_FORECAST_SUCCESS:
			return{
				...state,
				forecasts: {
					...action.forecasts
				},
				forecastsList: [
					...action.forecastsList
				],
				fetchingWeatherData: 'Loaded'
			};
		case actions.GET_DAILY_WEATHER_FORECAST_ERROR:
			return{
				...state,
				error: action.error,
				fetchingWeatherData: 'Error'
			};
		default:
			return state;
	}
};

export default reportsReducer;