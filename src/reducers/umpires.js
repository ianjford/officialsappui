import * as actions from './../actions/actionConstants';

const initialState = {
  umpires: {},
  umpiresList: [],
  fetchingStatus: '',
  errors: {}
};

const umpireReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_UMP_INFO:
      return {
        ...state,
        fetchingStatus: 'Loading'
      };
    case actions.GET_UMP_INFO_SUCCESS:
      return {
        ...state,
        umpires: { ...action.umpires },
        umpiresList: [...action.umpiresList],
        fetchingStatus: 'Loaded'
      };
    case actions.GET_UMP_INFO_ERROR:
      return {
        ...state,
        fetchingStatus: 'Error'
      };
    case actions.GET_UMPIRES_LIST_SUCCESS:
      return {
        ...state,
        umpires: {
          ...state.umpires,
          ...action.umpires
        }
      };
    case actions.UPDATE_UMP_SUCCESS:
      let updatingActive = action.updatedUmp.hasOwnProperty('isActive') ? true : false;
      if (updatingActive) {
        return {
          ...state,
          umpires: {
            ...state.umpires,
            [action.memberId]: {
              ...state.umpires[action.memberId],
              ...action.updatedUmp
            }
          }
        };
      } else {
        return {
          ...state,
          umpires: {
            ...state.umpires,
            [action.memberId]: {
              ...state.umpires[action.memberId],
              [action.seasonId]: {
                ...state.umpires[action.memberId][action.seasonId],
                ...action.updatedUmp
              }
            }
          }
        };
      }
    case actions.UPDATE_UMP_ERROR:
      updatingActive = action.updatedUmp.hasOwnProperty('isActive') ? true : false;
      if (updatingActive) {
        return {
          ...state,
          umpires: {
            ...state.umpires,
            [action.memberId]: {
              ...state.umpires[action.memberId],
              ...action.updatedUmp
            }
          },
          errors: {
          ...state.errors,
          [action.memberId]: action.error
        }
        };
      } else {
        return {
          ...state,
          umpires: {
            ...state.umpires,
            [action.memberId]: {
              ...state.umpires[action.memberId],
              [action.seasonId]: {
                ...state.umpires[action.memberId][action.seasonId],
                ...action.updatedUmp
              }
            }
          },
          errors: {
          ...state.errors,
          [action.memberId]: action.error
        }
        };
      }
    case actions.USER_LOGOUT:
      return {
        umpires: {},
        umpiresList: [],
        fetchingStatus: '',
        errors: {}
      };
    default:
      return state;
  }
};

export default umpireReducer;
