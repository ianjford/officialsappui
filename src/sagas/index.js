import { all, call, fork, put, select, take, takeLatest } from 'redux-saga/effects';
import * as actions from '../actions/actionConstants';
import {
  userLoginSuccess,
  userLogout,
  updateUserProfileSuccess,
  updateUserProfileError,
  updatePasswordSuccess,
  updatePasswordError,
  userRoleUpdateSuccess,
  userRoleUpdateError
} from './../actions/userActions';
import {
  getGamesSuccess,
  getGamesError,
  getUpdatedGamesSuccess,
  getMyGamesSuccess,
  getMyGamesError,
  getUpdatedMyGamesSuccess,
  getGamesByDateSuccess,
  getGamesByDateError,
  filterGamesSuccess,
  filterGamesError,
  getDailySummarySuccess,
  getDailySummaryError,
  updateUmpAvailableForGameSuccess,
  updateUmpAvailableForGameError
} from '../actions/gamesActions';
import {
  getUmpInfoSuccess,
  getUmpInfoError,
  getUmpiresListError,
  getUmpiresListSuccess,
  assignUmpToGameSuccess,
  assignUmpToGameError,
  removeUmpSuccess,
  removeUmpError,
  updateUmpSuccess,
  updateUmpError,
  updateMentorsMenteesSuccess,
  updateMentorsMenteesError,
  getMentorMenteeInfoSuccess,
  getMentorMenteeInfoError,
  getUmpsNextGameSuccess,
  getUmpsNextGameError
} from '../actions/umpireActions';
import { getDivisionsError, getDivisionsSuccess } from '../actions/divisionsActions';
import { getSeasonsError, getSeasonsSuccess } from '../actions/seasonActions';
import {
  getTeamsError,
  getTeamsSuccess,
  getTeamsVolunteeringSuccess,
  getTeamsVolunteeringError,
  updateTeamVolunteerSuccess,
  updateTeamVolunteerError
} from '../actions/teamActions';
import {
  getCoachesListError,
  getCoachesListSuccess,
  getCoachesTeamsSuccess,
  getCoachesTeamsError,
  addCoachToTeamSuccess,
  addCoachToTeamError, 
  removeCoachFromTeamSuccess,
  removeCoachFromTeamError
} from '../actions/coachActions';
import {
  getMembersListError,
  getMembersListSuccess,
  getPlayersListError,
  getPlayersListSuccess,
  getMentorsListError,
  getMentorsListSuccess
} from '../actions/listActions';
import{
  updateMobileStateSuccess,
  updateMobileStateError
} from '../actions/mobileActions';
import {
  requestCheckJWT,
  requestUserUpdate,
  requestPasswordChange,
  requestGames,
  requestMyGames,
  requestUmpInfo,
  requestDivisions,
  requestSeasons,
  requestSeasonsByActive,
  requestTeams,
  requestAssignUmpToGame,
  requestRemoveUmpFromGame,
  requestFilteredGames,
  requestCoachesList,
  requestMembersList,
  requestMentorsList,
  requestPlayersList,
  requestUmpiresList,
  requestUmpireUpdate,
  requestMenteeUpdate,
  requestMentorMenteeInfo,
  requestUmpiresNextGame,
  requestCoachesTeams,
  requestAddCoachToTeam,
  requestRemoveCoachFromTeam,
  requestTeamsVolunteering,
  requestTeamVolunteerUpdate,
  requestUpdateUserRole,
  requestDailySummary,
  requestAllUmpireData,
  requestAllVolunteerData,
  requestAllTeamData,
  requestUpdateUmpAvailableForGame,
  requestYtdDataSummary,
  requestDailyWeatherForecast
} from './../actions/api';
import {
  getAllUmpireDataSuccess,
  getAllUmpireDataError,
  getAllVolunteerDataSuccess,
  getAllVolunteerDataError,
  getAllTeamDataSuccess,
  getAllTeamDataError,
  getYtdSummaryDataSuccess,
  getYtdSummaryDataError,
  getDailyWeatherForecastError,
  getDailyWeatherForecastSuccess
} from '../actions/reportsActions';
import { clearErrors } from './../actions/errorActions';

//redux-saga selectors
const getToken = state => {
  return state.user.token;
};

/*const checkingToken = state => {
  return state.user.checkingToken;
};*/

export function* watchUserLogout() {
  while (true) {
    yield take(actions.USER_LOGOUT);
    yield fork(removeJWT);
  }
}

export function removeJWT() {
  window.localStorage.removeItem('kodaro-token');
}

export function* requestJWTCheck(token) {
  try {
    const response = yield call(requestCheckJWT, token);
    if (response.user) {
      yield put(userLoginSuccess(token, response.user, response.data));
    } else {
      yield put(userLogout());
    }
  } catch (e) {
    removeJWT();
  }
}

export function* watchCheckJWT() {
  while (true) {
    const { token } = yield take(actions.USER_CHECK_JWT);
    yield fork(requestJWTCheck, token);
  }
}

export function* requestProfileUpdate(name, phone) {
  try {
    const token = yield select(getToken);
    const response = yield call(requestUserUpdate, token, name, phone);
    if (response.status === 200) {
      yield put(updateUserProfileSuccess(name, phone));
    } else if (response.status === 401) {
      yield put(userLogout());
    } else {
      yield put(updateUserProfileError('Server error'));
    }
  } catch (e) {
    yield put(updateUserProfileError(e));
  }
}

export function* watchUserProfileUpdate() {
  while (true) {
    const { name, phone } = yield take(actions.USER_PROFILE_UPDATE);
    yield fork(requestProfileUpdate, name, phone);
  }
}

export function* userPasswordChange(password) {
  try {
    const token = yield select(getToken);
    const response = yield call(requestPasswordChange, token, password);
    if (response.status === 200) {
      yield put(updatePasswordSuccess());
    } else if (response.status === 401) {
      yield put(userLogout());
    } else {
      yield put(updatePasswordError('Server error'));
    }
  } catch (e) {
    yield put(updatePasswordError('Server error'));
  }
}

export function* watchUserPasswordChange() {
  while (true) {
    const { password } = yield take(actions.USER_PASSWORD_CHANGE);
    yield fork(userPasswordChange, password);
  }
}

export function* gamesRequest(startDate, numRows, lastStartDate, lastGameId) {
  try {
    let token = yield select(getToken);
    if (token === '') {
      const response = yield take(actions.USER_LOGIN_SUCCESS);
      token = response.token;
    }
    const { gamesList, games, error, status } = yield call(
      requestGames,
      token,
      numRows,
      lastStartDate,
      lastGameId,
      startDate,
      false
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getGamesError(error));
    } else {
      yield put(getGamesSuccess(gamesList, games));
    }
  } catch (error) {
    yield put(getGamesError(error));
  }
}

export function* watchGames() {
  while (true) {
    const { startDate, numRows, lastStartDate, lastGameId } = yield take(actions.GET_GAMES);
    yield fork(gamesRequest, startDate, numRows, lastStartDate, lastGameId);
  }
}

export function* updatedGamesRequest(startDate, numRows, lastStartDate, lastGameId) {
  try {
    let token = yield select(getToken);
    if (token === '') {
      const response = yield take(actions.USER_LOGIN_SUCCESS);
      token = response.token;
    }
    const { gamesList, games, error, status } = yield call(
      requestGames,
      token,
      numRows,
      lastStartDate,
      lastGameId,
      startDate,
      false
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getGamesError(error));
    } else {
      yield put(getUpdatedGamesSuccess(gamesList, games));
    }
  } catch (error) {
    yield put(getGamesError(error));
  }
}

export function* watchUpdatedGames() {
  while (true) {
    const { startDate, numRows, lastStartDate, lastGameId } = yield take(actions.UPDATED_GAMES);
    yield fork(updatedGamesRequest, startDate, numRows, lastStartDate, lastGameId);
  }
}

export function* gamesByDateRequest(startDate, numRows, lastStartDate, lastGameId) {
  try {
    const token = yield select(getToken);
    const { gamesList, games, error, status } = yield call(
      requestGames,
      token,
      numRows,
      lastStartDate,
      lastGameId,
      startDate,
      true
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getGamesByDateError(error));
    } else {
      yield put(getGamesByDateSuccess(startDate, gamesList, games));
    }
  } catch (error) {
    yield put(getGamesByDateError(error));
  }
}

export function* watchGamesByDate() {
  while (true) {
    const { startDate, numRows, lastStartDate, lastGameId } = yield take(actions.GET_GAMES_BY_DATE);
    yield fork(gamesByDateRequest, startDate, numRows, lastStartDate, lastGameId);
  }
}

export function* updatedMyGamesRequest(season, team, umpire, startDate) {
  try {
    const token = yield select(getToken);
    const { myGamesList, games, error, status } = yield call(requestMyGames, token, season, team, umpire, startDate);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getMyGamesError(error));
    } else {
      yield put(getUpdatedMyGamesSuccess(myGamesList, games));
    }
  } catch (error) {
    yield put(getMyGamesError(error));
  }
}

export function* watchUpdatedMyGames() {
  while (true) {
    const { season, team, umpire, startDate } = yield take(actions.UPDATED_MY_GAMES);
    yield fork(updatedMyGamesRequest, season, team, umpire, startDate);
  }
}

export function* myGamesRequest(season, team, umpire, startDate) {
  try {
    const token = yield select(getToken);
    const { myGamesList, games, error, status } = yield call(requestMyGames, token, season, team, umpire, startDate);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getMyGamesError(error));
    } else {
      yield put(getMyGamesSuccess(myGamesList, games));
    }
  } catch (error) {
    yield put(getMyGamesError(error));
  }
}

export function* watchMyGames() {
  while (true) {
    const { season, team, umpire, startDate } = yield take(actions.GET_MY_GAMES);
    yield fork(myGamesRequest, season, team, umpire, startDate);
  }
}

export function* filterGamesRequest(startDate, endDate, season, division, team, umpire, showAllGames, limitResults) {
  try {
    const token = yield select(getToken);
    const { filteredGamesList, games, error, status } = yield call(
      requestFilteredGames,
      token,
      startDate,
      endDate,
      season,
      division,
      team,
      umpire,
      showAllGames,
      limitResults
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(filterGamesError(error));
    } else {
      yield put(filterGamesSuccess(filteredGamesList, games));
    }
  } catch (error) {
    yield put(filterGamesError(error));
  }
}

export function* watchFilterGames() {
  while (true) {
    const { startDate, endDate, season, division, team, umpire, showAllGames, limitResults } = yield take(actions.FILTER_GAMES);
    yield fork(filterGamesRequest, startDate, endDate, season, division, team, umpire, showAllGames, limitResults);
  }
}

export function* umpInfo(memberId) {
  try {
    const token = yield select(getToken);
    const { umpires, umpiresList = [], error, status } = yield call(requestUmpInfo, token, memberId);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getUmpInfoError(memberId, error));
    } else {
      yield put(getUmpInfoSuccess(umpires, umpiresList));
    }
  } catch (error) {
    yield put(getUmpInfoError(memberId, error));
  }
}

export function* watchUmpInfo() {
  while (true) {
    const { memberId } = yield take(actions.GET_UMP_INFO);
    yield fork(umpInfo, memberId);
  }
}

export function* dailySummary() {
  try {
    const token = yield select(getToken);
    const { dailySummary, error, status } = yield call(requestDailySummary, token);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getDailySummaryError(error));
    } else {
      yield put(getDailySummarySuccess(dailySummary));
    }
  } catch (error) {
    yield put(getDailySummaryError(error));
  }
}

export function* watchDailySummary() {
  while (true) {
    yield take(actions.GET_DAILY_SUMMARY);
    yield fork(dailySummary);
  }
}

export function* seasonsGet() {
  try {
    const token = yield select(getToken);
    const { seasonsList, seasons, error, status } = yield call(requestSeasons, token);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getSeasonsError(error));
    } else {
      yield put(getSeasonsSuccess(seasonsList, seasons));
    }
  } catch (error) {
    yield put(getSeasonsError(error));
  }
}

export function* watchSeasonsGet() {
  while (true) {
    yield take(actions.GET_SEASONS);
    yield fork(seasonsGet);
  }
}

export function* seasonsGetByActive(activeSeason) {
  try {
    const token = yield select(getToken);
    const { seasonsList, seasons, error, status } = yield call(requestSeasonsByActive, token, activeSeason);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getSeasonsError(error));
    } else {
      yield put(getSeasonsSuccess(seasonsList, seasons));
    }
  } catch (error) {
    yield put(getSeasonsError(error));
  }
}

export function* watchSeasonsGetByActive() {
  while (true) {
    const { activeSeason } = yield take(actions.GET_SEASONS_BY_ACTIVE);
    yield fork(seasonsGetByActive, activeSeason);
  }
}

export function* watchDivisionsGet() {
  while (true) {
    yield take(actions.GET_DIVISIONS);
    yield fork(divisionsRequest);
  }
}

export function* divisionsRequest() {
  try {
    const token = yield select(getToken);
    const { divisionsList, parentDivisionsList, divisions, error, status } = yield call(requestDivisions, token);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getDivisionsError(error));
    } else {
      yield put(getDivisionsSuccess(divisionsList, parentDivisionsList, divisions));
    }
  } catch (error) {
    yield put(getDivisionsError(error));
  }
}

export function* watchTeamsGet() {
  while (true) {
    const { seasonId, divisionId } = yield take(actions.GET_TEAMS);
    yield fork(teamsRequest, divisionId, seasonId);
  }
}

export function* teamsRequest(divisionId, seasonId) {
  try {
    const token = yield select(getToken);
    const { teamsList, teams, error, status } = yield call(requestTeams, token, seasonId, divisionId);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getTeamsError(error));
    } else {
      yield put(getTeamsSuccess(teamsList, teams));
    }
  } catch (error) {
    yield put(getTeamsError(error));
  }
}

export function* watchAddUmpToGame() {
  while (true) {
    const { umpireId, gameId, plate } = yield take(actions.ASSIGN_UMP_TO_GAME);
    yield fork(assignUmpToGameRequest, umpireId, gameId, plate);
  }
}

export function* assignUmpToGameRequest(umpireId, gameId, plate) {
  try {
    const token = yield select(getToken);
    const { updatedGame, error, status } = yield requestAssignUmpToGame(token, umpireId, gameId, plate);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(assignUmpToGameError(umpireId, gameId, plate, error, updatedGame));
    } else {
      yield put(assignUmpToGameSuccess(umpireId, gameId, updatedGame));
    }
  } catch (e) {
    yield put(assignUmpToGameError(umpireId, gameId, plate, e));
  }
}

export function* watchRemoveUmpFromGame() {
  while (true) {
    const { umpireId, gameId, plate } = yield take(actions.REMOVE_UMP_FROM_GAME);
    yield fork(removeUmpFromGameRequest, umpireId, gameId, plate);
  }
}

export function* removeUmpFromGameRequest(umpireId, gameId, plate) {
  try {
    const token = yield select(getToken);
    const { updatedGame, error, status } = yield requestRemoveUmpFromGame(token, umpireId, gameId, plate);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(removeUmpError(umpireId, gameId, plate, error, updatedGame));
    } else {
      yield put(removeUmpSuccess(umpireId, gameId, updatedGame));
    }
  } catch (e) {
    yield put(removeUmpError(umpireId, gameId, plate, e));
  }
}

export function* watchCoachesList() {
  while (true) {
    yield take(actions.GET_COACHES_LIST);
    try {
      const token = yield select(getToken);
      const { coachesList, error, status } = yield requestCoachesList(token);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getCoachesListError(error));
      } else {
        yield put(getCoachesListSuccess(coachesList));
      }
    } catch (e) {
      yield put(getCoachesListError(e));
    }
  }
}

export function* watchPlayersList() {
  while (true) {
    const { seasonId } = yield take(actions.GET_PLAYERS_LIST);
    try {
      const token = yield select(getToken);
      const { playersList, error, status } = yield requestPlayersList(token, seasonId);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getPlayersListError(error));
      } else {
        yield put(getPlayersListSuccess(playersList));
      }
    } catch (e) {
      yield put(getPlayersListError(e));
    }
  }
}

export function* watchMembersList() {
  while (true) {
    yield take(actions.GET_MEMBERS_LIST);
    try {
      const token = yield select(getToken);
      const { membersList, error, status } = yield requestMembersList(token);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getMembersListError(error));
      } else {
        yield put(getMembersListSuccess(membersList));
      }
    } catch (e) {
      yield put(getMembersListError(e));
    }
  }
}

export function* watchMentorsList() {
  while (true) {
    const { seasonId } = yield take(actions.GET_MENTORS_LIST);
    try {
      const token = yield select(getToken);
      const { mentorsList, error, status } = yield requestMentorsList(token, seasonId);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getMentorsListError(error, seasonId));
      } else {
        yield put(getMentorsListSuccess(mentorsList, seasonId));
      }
    } catch (e) {
      yield put(getMentorsListError(e, seasonId));
    }
  }
}

export function* watchUmpiresList() {
  while (true) {
    const { seasonId } = yield take(actions.GET_UMPIRES_LIST);
    try {
      const token = yield select(getToken);
      const { umpiresList, umpires, error, status } = yield requestUmpiresList(token, seasonId);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getUmpiresListError(seasonId, error));
      } else {
        yield put(getUmpiresListSuccess(seasonId, umpiresList, umpires));
      }
    } catch (e) {
      yield put(getUmpiresListError(e));
    }
  }
}

export function* umpireUpdate(memberId, umpireLevel, isMentor, isActive, seasonId) {
  try {
    const token = yield select(getToken);
    const { error, status, updatedUmp } = yield requestUmpireUpdate(
      token,
      memberId,
      umpireLevel,
      isMentor,
      isActive,
      seasonId
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(updateUmpError(memberId, seasonId, error));
    } else {
      yield put(updateUmpSuccess(memberId, seasonId, updatedUmp));
    }
  } catch (error) {
    yield put(updateUmpError(memberId, error));
  }
}

export function* watchUmpUpdate() {
  while (true) {
    const { memberId, umpireLevel, isMentor, isActive, seasonId } = yield take(actions.UPDATE_UMP);
    yield fork(umpireUpdate, memberId, umpireLevel, isMentor, isActive, seasonId);
  }
}

export function* updateMentees(mentorId, menteeId, column, seasonId, operation) {
  try {
    const token = yield select(getToken);
    const { error, status, updatedMentees } = yield requestMenteeUpdate(
      token,
      mentorId,
      menteeId,
      column,
      seasonId,
      operation
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(updateMentorsMenteesError(mentorId, error));
    } else {
      yield put(updateMentorsMenteesSuccess(updatedMentees));
    }
  } catch (error) {
    yield put(updateMentorsMenteesError(mentorId, error));
  }
}

export function* watchUpdateMentees() {
  while (true) {
    const { mentorId, menteeId, column, seasonId, operation } = yield take(actions.UPDATE_MENTEES);
    yield fork(updateMentees, mentorId, menteeId, column, seasonId, operation);
  }
}

export function* watchMentorMenteeInfo() {
  while (true) {
    try {
      const { memberId, seasonId } = yield take(actions.GET_MENTOR_MENTEE_INFO);
      const token = yield select(getToken);
      const { error, status, mentorMenteeInfo } = yield requestMentorMenteeInfo(token, memberId, seasonId);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getMentorMenteeInfoError(error));
      } else {
        yield put(getMentorMenteeInfoSuccess(mentorMenteeInfo));
      }
    } catch (error) {
      yield put(getMentorMenteeInfoError(error));
    }
  }
}

export function* umpsNextGame(memberId, umpType) {
  try {
    const token = yield select(getToken);
    const { error, status, nextGame } = yield requestUmpiresNextGame(token, memberId);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getUmpsNextGameError(memberId, error, umpType));
    } else {
      yield put(getUmpsNextGameSuccess(memberId, nextGame, umpType));
    }
  } catch (error) {
    yield put(getUmpsNextGameError(memberId, error, umpType));
  }
}

export function* watchUmpNextGame() {
  while (true) {
    const { memberId, umpType } = yield take(actions.GET_UMPS_NEXT_GAME);
    yield fork(umpsNextGame, memberId, umpType);
  }
}

export function* sendClearErrors() {
  try {
    yield clearErrors();
  } catch (error) {
    console.log(error);
  }
}

export function* watchClearErrors() {
  while (true) {
    yield take(actions.CLEAR_ERRORS);
    yield fork(sendClearErrors);
  }
}

export function* watchCoachesTeams() {
  while (true) {
    try {
      const { memberId, seasonId } = yield take(actions.GET_COACHES_TEAMS);
      const token = yield select(getToken);
      const { error, status, teamsList } = yield requestCoachesTeams(token, memberId, seasonId);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getCoachesTeamsError(error));
      } else {
        yield put(getCoachesTeamsSuccess(teamsList));
      }
    } catch (error) {
      yield put(getCoachesTeamsError(error));
    }
  }
}

export function* watchAddCoachToTeam() {
  while (true) {
    try {
      const { seasonId, teamId, teamName, divisionName } = yield take(actions.ADD_COACH_TO_TEAM);
      const token = yield select(getToken);
      const { error, status } = yield requestAddCoachToTeam(token, teamId, seasonId);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(addCoachToTeamError(error));
      } else {
        yield put(addCoachToTeamSuccess(teamId, teamName, divisionName));
      }
    } catch (error) {
      yield put(addCoachToTeamError(error));
    }
  }
}

export function* watchRemoveCoachFromTeam() {
  while (true) {
    const { memberId, teamId, seasonId } = yield take(actions.REMOVE_COACH_FROM_TEAM);
    yield fork(removeCoachFromTeam, memberId, teamId, seasonId);
  }
}

export function* removeCoachFromTeam(memberId, teamId, seasonId) {
  try {
    const token = yield select(getToken);
    const { error, status } = yield requestRemoveCoachFromTeam(token, memberId, teamId, seasonId);
    if (status === 401) {
      yield put(userLogout);
    } else if (error) {
      yield put(removeCoachFromTeamError(error));
    } else {
      yield put(removeCoachFromTeamSuccess(memberId, teamId, seasonId));
    }
  } catch (e) {
    yield put(removeCoachFromTeamError(e));
  }
}

export function* watchTeamsVolunteering() {
  while (true) {
    try {
      const { memberId, seasonId } = yield take(actions.GET_TEAMS_VOLUNTEERING);
      const token = yield select(getToken);
      const { error, status, teamsList } = yield requestTeamsVolunteering(token, memberId, seasonId);
      if (status === 401) {
        yield put(userLogout());
      } else if (error) {
        yield put(getTeamsVolunteeringError(error));
      } else {
        yield put(getTeamsVolunteeringSuccess(teamsList));
      }
    } catch (error) {
      yield put(getTeamsVolunteeringError(error));
    }
  }
}

export function* teamVolunteerUpdate({ memberId, seasonId, teamId, column, value, operation }) {
  try {
    const token = yield select(getToken);
    const { error, status, teamsList } = yield requestTeamVolunteerUpdate(
      token,
      memberId,
      seasonId,
      teamId,
      column,
      value,
      operation
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(updateTeamVolunteerError(error));
    } else {
      yield put(updateTeamVolunteerSuccess(teamsList));
    }
  } catch (error) {
    yield put(updateTeamVolunteerError(error));
  }
}

export function* watchTeamVolunteerUpdate() {
  yield takeLatest(actions.UPDATE_TEAM_VOLUNTEER, teamVolunteerUpdate);
}

export function* updateUserRole(memberId, email, isAdmin) {
  try {
    const token = yield select(getToken);
    const { error, status, user } = yield requestUpdateUserRole(token, memberId, email, isAdmin);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(userRoleUpdateError(error));
    } else {
      yield put(userRoleUpdateSuccess(user));
    }
  } catch (error) {
    yield put(userRoleUpdateError(error));
  }
}

export function* watchUpdateUserRole() {
  while (true) {
    const { memberId, email, isAdmin } = yield take(actions.UPDATE_USER_ROLE);
    yield fork(updateUserRole, memberId, email, isAdmin);
  }
}

export function* setUpdateMobileState(mobile) {
  try {
    if (mobile){
      yield put(updateMobileStateSuccess(mobile));
    } else {
      yield put(updateMobileStateError("No value passed in"));
    } 
  } catch (error) {
    console.log(error);
  }
}

export function* watchUpdateMobileState() {
  while (true) {
    const { mobile } = yield take(actions.UPDATE_MOBILE_STATE);
    yield fork(setUpdateMobileState, mobile);
  }
}

export function* allUmpireData() {
  try {
    const token = yield select(getToken);
    const { error, status, umpires, umpiresList } = yield requestAllUmpireData(token);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getAllUmpireDataError(error));
    } else {
      yield put(getAllUmpireDataSuccess(umpires, umpiresList));
    }
  } catch (error) {
    console.log(error);
  }
}

export function* watchGetAllUmpireData() {
  while (true) {
    yield take(actions.GET_ALL_UMPIRE_DATA);
    yield fork(allUmpireData);
  }
}

export function* allVolunteerData() {
  try {
    const token = yield select(getToken);
    const { error, status, volunteers, volunteersList } = yield requestAllVolunteerData(token);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getAllVolunteerDataError(error));
    } else {
      yield put(getAllVolunteerDataSuccess(volunteers, volunteersList));
    }
  } catch (error) {
    console.log(error);
  }
}

export function* watchGetAllVolunteerData() {
  while (true) {
    yield take(actions.GET_ALL_VOLUNTEER_DATA);
    yield fork(allVolunteerData);
  }
}

export function* allTeamData() {
  try {
    const token = yield select(getToken);
    const { error, status, teams, teamsList } = yield requestAllTeamData(token);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getAllTeamDataError(error));
    } else {
      yield put(getAllTeamDataSuccess(teams, teamsList));
    }
  } catch (error) {
    console.log(error);
  }
}

export function* watchGetAllTeamData() {
  while (true) {
    yield take(actions.GET_ALL_TEAM_DATA);
    yield fork(allTeamData);
  }
}

export function* ytdSummaryData() {
  try {
    const token = yield select(getToken);
    const { error, status, seasons, seasonsList } = yield requestYtdDataSummary(token);
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(getYtdSummaryDataError(error));
    } else {
      yield put(getYtdSummaryDataSuccess(seasons, seasonsList));
    }
  } catch (error) {
    console.log(error);
  }
}

export function* watchYtdSummaryData() {
  while (true) {
    yield take(actions.GET_YTD_DATA);
    yield fork(ytdSummaryData);
  }
}

export function* setUmpAvailableForGame(gameID, umpireAvailable) {
  try {
    const token = yield select(getToken);
    const { error, status, gameId, umpAvailable } = yield requestUpdateUmpAvailableForGame(
      token,
      gameID,
      umpireAvailable
    );
    if (status === 401) {
      yield put(userLogout());
    } else if (error) {
      yield put(updateUmpAvailableForGameError(error));
    } else {
      yield put(updateUmpAvailableForGameSuccess(gameId, umpAvailable));
    }
  } catch (error) {
    yield put(updateUmpAvailableForGameError(error));
  }
}

export function* watchUpdateUmpAvailableForGame() {
  while (true) {
    const { gameId, umpAvailable } = yield take(actions.UPDATE_UMP_AVAILABLE_FOR_GAME);
    yield fork(setUmpAvailableForGame, gameId, umpAvailable);
  }
}

export function* dailyWeatherForecast(){
  try{
    const token = yield select(getToken);
    const { error, status, forecasts, forecastsList } = yield requestDailyWeatherForecast(token);
    if(status === 401) {
      yield put(userLogout());
    } else if(error){
      yield put(getDailyWeatherForecastError(error));
    } else {
      yield put(getDailyWeatherForecastSuccess(forecasts, forecastsList));
    }
  } catch (error){
    yield put(getDailyWeatherForecastError(error));
  }
}

export function* watchGetDailyWeatherForecast(){
  while(true){
    const { forecasts, forecastsList } = yield take(actions.GET_DAILY_WEATHER_FORECAST);
    yield fork(dailyWeatherForecast, forecasts, forecastsList);
  }
}

export default function* rootSaga() {
  yield all([
    fork(watchUserLogout),
    fork(watchCheckJWT),
    fork(watchUserProfileUpdate),
    fork(watchUserPasswordChange),
    fork(watchGames),
    fork(watchMyGames),
    fork(watchGamesByDate),
    fork(watchFilterGames),
    fork(watchUmpInfo),
    fork(watchAddUmpToGame),
    fork(watchRemoveUmpFromGame),
    fork(watchSeasonsGet),
    fork(watchSeasonsGetByActive),
    fork(watchDivisionsGet),
    fork(watchTeamsGet),
    fork(watchCoachesList),
    fork(watchPlayersList),
    fork(watchMembersList),
    fork(watchMentorsList),
    fork(watchUmpiresList),
    fork(watchUmpUpdate),
    fork(watchUpdateMentees),
    fork(watchMentorMenteeInfo),
    fork(watchUmpNextGame),
    fork(watchClearErrors),
    fork(watchCoachesTeams),
    fork(watchAddCoachToTeam),
    fork(watchRemoveCoachFromTeam),
    fork(watchTeamsVolunteering),
    fork(watchTeamVolunteerUpdate),
    fork(watchUpdatedMyGames),
    fork(watchUpdatedGames),
    fork(watchUpdateUserRole),
    fork(watchDailySummary),
    fork(watchUpdateMobileState),
    fork(watchGetAllUmpireData),
    fork(watchGetAllVolunteerData),
    fork(watchGetAllTeamData),
    fork(watchYtdSummaryData),
    fork(watchUpdateUmpAvailableForGame),
    fork(watchGetDailyWeatherForecast)
  ]);
}
