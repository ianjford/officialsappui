import React, { Component } from 'react';
import ComboBoxSearch from './ComboBoxSearch';
import './MobileGameUmpire.css';

class MobileGameUmpire extends Component {
  state = {
    selectUmp: {},
    error: '',
    plate: this.props.plate
  };

  handleRemoveUmp = (umpireId, gameId, plate) => {
    this.props.removeUmp(umpireId, gameId, plate);
  };

  handleSelectUmp = (selectedOption, gameId, position) => {
    if (selectedOption) {
      const { value, label } = selectedOption;
      this.setState((prevState, props) => ({
        selectedUmp: {
          [position]: {
            name: label,
            umpireId: value,
            gameId,
            position
          }
        }
      }));
    } else {
      this.setState({ selectedUmp: { [position]: null } });
    }
  };

  handleAssignUmp = (gameId, position) => {
    if (this.state.selectedUmp[position] !== null) {
      const { umpireId } = this.state.selectedUmp[position];
      this.props.assignUmpToGame(umpireId, gameId, position);
    } else {
      this.setState({ error: 'Please select an umpire.' });
    }
  };

  handleUmpSignUp = (memberId, gameId, position) => {
    this.props.assignUmpToGame(memberId, gameId, position);
  };

  render() {
    const {
      umpiresList,
      userType,
      memberId,
      position,
      plate,
      alreadyAssignedThisGame,
      selectedGame
    } = this.props;

    if (userType === 'Admin') {
      return (
        <div className="mobile-game-umpire-container">
          {selectedGame[`${position}Id`] === null ? (
            <div className="mobile-game-umpire-innerContainer">
              <h3 className="">{plate}:</h3>
              <ComboBoxSearch
                mobile="true"
                placeholder="Select umpire"
                items={umpiresList}
                onItemChange={selectedOption => this.handleSelectUmp(selectedOption, selectedGame.id, position)}
              />
              {!alreadyAssignedThisGame ? (
                <button
                  className="pt-button pt-intent-primary"
                  onClick={() => this.handleAssignUmp(selectedGame.id, position)}
                >
                  Assign
                </button>
              ) : (
                <div />
              )}
            </div>
          ) : (
            <div className="mobile-game-umpire-innerContainer">
              <h3>{plate}:</h3>
              <span className="ump-name">
                {`${selectedGame[`${position}Name`]} 
                (L${selectedGame[`${position}Level`]})`}
              </span>
              <button
                className="pt-button pt-intent-danger"
                onClick={() => this.handleRemoveUmp(selectedGame[`${position}Id`], selectedGame.id, position)}
              >
                Remove
              </button>
            </div>
          )}
        </div>
      );
    } else if (userType === 'umpire') {
      return (
        <div className="mobile-game-umpire-container">
          {selectedGame[`${position}Id`] === null ? (
            <div className="mobile-game-umpire-innerContainer">
              <h3>{plate}:</h3>
              <div className="ump-name">No umpire</div>
              {!alreadyAssignedThisGame ? (
                <button
                  className="pt-button pt-intent-primary"
                  onClick={() => this.handleUmpSignUp(memberId, selectedGame.id, position)}
                >
                  Sign Up
                </button>
              ) : (
                <div />
              )}
            </div>
          ) : (
            <div className="mobile-game-umpire-innerContainer">
              <h3>{plate}:</h3>
              <span className="ump-name">
                {`${selectedGame[`${position}Name`]} 
                (L${selectedGame[`${position}Level`]})`}
              </span>
              {selectedGame[`${position}Id`] === memberId ?
                <button
                  className="pt-button pt-intent-danger"
                  onClick={() => this.handleRemoveUmp(selectedGame[`${position}Id`], selectedGame.id, position)}
                >
                  Cancel
                </button>
              :
                ''
              }
            </div>
          )}
        </div>
      );
    } else if (userType === 'coach') {
      return (
        <div className="mobile-game-umpire-container">
          {selectedGame[`${position}Id`] === null ? (
            <div className="mobile-game-umpire-innerContainer">
              <h3>{plate}:</h3>
              <ComboBoxSearch
                mobile="true"
                placeholder="Select umpire"
                items={umpiresList}
                onItemChange={selectedOption => this.handleSelectUmp(selectedOption, selectedGame.id, position)}
              />
              <button className="pt-button pt-intent-primary">Invite</button>
            </div>
          ) : (
            <div className="mobile-game-umpire-innerContainer">
              <h3>{plate}:</h3>
              <span className="ump-name">
                {`${selectedGame[`${position}Name`]} 
                (L${selectedGame[`${position}Level`]})`}
              </span>
            </div>
          )}
        </div>
      );
    } else {
      return (
        <div className="mobile-game-umpire-container">
          {selectedGame[`${position}Id`] === null ? (
            <div className="mobile-game-umpire-innerContainer">
              <h3>{plate}:</h3>
              <ComboBoxSearch
                mobile="true"
                placeholder="Select umpire"
                items={umpiresList}
                onItemChange={selectedOption => this.handleSelectUmp(selectedOption, selectedGame.id, position)}
              />
              <button className="pt-button pt-intent-primary">Invite</button>
            </div>
          ) : (
            <div className="mobile-game-umpire-innerContainer">
              <h3>{plate}:</h3>
              <span className="ump-name">
                {`${selectedGame[`${position}Name`]} 
                (L${selectedGame[`${position}Level`]})`}
              </span>
            </div>
          )}
        </div>
      );
    }
  }
}

export default MobileGameUmpire;
