import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import MobileGameUmpire from './MobileGameUmpire';
import ErrorPopup from './ErrorPopup';
import './SelectedGameMobile.css';

class SelectedGameMobile extends Component {
  state = {};

  render() {
    const {
      umpires,
      umpiresList,
      umpiresFetchStatus,
      memberId,
      removeUmp,
      assignUmpToGame,
      alreadyAssignedThisGame,
      userType,
      selectedGame,
      mobile,
    } = this.props;

    console.log(mobile);

    if (selectedGame && selectedGame !== null) {
      return (
        <div className="selected-game-container">
          <div className="selected-game-container-header">
            {mobile.page === 'home' ? 
              <Link className="back-to-games-button" to="/">
                Back to games
              </Link>
            :
              <Link className="back-to-games-button" to="/my-games">
                Back to games
              </Link>
            }
          </div>
          <div className="selected-game-container-body">
            <div className="row">
              <h3 className="left">{selectedGame.divisionName}</h3>
            </div>
            <div className="row">
              <h3 className="left">
                {selectedGame.homeTeamName} (H) vs {selectedGame.awayTeamName} (A)
              </h3>
            </div>
            <div className="row">
              <h3 className="left">{selectedGame.facilityName}</h3>
            </div>
            <div className="row-game-details">
              <MobileGameUmpire
                selectedGame={selectedGame}
                umpires={umpires}
                umpiresList={umpiresList}
                umpiresFetchStatus={umpiresFetchStatus}
                userType={userType}
                memberId={memberId}
                position="umpPlate"
                removeUmp={removeUmp}
                assignUmpToGame={assignUmpToGame}
                plate="Plate"
                alreadyAssignedThisGame={alreadyAssignedThisGame}
              />
            </div>
            <div className="row-game-details">
              <MobileGameUmpire
                selectedGame={selectedGame}
                umpires={umpires}
                umpiresList={umpiresList}
                umpiresFetchStatus={umpiresFetchStatus}
                userType={userType}
                memberId={memberId}
                position="umpBase1"
                removeUmp={removeUmp}
                assignUmpToGame={assignUmpToGame}
                plate="B1"
                alreadyAssignedThisGame={alreadyAssignedThisGame}
              />
            </div>
            {(selectedGame.umpAvailable !== null || selectedGame.umpAvailable !== '') &&
            selectedGame.umpAvailable > 2 ? (
              <div className="row-game-details">
                <MobileGameUmpire
                  selectedGame={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase2"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                  plate="B2"
                  alreadyAssignedThisGame={alreadyAssignedThisGame}
                />
              </div>
            ) : (
              <div />
            )}
            {(selectedGame.umpAvailable !== null || selectedGame.umpAvailable !== '') &&
            selectedGame.umpAvailable > 3 ? (
              <div className="row-game-details">
                <MobileGameUmpire
                  selectedGame={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase3"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                  plate="B3"
                  alreadyAssignedThisGame={alreadyAssignedThisGame}
                />
              </div>
            ) : (
              <div />
            )}
            {this.props.error !== null ? (
              <div>
                <ErrorPopup error={this.props.error} clearErrors={this.props.clearErrors} />
              </div>
            ) : (
              ''
            )}
          </div>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

export default SelectedGameMobile;
