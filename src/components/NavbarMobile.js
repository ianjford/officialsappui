import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import MobileMenu from './MobileMenu';
import './NavbarMobile.css';

class NavbarMobile extends Component {
  state = {
    isOpen: false
  };

  handleOnClick = event => {
    this.setState({ isOpen: false });
  };

  render() {
    const { handleLogout, history } = this.props;
    return (
      <nav className="pt-navbar pt-fixed-top">
        <div className="pt-navbar-group pt-align-left">
          <div className="navbar-menu">
            <MobileMenu isOpen={this.state.isOpen} history={history} handleLogout={handleLogout} handleOnClick={this.handleOnClick} />
          </div>
        </div>
        <div className="mobile-navbar-header">
          <h2>PHBA App</h2>
        </div>
      </nav>
    );
  }
}

export default NavbarMobile;
