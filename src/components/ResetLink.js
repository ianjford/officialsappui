import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './ResetLink.css';
import logoBlueCrew from './../assets/logoBlueCrew.png';

class ResetLink extends Component {
  state = {
    email: '',
    error: '',
  };

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      this.handleSubmit(event);
    }
  };

  handleEmail = event => {
    let email = event.target.value;
    if (this.state.error !== '') {
      this.setState((prevState, props) => ({ email, error: '' }));
    }
    this.setState((prevState, props) => ({ email }));
  };

  handleSubmit = event => {
    event.preventDefault();
    const { email } = this.state;
    if (email === '') {
      this.setState({ error: 'Please enter a valid email.' });
      return;
    }

    fetch('/api/user/reset', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email,
      }),
    })
      .then(res => {
        if (res.status === 200) {
          // Add message to check email then setTimeout to redirect to login
          this.props.history.push('/', { from: '/reset/link' });
        } else {
          this.setState({ error: 'Invalid email' });
        }
      })
      .catch(error => {
        this.setState({ error: 'Unable to send email link. Please retry later or contact Kodaro support.' });
      });
  };

  componentDidMount() {
    if (this.emailInput) {
      this.emailInput.select();
    }
  }

  render() {
    //const emailClass = this.state.error !== '' ? 'pt-intent-danger' : '';
    return (
      <div className="reset-link-container">
        <div className="reset-link-header">
          <img src={logoBlueCrew} height="auto" width="200" alt="Kodaro logo" />
          <p>
            Enter the email address associated with your account, and we’ll email you a link to reset your password.
          </p>
        </div>
        <div>
          <input
            className="login-inputs"
            type="text"
            placeholder="Enter Email"
            onChange={this.handleEmail}
            onKeyPress={this.handleKeyPress}
            ref={input => {
              this.emailInput = input;
            }}
          />
        </div>
        <div className="reset-link-error">
          <p>{this.state.error}</p>
        </div>

        <div className="submit-btn-container">
          <button className="submit-button-blue" onClick={this.handleSubmit}>
            Send Reset Link
          </button>
        </div>

        <div className="login-footer">
          <Link to="/login">Back to Log in</Link>
        </div>
      </div>
    );
  }
}

export default ResetLink;
