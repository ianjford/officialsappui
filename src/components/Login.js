import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import logoBlueCrew from './../assets/logoBlueCrew.png';
import './Login.css';

class Login extends Component {
  state = {
    email: '',
    password: '',
    error: '',
    errorInput: ''
  };

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      this.handleSubmit(event);
    }
  };

  handleEmail = event => {
    let email = event.target.value;
    if (this.state.errorInput === 'email' || this.state.errorInput === 'All') {
      this.setState((prevState, props) => ({ email, error: '', errorInput: '' }));
    }
    this.setState((prevState, props) => ({ email }));
  };

  handlePassword = event => {
    let password = event.target.value;
    if (this.state.errorInput === 'password' || this.state.errorInput === 'All') {
      this.setState((prevState, props) => ({ password, error: '', errorInput: '' }));
    }
    this.setState((prevState, props) => ({ password }));
  };

  handleSubmit = event => {
    event.preventDefault();
    const { email, password } = this.state;
    if (email === '') {
      this.setState({ error: 'Please enter a valid email.', errorInput: 'email' });
      return;
    }
    if (password === '' || password === undefined) {
      this.setState({ error: 'Please enter a password.', errorInput: 'password' });
      return;
    }
    fetch('/api/login', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email,
        password
      })
    })
      .then(res => res.json())
      .then(res => {
        const { token, user, data } = res;
        window.localStorage.setItem('kodaro-token', token);
        this.props.handleLogin(token, user, data);
        if (user.userType === 'umpire' && !user.umpireLevel > 0) {
          this.props.history.push('/welcome', { from: '/login' });
        } else {
          this.props.history.push('/', { from: '/login' });
        }
      })
      .catch(error => {
        this.setState({ error: 'Unable to log in. Please retry or contact PHBA for support.' });
      });
  };

  componentWillMount() {
    if (this.props.isLoggedIn) {
      this.props.history.push('/', { from: '/login' });
    }
  }

  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.history.push('/', { from: '/login' });
    }

    if (this.emailInput) {
      this.emailInput.select();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isLoggedIn) {
      this.props.history.push('/', { from: '/login' });
    }
  }

  render() {
    //const emailClass = this.state.errorInput === 'email' || this.state.errorInput === 'All' ? 'pt-intent-danger' : '';
    //const passwordClass = this.state.errorInput === 'password' || this.state.errorInput === 'All' ? 'pt-intent-danger' : '';

    return (
      <div className="login-container">
        <div className="form-container">
          <div className="sign-in-header">
            <h2>Hi there!</h2>
            <h2>Welcome to the</h2>
            <h2>PHBA Blue Crew</h2>
          </div>
          <div className="login-header">
            <img src={logoBlueCrew} height="auto" width="200" alt="Kodaro logo" />
          </div>
          <div>
            <input
              className="login-inputs"
              data-test="email"
              value={this.state.email}
              onChange={this.handleEmail}
              onKeyPress={this.handleKeyPress}
              ref={input => {
                this.emailInput = input;
              }}
              type="text"
              placeholder="Email"
              required
            />
          </div>
          <div>
            <input
              className="login-inputs"
              data-test="password"
              value={this.state.password}
              onChange={this.handlePassword}
              onKeyPress={this.handleKeyPress}
              type="password"
              placeholder="Password"
              required
            />
          </div>
          <div className="login-error">
            <p>{this.state.error}</p>
          </div>
          <div className="submit-btn-container">
            <button className="submit-button-blue" data-test="submit" onClick={this.handleSubmit}>
              Sign In
            </button>
          </div>
          <div className="login-links">
            <div className="login-forgot-password">
              <Link to="/reset/link">Forgot Password</Link>
            </div>
            <div className="login-create-account">
              <Link to="/register">Create Account</Link>
            </div>
          </div>
          <div className="login-footer">
            <a href="http://phba.org/">go to phba.org</a>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
