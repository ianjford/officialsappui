import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { DateInput, DateRangePicker } from '@blueprintjs/datetime';
import { Overlay } from '@blueprintjs/core';
import format from 'date-fns/format';
import addDays from 'date-fns/add_days';
import subDays from 'date-fns/sub_days';
import MobileGameUmpire from './MobileGameUmpire';
import './Calendar.css';
import { getUmpInfo } from '../actions/umpireActions';

class Calendar extends Component {
  state = {
    selectedDate: null,
    selectedGame: null
  };

  componentDidMount() {
    const selectedDate = '05/02/2017';
    this.setState({ selectedDate });
    this.props.getGamesByDate(selectedDate, 10, null, null);
  }

  handleAddDate = e => {
    e.preventDefault();
    const newSelectedDate = format(addDays(this.state.selectedDate, 1), 'MM/DD/YYYY');
    this.setState({ selectedDate: newSelectedDate, selectedGame: null });
    this.props.getGamesByDate(newSelectedDate, 10, null, null);
  };

  handleSubtractDate = e => {
    e.preventDefault();
    const newSelectedDate = format(subDays(this.state.selectedDate, 1), 'MM/DD/YYYY');
    this.setState({ selectedDate: newSelectedDate, selectedGame: null });
    this.props.getGamesByDate(newSelectedDate, 10, null, null);
  };

  selectGame = gameId => {
    this.setState({ selectedGame: gameId });
  };

  loadAdditionalGames = () => {
    const { games, calendarGamesList, getGamesByDate } = this.props;
    const { selectedDate } = this.state;
    const gamesList =
      calendarGamesList[this.state.selectedDate] === undefined ? [] : calendarGamesList[this.state.selectedDate];
    console.log({ gamesList, selectedDate });
    const lastGameId = gamesList[gamesList.length - 1];
    const lastStartDate = format(games[lastGameId].startDate, 'MM-DD-YYYY HH:mm:ss');
    getGamesByDate(selectedDate, 10, lastStartDate, lastGameId);
  };

  render() {
    const {
      games,
      calendarGamesList,
      umpires,
      umpiresList,
      umpiresFetchStatus,
      userType,
      memberId,
      assignUmpToGame,
      removeUmp
    } = this.props;
    const { selectedGame } = this.state;
    const gamesList =
      calendarGamesList[this.state.selectedDate] === undefined ? [] : calendarGamesList[this.state.selectedDate];
    return (
      <div className="home-mobile-container">
        <div className="container-date-picker">
          <button
            className="pt-button pt-minimal pt-icon-large pt-icon-chevron-left"
            onClick={this.handleSubtractDate}
          />
          <h2>{this.state.selectedDate}</h2>
          <button className="pt-button pt-minimal pt-icon-large pt-icon-chevron-right" onClick={this.handleAddDate} />
        </div>
        <div className="calendar-filters-container">
          <select id="gender">
            <option value="all-genders">All Genders</option>
            <option value="Girls">Girls</option>
            <option value="Boys">Boys</option>
          </select>
          <select id="division">
            <option value="all-divisions">All Divisions</option>
            {/* {this.state.divisionsTable.map((division, index) => {
              return <option value={division.divisionName}> {division.divisionName} </option>;
            })} */}
          </select>
          <select id="umpire-type">
            <option value="any-umpire">Any Umpire</option>
            {/* {this.state.umpiresTable.map((umpires, index) => {
              return <option value={umpires.umpire}> {umpires.umpire} </option>;
            })} */}
          </select>
        </div>
        <div className="form-container-home-upcoming-games">
          <div className="section-heading">
            <h2 className="left">GAMES</h2>
          </div>
          <div className="mobile-games-container">
            {gamesList.map((id, index) => {
              return (
                <div className="row" key={id} onClick={() => this.selectGame(id)}>
                  <p className="left">
                    {`${format(games[id].startDate, 'MM/DD/YY h:mm a')} - ${games[id].divisionName} - ${
                      games[id].facilityName
                    }`}
                  </p>
                </div>
              );
            })}
          </div>
          <div className="load-additional-games-container">
            <a onClick={this.loadAdditionalGames}>Load More Games</a>
          </div>
        </div>
        <div className="form-container-home-game-details">
          <div className="section-heading">
            <h2 className="left">GAME DETAILS</h2>
          </div>
          {selectedGame === null ? (
            <div className="mobile-game-details-container">
              <h3>Select a Game</h3>
            </div>
          ) : (
            <div className="mobile-game-details-container">
              <div className="row">
                <h3 className="left">{games[selectedGame].divisionName}</h3>
              </div>
              <div className="row">
                <h3 className="left">
                  {games[selectedGame].homeTeamName} (H) vs {games[selectedGame].awayTeamName} (A)
                </h3>
              </div>
              <div className="row">
                <h3 className="left">{`Field: ${games[selectedGame].facilityName}`}</h3>
              </div>
              <div className="row">
                <h3 className="left">Plate Ump:</h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpPlate"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
              <div className="row">
                <h3 className="left">B1 Ump:</h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase1"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
              <div className="row">
                <h3 className="left">B2 Ump:</h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase2"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
              <div className="row">
                <h3 className="left">B3 Ump:</h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase3"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default Calendar;
