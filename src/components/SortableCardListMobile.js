import React, { Component } from 'react';
import { DragDropContext } from 'react-dnd';
import { default as TouchBackend } from 'react-dnd-touch-backend';
import TeamVolunteerCard from './TeamVolunteerCard';

class SortableCardListMobile extends Component {
  moveCard = (dragIndex, hoverIndex) => {
    const { list, handleNewList } = this.props;
    const dragCard = list[dragIndex];
    let listWithoutCard = [...list.slice(0, dragIndex), ...list.slice(dragIndex + 1)];
    let newList = [...listWithoutCard.slice(0, hoverIndex), dragCard, ...listWithoutCard.slice(hoverIndex)];
    handleNewList(newList);
  };

  render() {
    const { list } = this.props;
    return (
      <div>
        {list.map((card, index) => {
          return (
            <TeamVolunteerCard team={card} key={card.teamId} index={index} moveCard={this.moveCard} {...this.props} />
          );
        })}
      </div>
    );
  }
}

export default DragDropContext(TouchBackend)(SortableCardListMobile);
