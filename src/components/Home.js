import React, { Component } from 'react';
import { Dialog, Tab2, Tabs2 } from '@blueprintjs/core';
import { DateRangePicker } from '@blueprintjs/datetime';
import GameUmpire from './GameUmpire';
import ComboBoxSearch from './ComboBoxSearch';
import format from 'date-fns/format';
import addDays from 'date-fns/add_days';
import './Home.css';
import WeatherContainer from '../containers/WeatherContainer';
import ErrorPopup from './ErrorPopup';
import MentorMenteeCardContainer from '../containers/MentorMenteeCardContainer';
import UserStatisticsContainer from '../containers/UserStatisticsContainer';

class Home extends Component {
  state = {
    isOpen: false,
    filteringGames: false,
    filters: {
      startDate: '',
      endDate: '',
      season: '',
      division: '',
      team: '',
      umpire: '',
      allGames: false,
      levelNeeded: '',
      levelAssigned: ''
    },
    selectedOption: '',
    tab: 'home-opening-tab',
    displayAdditionalGames: false,
    updatingAnyGames: false,
  };

  componentDidMount() {
    const {
      getUmpire,
      getSeasonsByActive,
      getDivisions,
    } = this.props;

    const today = new Date(Date.now());
    const tomorrow = addDays(today, 1);
    //const todayFormatted = format(today, 'MM/DD/YYYY');
    //const tomorrowFormatted = format(tomorrow, 'MM/DD/YYYY');

    getSeasonsByActive(true);
    getUmpire();
    this.getAllGames();
    getDivisions();

    this.setState({
      filters: { ...this.state.filters, startDate: today, endDate: tomorrow }
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      const {
        seasonsList,
        fetchingGamesStatus,
        getCoachesTeams,
        memberId,
        updateUmpireStatus,
      } = this.props;

      let gamesCheck = this.isGameListLoaded();
      let seasonsCheck = this.areSeasonsLoaded();
      let isCoach = this.isCoach();
      let defaultGamesCheck = (this.props.userType === 'Admin' && this.props.umpireLevel === null && !isCoach) ? true : this.defaultGamesLoaded();
      //let coachingTeamsCheck = this.teamsCoachingLoaded();
      let isUpdating = this.state.updatingAnyGames;

      if (!gamesCheck) gamesCheck = nextProps.fetchingGamesStatus === 'Loaded' || fetchingGamesStatus === 'Loaded';

      if (!seasonsCheck)
        seasonsCheck = nextProps.fetchingSeasonsStatus === 'Loaded' || nextProps.fetchingSeasonsStatus === 'Loading' || this.areSeasonsLoaded();

      if (!defaultGamesCheck){
        defaultGamesCheck =
          nextProps.fetchingMyGamesStatus === 'Loaded' || nextProps.fetchingMyGamesStatus === 'Loading' || this.defaultGamesLoaded(); 
      } 

      isUpdating = nextProps.fetchingGamesStatus === 'Loading' || nextProps.fetchingMyGamesStatus === 'Loading' || nextProps.fetchingFilteredGamesStatus === 'Loading';

      if (this.areSeasonsLoaded()) {
        if (nextProps.fetchingCoachTeamsStatus !== 'Loading' && nextProps.fetchingCoachTeamsStatus !== 'Loaded' && !this.teamsCoachingLoaded()){
          getCoachesTeams(memberId, seasonsList[0]);
        }

        if (!defaultGamesCheck &&
          nextProps.fetchingMyGamesStatus !== 'Loading' &&
          nextProps.fetchingCoachTeamsStatus === 'Loaded'
        ) {
          this.loadDefaultGames(nextProps.teamsCoaching);
        }
      }

      if (this.defaultGamesLoaded() && updateUmpireStatus !== nextProps.updateUmpireStatus) {
        this.updateMyGames();
      }

      this.setState({
        updatingAnyGames: isUpdating
      });
    }
  }

  isAdmin = () => {
    return this.props.userType === 'Admin';
  }

  isUmpire = () => {
    return this.props.umpireLevel !== null && this.props.umpireLevel > 0;
    //return false;
  }

  isCoach = () => {
    return this.props.teamsCoaching.length > 0;
    //return false;
  }

  defaultGamesLoaded = () => {
    const { fetchingMyGamesStatus } = this.props;
    return fetchingMyGamesStatus === 'Loaded' || fetchingMyGamesStatus === 'Error';
  }

  isGameListLoaded = () => {
    const { fetchingGamesStatus } = this.props;
    return fetchingGamesStatus === 'Loaded' || fetchingGamesStatus === 'Error';
  }

  areSeasonsLoaded = () => {
    const { fetchingSeasonsStatus } = this.props;
    return fetchingSeasonsStatus === 'Loaded' || fetchingSeasonsStatus === 'Error';
  }

  teamsCoachingLoaded = () => {
    const { fetchingCoachTeamsStatus } = this.props;
    return fetchingCoachTeamsStatus === 'Loaded' || fetchingCoachTeamsStatus === 'Error';
  }

  getGameUmpireLevel = game => {
    switch(game.divisionId){
      case "22303":
        //"boys intstructional 4"
        break;
      case "22304":
        //"boys instructional 5-6"
        break;
      case "22305":
        //"boys intermediate 7"
        break;
      case "22306":
        //"boys pioneer 8"
        if(game.umpAvailable > Number(game.umpsUsed)){
          if(game.umpPlateLevel >= 3) {
            return 1;
          } else if(game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            return 2;
          }else {
            return 3;
          }
        }
        break;
      case "22309":
        //"boys american 9-10"
        if(game.umpAvailable > Number(game.umpsUsed)){
          if(game.umpPlateLevel >= 3) {
            return 1;
          } else if(game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            return 2;
          }else {
            return 3;
          }
        }
        break;
      case "22310":
        //"boys national 11-12"
        if(game.umpAvailable > Number(game.umpsUsed) && 
          ((game.umpPlateLevel === null || game.umpPlateLevel === 'null') || 
            (game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3))){
          return 3;
        } else {
          return 4;
        }
        //break;
      case "22311":
        //"boys western 13-14"
        if(game.umpAvailable > Number(game.umpsUsed) && (game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3)){
          return 3;
        } else {
          return 4;
        }
        //break;
      case "22312":
        //"boys majors 15-18"
        break;
      case "22315":
        //"girls instructional 4-6"
        break;
      case "22316":
        //"girls pioneer 7-8"
       if(game.umpAvailable > Number(game.umpsUsed)){
          if(game.umpPlateLevel >= 3) {
            return 1;
          } else if(game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            return 2;
          }else {
            return 3;
          }
        }
        break;
      case "22317":
        //"girls american 9-10"
        if(game.umpAvailable > Number(game.umpsUsed)){
          if(game.umpPlateLevel >= 3) {
            return 1;
          } else if(game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            return 2;
          }else {
            return 3;
          }
        }
        break;
      case "22318":
        //"girls national 11-12"
         if(game.umpAvailable > parseInt(game.umpsUsed, 10) && 
          ((game.umpPlateLevel === null || game.umpPlateLevel === 'null') || 
            (game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3))){
          return 3;
        } else {
          return 4;
        }
        //break;
      case "22319":
        //"girls western 13-18"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10) && game.umpPlateLevel >= 3){
          return 3;
        } else {
          return 4;
        }
        //break;
      default:
        console.log(game);
        break;
    }
  }

  handleCheckboxChange = event => {
    const { checked } = event.target;
    const { season, division, team, umpire } = this.state.filters;

    let showAllGames = checked;
    let isFiltering = checked;

    console.log(this.state.filters.season === '');
    console.log(this.state.filters);
    isFiltering = season !== '' || division !== '' || team !== '' || umpire !== '' || checked;

    this.setState(
      (prevState, props) => ({
        ...prevState,
        filteringGames: isFiltering,
        filters: { ...prevState.filters, allGames: showAllGames}
      }),
      this.handleFilterGames
    );
  }

  handleSelectChange = event => {
    const { id, value } = event.target;
    let newValue = value === 'all' ? '' : value;
    if (id === 'division') {
      const teamValue = this.state.filters.division !== value ? '' : this.state.filters.team;
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, [id]: newValue, team: teamValue }
        }),
        this.handleFilterGames
      );
      this.props.getTeams(value, this.props.seasonsList[0]);
    } else {
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, [id]: newValue }
        }),
        this.handleFilterGames
      );
    }
  };

  handleFilterDate = selectedDate => {
    console.log(selectedDate);

    this.setState(
      (prevState, props) => ({
        ...prevState,
        filteringGames: true,
        filters: { ...prevState.filters, startDate: selectedDate[0], endDate: selectedDate[1] }
      }),
      this.handleFilterGames
    );
  };

  handleUmpireChange = selectedOption => {
    if (selectedOption !== null) {
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, umpire: selectedOption.value },
          selectedOption
        }),
        this.handleFilterGames
      );
    } else {
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, umpire: '' },
          selectedOption: ''
        }),
        this.handleFilterGames
      );
    }
  };

  removeFilters = () => {
    this.setState({
      filteringGames: false,
      filters: {
        startDate:  new Date(Date.now()),
        endDate: addDays(new Date(Date.now()), 1),
        season: '',
        division: '',
        team: '',
        umpire: '',
        allGames: false,
        levelAssigned: '',
        levelNeeded: ''
      },
      selectedOption: ''
    });
  };

  handleFilterGames = () => {
    let endDate =
      this.state.filters.endDate === null
        ? this.state.filters.startDate
        : this.state.filters.endDate;
    endDate = endDate === null ? null : format(endDate, 'MM/DD/YYYY');
    this.props.filterGames(
      format(this.state.filters.startDate, 'MM/DD/YYYY'),
      endDate,
      this.state.filters.season,
      this.state.filters.division,
      this.state.filters.team,
      this.state.filters.umpire,
      this.state.filters.allGames,
      false
    );
  };

  handleFilterDialog = event => {
    this.setState({ ...this.state, isOpen: !this.state.isOpen });
  };

  loadAdditionalGames = () => {
    const { getUpdatedGames, seasonsList } = this.props;
    const { filteringGames } = this.state;
    /*const lastGameId = filteringGames
      ? filteredGamesList[filteredGamesList.length - 1]
      : gamesList[gamesList.length - 1];*/
    /*const lastStartDate = filteringGames 
    ? filteredGamesList[lastGameId]
    : games[lastGameId].startDate;*/
    const numberOfGamesToLoad = 145;
    if(filteringGames){
      this.props.filterGames(format(this.state.filters.startDate, 'MM/DD/YYYY'), null, seasonsList[0], null, null, null, this.state.filteres.allGames, false);
    } else {
      getUpdatedGames(format(this.state.filters.startDate, 'MM/DD/YYYY'), numberOfGamesToLoad, null, null);
    }
    this.setState({ displayAdditionalGames: true });
    };

  loadDefaultGames = (teamsBeingCoached) => {
      const { getMyGames, seasonsList, memberId } = this.props;
      const isUmpire = this.isUmpire();
  
      let myTeams = '';
      if(teamsBeingCoached.length > 0 ){
        for(let i = 0; i < teamsBeingCoached.length; i++){
          if(i === teamsBeingCoached.length - 1)
            myTeams += teamsBeingCoached[i].teamId;
          else  
            myTeams += teamsBeingCoached[i].teamId + ",";
        }
      }
      const nullValue = '';

      let seasonId = seasonsList[0];
      let teamIds = teamsBeingCoached.length > 0 ? myTeams : nullValue;
      let umpireId = isUmpire ? memberId : nullValue;

      getMyGames(seasonId, teamIds, umpireId, format(Date.now(), 'MM/DD/YYYY'));
  };

  updateMyGames = () => {
    if (this.props.fetchingMyGamesStatus !== 'Loading') {
      const { seasonsList, memberId, getUpdatedMyGames, teamsCoaching } = this.props;

      const isUmpire = this.isUmpire();
      const isCoach = this.isCoach();

      let myTeams = '';
      for(let i = 0; i < teamsCoaching.length; i++){
        if(i === teamsCoaching.length - 1)
          myTeams += teamsCoaching[i].teamId;
        else  
          myTeams += teamsCoaching[i].teamId + ",";
      }
      const nullValue = '';

      let seasonId = seasonsList[0];
      let teamIds = isCoach ? myTeams : nullValue;
      let umpireId = isUmpire ? memberId : nullValue;

      getUpdatedMyGames(seasonId, teamIds, umpireId, format(Date.now(), 'MM/DD/YYYY'));
    }
  };

  getAllGames = () => {
    let numberOfGamesToLoad = 25;
    const today = new Date(Date.now());
    //const tomorrow = addDays(today, 1);
    const todayFormatted = format(today, 'MM/DD/YYYY');
    //const tomorrowFormatted = format(tomorrow, 'MM/DD/YYYY');
    this.props.getGames(todayFormatted, numberOfGamesToLoad, null, null);
  }

  updateAllGames = () => {
    let numberOfGamesToLoad = 25;
    this.props.getUpdatedGames(format(Date.now(), 'MM/DD/YYYY'), numberOfGamesToLoad, null, null);
  };

  handleTabChange = newTab => {
    let loadedDefault = this.state.defaultGamesLoaded;
    let loadedGames = this.state.gamesListLoaded;
    let isUpdating = false;

    if (newTab === 'home-opening-tab') {
      this.updateMyGames();
      isUpdating = true;
    } else if (newTab === 'home-selection-tab') {
      this.updateAllGames();
      isUpdating = true;
    }
    this.setState({
      ...this.state,
      tab: newTab,
      isOpen: false,
      filteringGames: false,
      defaultGamesLoaded: loadedDefault,
      gamesListLoaded: loadedGames,
      displayAdditionalGames: false,
      updatingAnyGames: isUpdating
    });
  };

  /*{this.state.tab === 'home-opening-tab' ? 
    <div className="my-game-numbering">{games[id].gameNumber}.</div>
  :
    ''
  }      put on line 790 for numbering on my-games        */

  render() {
    const {
      divisions,
      divisionsList,
      teams,
      teamsList,
      games,
      gamesList,
      myGamesList,
      filteredGamesList,
      memberId,
      umpires,
      umpiresList,
      umpiresFetchStatus,
      seasons,
      seasonsList
    } = this.props;
    const {
      displayAdditionalGames,
      tab,
      filteringGames,
    } = this.state;
    let displayedGameList = [];

    let isCoach = this.isCoach();
    let isUmpire = this.isUmpire();
    let isAdmin = this.isAdmin();

    if (this.teamsCoachingLoaded()) {
      if (tab === 'home-selection-tab' || (isAdmin && !isCoach && !isUmpire)) {
        if (filteringGames) {
          if(this.state.filters.levelAssigned !== '' && this.state.filters.levelNeeded !== ''){
            filteredGamesList
            .filter(game => (this.state.filters.levelAssigned === games[game].umpPlateLevel ||
                this.state.filters.levelAssigned === games[game].umpBase1Level ||
                this.state.filters.levelAssigned === games[game].umpBase2Level ||
                this.state.filters.levelAssigned === games[game].umpBase3Level) &&
                (this.getGameUmpireLevel(games[game]) <= this.state.filters.levelNeeded))
            .map(game => {
              return displayedGameList.push(game);
            });
          } else if(this.state.filters.levelAssigned !== '' && this.state.filters.levelNeeded === ''){
            filteredGamesList
            .filter(game => this.state.filters.levelAssigned === games[game].umpPlateLevel ||
                this.state.filters.levelAssigned === games[game].umpBase1Level ||
                this.state.filters.levelAssigned === games[game].umpBase2Level ||
                this.state.filters.levelAssigned === games[game].umpBase3Level)
            .map(game => {
              return displayedGameList.push(game);
            });
          } else if(this.state.filters.levelAssigned === '' && this.state.filters.levelNeeded !== ''){
            filteredGamesList
            .filter(game => this.getGameUmpireLevel(games[game]) <= this.state.filters.levelNeeded)
            .map(game => {
              return displayedGameList.push(game);
            });
          }else {
            displayedGameList = filteredGamesList;
          }
        } else {
          displayedGameList = gamesList;
        }
      } else if (tab === 'home-opening-tab') {
        displayedGameList = myGamesList;
      }
    }

    const hideLoadMoreGamesClass = filteringGames ? 'hide' : '';

    return (
      <div className="home-container">
        <div className="main-col">
          <div className="games-container">
            <div className="table-header">
              {isAdmin ? (
                <div>
                   {isCoach || isUmpire ?
                      <Tabs2
                        id="home-page-tabs"
                        large="true"
                        defaultSelectedTabId="home-opening-tab"
                        selectedOption={this.state.tab}
                        onChange={selectedOption => this.handleTabChange(selectedOption)}
                      >
                        <Tab2 id="home-opening-tab" title="My Games" />
                        <Tab2 id="home-selection-tab" title="Assign Games" />
                        <Tabs2.Expander />
                      </Tabs2>
                    :
                      <Tabs2
                          id="home-page-tabs"
                          large="true"
                          defaultSelectedTabId="home-selection-tab-tab"
                      >
                        <Tab2 id="home-selection-tab" title="Assign Games" />
                        <Tabs2.Expander />
                      </Tabs2>  
                    }
                  </div>
              ) : (
               <div>
                 {isCoach || isUmpire ?
                    <Tabs2
                      id="home-page-tabs"
                      large="true"
                      defaultSelectedTabId="home-opening-tab"
                      selectedOption={this.state.tab}
                      onChange={selectedOption => this.handleTabChange(selectedOption)}
                    >
                      <Tab2 id="home-opening-tab" title="My Games" />
                      <Tab2 id="home-selection-tab" title="All Games" />
                      <Tabs2.Expander />
                    </Tabs2>
                  :
                    <Tabs2
                      id="home-page-tabs"
                      large="true"
                      defaultSelectedTabId="home-selection-tab"
                    >
                      <Tab2 id="home-selection-tab" title="All Games" />
                      <Tabs2.Expander />
                    </Tabs2>
                  }
                </div>
              )}
              <div className="filter-objects">
                {this.state.tab === 'home-selection-tab' || (isAdmin && !isCoach && !isUmpire) ? (
                  <div className="game-filters-btn">
                    <button className="pt-button" onClick={this.handleFilterDialog}>
                      Filters
                    </button>
                  </div>
                ) : (
                  ''
                )}
                <Dialog
                  className="games-filter-dialog"
                  isOpen={this.state.isOpen}
                  title="Game Filters"
                  onClose={this.handleFilterDialog}
                >
                  <div className="game-filters-container">
                    <label className="pt-label pt-inline date-range">
                      Select Date:
                      <DateRangePicker
                        format="MM/DD/YYYY"
                        allowSingleDayRange={true}
                        closeOnSelection={true}
                        value={[this.state.filters.startDate, this.state.filters.endDate]}
                        minDate={new Date(Date.now())}
                        onChange={this.handleFilterDate}
                        shortcuts={false}
                      />
                    </label>
                    <label className="pt-label pt-inline">
                      Season:
                      <select
                        className="pt-input"
                        id="season"
                        value={this.state.filters.season}
                        onChange={this.handleSelectChange}
                      >
                        <option key="seasons-all" value="all">
                          All Seasons
                        </option>
                        {seasonsList.map(seasonId => {
                          return (
                            <option key={seasonId} value={seasonId}>
                              {seasons[seasonId].name}
                            </option>
                          );
                        })}
                      </select>
                    </label>
                    <label className="pt-label pt-inline">
                      Division:
                      <select
                        className="pt-input"
                        id="division"
                        value={this.state.filters.division}
                        onChange={this.handleSelectChange}
                      >
                        <option key="divisions-all" value="all">
                          All Division
                        </option>
                        {divisionsList.map(divisionId => {
                          return (
                            <option key={divisionId} value={divisionId}>
                              {divisions[divisionId].name}
                            </option>
                          );
                        })}
                      </select>
                    </label>
                    <label className="pt-label pt-inline">
                      Teams:
                      <select
                        className="pt-input"
                        id="team"
                        value={this.state.filters.team}
                        onChange={this.handleSelectChange}
                      >
                        <option key="teams-all" value="all">
                          All Teams
                        </option>
                        {teamsList.map(teamId => {
                          return (
                            <option key={teamId} value={teamId}>
                              {teams[teamId].name}
                            </option>
                          );
                        })}
                      </select>
                    </label>
                    <label className="pt-label pt-inline umpire-label">
                      Umpire:
                      <ComboBoxSearch
                        className="umpire-select"
                        placeholder="Select umpire"
                        items={umpiresList}
                        selectedOption={this.state.selectedOption}
                        onItemChange={selectedOption => this.handleUmpireChange(selectedOption)}
                      />
                    </label>
                    <label className="pt-label pt-inline level-label">
                      Level Assigned:
                      <select
                        className="pt-input"
                        id="levelAssigned"
                        value={this.state.filters.levelAssigned}
                        onChange={this.handleSelectChange}
                      >
                        <option key="levels-all" value="all">
                          All Levels
                        </option>
                        <option key="levels-one" value="1">
                          1
                        </option>
                        <option key="levels-two" value="2">
                          2
                        </option> 
                        <option key="levels-three" value="3">
                          3
                        </option>
                        <option key="levels-four" value="4">
                          4
                        </option>
                      </select>
                    </label>
                    <label className="pt-label pt-inline level-label">
                      Level Needed:
                      <select
                        className="pt-input"
                        id="levelNeeded"
                        value={this.state.filters.levelNeeded}
                        onChange={this.handleSelectChange}
                      >
                        <option key="levels-all" value="all">
                          All Levels
                        </option>
                        <option key="levels-one" value="1">
                          1
                        </option>
                        <option key="levels-two" value="2">
                          2
                        </option> 
                        <option key="levels-three" value="3">
                          3
                        </option>
                        <option key="levels-four" value="4">
                          4
                        </option>
                      </select>
                    </label>
                    <label className="pt-label pt-inline checkbox-display-full-games">
                      Show All Games:
                        <input 
                          type="checkbox" 
                          value={this.state.filters.allGames}  
                          onChange={this.handleCheckboxChange}
                          checked={this.state.filters.allGames}
                        />
                    </label>
                    <div className="game-filters-clear-btn">
                      <button className="pt-button" onClick={this.removeFilters}>
                        Clear
                      </button>
                    </div>
                  </div>
                </Dialog>
              </div>
            </div>
            {this.props.error !== null ? (
              <div>
                <ErrorPopup error={this.props.error} clearErrors={this.props.clearErrors} />
              </div>
            ) : (
              ''
            )}
            {this.defaultGamesLoaded() && this.isGameListLoaded() && this.teamsCoachingLoaded() && !this.state.updatingAnyGames ? (
              <div>
                {displayedGameList !== null && displayedGameList.length > 0 && Object.values(games).length > 0 ? (
                  <div className="table">
                    {displayedGameList.map((id, index) => {
                      return (
                        <div className="row" key={`todayGames-${id}`}>
                          {this.state.tab === 'home-opening-tab' ? 
                            <div className="my-game-numbering">{games[id].gameNumber}.</div>
                          :
                            ''
                          }   
                          <div className="game-date-location-container">
                            <h3 className="game-time">{format(games[id].startDate, 'ddd MMM D, h:mm a')}</h3>
                            <h3>{`Field: ${games[id].facilityName}`}</h3>
                            <h3>{games[id].divisionName}</h3>
                            <h3>{`${games[id].homeTeamName} v. ${games[id].awayTeamName}`}</h3>
                          </div>
                          <GameUmpire
                            games={games}
                            gameId={id}
                            umpires={umpires}
                            umpiresList={umpiresList}
                            umpiresFetchStatus={umpiresFetchStatus}
                            memberId={memberId}
                            isUmpire={isUmpire}
                            isAdmin={isAdmin}
                            isCoach={isCoach}
                            assignUmpToGame={this.props.assignUmpToGame}
                            removeUmp={this.props.removeUmp}
                            allowsAssigning={this.state.tab === 'home-selection-tab'}
                          />
                        </div>
                      );
                    })}
                  </div>
                ) : (
                  <div className="table">
                    <div className="empty-table"> Not assigned to any games. </div>
                  </div>
                )}
              </div>
            ) : (
              <div className="empty-table"> Loading Games... </div>
            )}
            <div className={`${hideLoadMoreGamesClass}`}>
              {!displayAdditionalGames &&
              !this.state.updatingAnyGames &&
              tab !== 'home-opening-tab' &&
              displayedGameList !== null &&
              displayedGameList.length > 0 ? (
                <div className="load-additional-games-container">
                  <a onClick={this.loadAdditionalGames}>Load More Games</a>
                </div>
              ) : (
                ''
              )}
            </div>
          </div>
        </div>
        <div className="side-col">
          <div className="sid-col-tuple">
            <WeatherContainer />
            {this.isUmpire() ? <UserStatisticsContainer /> : ''}
          </div>
          <div className="side-col-tuple">
            <MentorMenteeCardContainer />
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
