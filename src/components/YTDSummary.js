import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './ReportTable.css';

class YTDSummary extends Component {
  state = {
  };

  componentDidMount(){
    this.props.getYtdSummaryData();
    //this.props.getSeasonsByActive(true);
  }

  formatPercent = percentile => {
    return Math.round(parseFloat(percentile)*100)/100 + "%";
  }

  render() {
    const {
      reportSeasons,
      reportSeasonsList,
      fetchingYtdSummaryDataStatus,
    } = this.props;

    let outputList = [];

    if(fetchingYtdSummaryDataStatus === 'Loaded'){
      outputList = reportSeasonsList;
    }

    return (
      <div className="report-list-container-main ytd-report-container">
        <div className="report-list-table-header">
          <h1 className="left">YTD Summary Table</h1>
        </div>
        <table className="ytd-report List-Table">
          <thead>
            <tr>
              <th className="small">Season Name</th>
              <th className="small">Total Games</th>
              <th className="large">Total Umpires Requested</th>
              <th className="large">Total Umpires Filled</th>
              <th className="small">% Filled</th>
              <th className="large">% Filled by Guardian</th>
              <th className="large">% Filled by non-Guardians</th>
            </tr>
          </thead>
          <tbody>
            {fetchingYtdSummaryDataStatus === 'Loaded' ? 
              outputList.map((season, index) => {
                return (
                  <tr className="row-highlighter" key={index}>
                    <td className="selection-highlighter small tableLeft">{reportSeasons[season].seasonName}</td>
                    <td className="selection-highlighter small">{reportSeasons[season].totalGames}</td>
                    <td className="selection-highlighter large">{reportSeasons[season].umpsReqTotal}</td>
                    <td className="selection-highlighter large">{reportSeasons[season].umpsFilledTotal}</td>
                    <td className="selection-highlighter small">{this.formatPercent(reportSeasons[season]["%Filled"])}</td>
                    <td className="selection-highlighter large">{this.formatPercent(reportSeasons[season]["%GuardFilled"])} ({reportSeasons[season].guardiansFilled})</td>
                    <td className="selection-highlighter large">{this.formatPercent(reportSeasons[season]["%NonGuardFilled"])}  ({reportSeasons[season].nonGuardiansFilled})</td>
                  </tr>
                );
              })
            :
              <tr className="row-highlighter" key="loading-seasons">
                <td>Loading season data...</td>
              </tr>
            }
          </tbody>
        </table>
      </div>
    );
  }
}

export default YTDSummary;
