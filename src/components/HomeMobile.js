import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import addDays from 'date-fns/add_days';
import subDays from 'date-fns/sub_days';
import './HomeMobile.css';
import ErrorPopup from './ErrorPopup';

class HomeMobile extends Component {
  state = {
    filter: {
      startDate: '',
      value: 7,
      showAllGames: false,
      page: 'home'
    },
    filterType: {
      day: { value: 1, text: 'By Day' }, 
      week: { value: 7, text: 'By Week' }
    },
    additionalGamesLoaded: false,
    displayAdditionalGames: false
  };

  componentDidMount() {
    const { getUmpire, getSeasonsByActive, updateMobileState } = this.props;

    getUmpire();
    getSeasonsByActive(true);

    let date = Date.now();
    //let date = '03/01/2014 08:00:00.00 Z';
    let today = format(date, 'MM/DD/YYYY');

    let startWithFilterGames = false;

    let newMobile = {};
    let newDay = today;
    let newFilterValue = 7;
    if (Object.values(this.props.mobile).length > 0 && this.props.mobile.page) {
      
      let toShowAllGames = false;
      console.log(this.props.mobile.value);
      if (this.props.mobile.value) {
        newFilterValue = this.props.mobile.value;
      }
      if (this.props.mobile.startDate) {
        newDay = this.props.mobile.startDate;
      }
      if (this.props.mobile.showAllGames) {
        toShowAllGames = this.props.mobile.showAllGames;
        startWithFilterGames = toShowAllGames;
      }

      newMobile = { startDate: newDay, filter: newFilterValue, showAllGames: startWithFilterGames, page: 'home' };

      if (startWithFilterGames) {       
        const endDate = format(addDays(newDay, newFilterValue + 1), 'MM/DD/YYYY');
        this.props.filterGames(newDay, endDate, this.areSeasonsLoaded() ? this.props.seasonsList[0] : '', '', '', '', startWithFilterGames, true);
      } else {
        this.loadDefaultGamesFromDates(newDay, newFilterValue);
      }
    } else {
      newMobile = {
        startDate: today,
        value: this.state.filter.value,
        showAllGames: startWithFilterGames,
        page: 'home'
      };
      this.loadDefaultGames();
    }

    updateMobileState(newMobile);

    console.log(newMobile);

    this.setState({
      filter: {
        startDate: newDay,
        value: newFilterValue, 
        page: 'home',
        showAllGames: startWithFilterGames
      } 
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      const {
        seasonsList,
        fetchingGamesStatus,
        fetchingCoachTeamsStatus,
        fetchingSeasonsStatus,
        memberId,
        getCoachesTeams
      } = this.props;
      const {
        displayAdditionalGames,
        additionalGamesLoaded
      } = this.state;

      let umpiresCheck = this.areUmpiresLoaded();
      let seasonsCheck = this.areSeasonsLoaded();
      let defaultGamesCheck = this.defaultGamesLoaded();
      let coachingTeamsCheck = this.teamsCoachingLoaded();
      let additionalGamesCheck = additionalGamesLoaded;

      if (!umpiresCheck) umpiresCheck = nextProps.umpFetchStatus === 'Loaded' && this.props.umpFetchStatus === 'Loaded';

      if (!seasonsCheck)
        seasonsCheck = nextProps.fetchingSeasonsStatus === 'Loaded' || fetchingSeasonsStatus === 'Loaded';

      if (!defaultGamesCheck) {
        defaultGamesCheck = nextProps.fetchingGamesStatus === 'Loaded' || fetchingGamesStatus === 'Loaded';
        if (
          nextProps.fetchingGamesStatus !== 'Loading' &&
          fetchingGamesStatus !== 'Loading' &&
          nextProps.fetchingGamesStatus !== 'Error' &&
          fetchingGamesStatus !== 'Error' &&
          nextProps.fetchingGamesStatus !== 'Loaded' &&
          fetchingGamesStatus !== 'Loaded'
        ) {
          this.loadDefaultGames();
        }
      }

      additionalGamesCheck =
        this.areSeasonsLoaded() &&
        displayAdditionalGames &&
        (nextProps.fetchingGamesStatus === 'Loaded' || fetchingGamesStatus === 'Loaded');

      if (seasonsCheck) {
        if (!coachingTeamsCheck) {
          if (nextProps.fetchingCoachTeamsStatus === 'Loaded' || fetchingCoachTeamsStatus === 'Loaded') {
            coachingTeamsCheck = true;
          } else if (nextProps.fetchingCoachTeamsStatus !== 'Loading' && fetchingCoachTeamsStatus !== 'Loading') {
            getCoachesTeams(memberId, seasonsList[0]);
          }
        }
      }

      let newFilter = this.state.filter;
      console.log(nextProps.mobile);

      if(nextProps.mobile.page === 'home'){
        newFilter = nextProps.mobile; 
      }

      console.log(this.state);

      this.setState({
        ...this.state,
        filter: newFilter, 
        additionalGamesLoaded: additionalGamesCheck,
      });
    }
  }

  isAdmin = () => {
    return this.props.userType === 'Admin';
  }

  isUmpire = () => {
    return this.props.umpireLevel !== null && this.props.umpireLevel > 0;
    //return false;
  }

  isCoach = () => {
    return this.props.teamsCoaching.length > 0;
    //return false;
  }

  defaultGamesLoaded = () => {
    const { fetchingGamesStatus } = this.props;
    return fetchingGamesStatus === 'Loaded' || fetchingGamesStatus === 'Error';
  }

  filterGamesLoaded = () => {
    const { fetchingFilteredGamesStatus } = this.props;
    return fetchingFilteredGamesStatus === 'Loaded' || fetchingFilteredGamesStatus === 'Error';
  }

  areSeasonsLoaded = () => {
    const { fetchingSeasonsStatus } = this.props;
    return fetchingSeasonsStatus === 'Loaded' || fetchingSeasonsStatus === 'Error';
  }

  areUmpiresLoaded = () => {
    const { umpFetchStatus } = this.props;
    return umpFetchStatus === 'Loaded' || umpFetchStatus === 'Error';
  }

  teamsCoachingLoaded = () => {
    const { fetchingCoachTeamsStatus } = this.props;
    return fetchingCoachTeamsStatus === 'Loaded' || fetchingCoachTeamsStatus === 'Error';
  }

  handleAddDate = e => {
    e.preventDefault();
    const { startDate, value, showAllGames } = this.state.filter;
    const newSelectedDate = format(addDays(startDate, value === 7 ? 8 : 1), 'MM/DD/YYYY');
    this.loadDefaultGamesFromDates(newSelectedDate, value);

    this.props.updateMobileState({
      startDate: newSelectedDate,
      value: value,
      page: 'home',
      showAllGames: showAllGames
    });

    this.setState({
      ...this.state,
      defaultGamesLoaded: false,
      displayAdditionalGames: false
    });
  };

  handleSubtractDate = e => {
    e.preventDefault();
    const { startDate, value, showAllGames } = this.state.filter;
    const newSelectedDate = format(subDays(startDate, value === 7 ? 8 : 1), 'MM/DD/YYYY');
    this.loadDefaultGamesFromDates(newSelectedDate, value);

    this.props.updateMobileState({
      startDate: newSelectedDate,
      value: value,
      page: 'home',
      showAllGames: showAllGames
    });

    this.setState({
      ...this.state,
      defaultGamesLoaded: false,
      displayAdditionalGames: false
    });
  };

  handleDateDisplay = () => {
    const { startDate, value } = this.props.mobile;

    if (this.defaultGamesLoaded()) {
      let nowDate = new Date(startDate);
      let startDay = nowDate.getUTCDate();
      let startMonth = nowDate.getUTCMonth() + 1;

      let nextDate = new Date(addDays(nowDate, value));
      let endDay = nextDate.getUTCDate();
      let endMonth = nextDate.getUTCMonth() + 1;

      nowDate = format(nowDate, 'MM/DD/YYYY');
      nextDate = format(nextDate, 'MM/DD/YYYY');

      if (value === 7) {
        return (
          <div>
            {startMonth}/{startDay} - {endMonth}/{endDay}
          </div>
        );
      } else {
        return <div>{nowDate}</div>;
      }
    }
  };

  handleMobileFilter = event => {
    let newFilterValue;
    switch (event.target.value) {
      case '7':
        newFilterValue = 7;
        break;
      case '1':
        newFilterValue = 1;
        break;
      default:
        newFilterValue = this.state.filter.value;
        break;
    }

    this.props.updateMobileState({...this.state.filter, value: newFilterValue});
    this.setState( 
      (prevState, props) => ({
      ...prevState,
      filter:{
        ...this.state.filter,
        value: newFilterValue, 
      },
      filterApplied: false 
      }),
      this.loadDefaultGamesFromDates(this.state.filter.startDate, newFilterValue)
    );    
  };

  handleCheckboxChange = event => {
    const { checked } = event.target;
    const date = this.state.filter.startDate === '' ? Date.now() : this.state.filter.startDate;
    const startDate = format(date, 'MM/DD/YYYY');
    const endDate = format(addDays(startDate, this.state.filter.value), 'MM/DD/YYYY');

    if(checked){
      this.props.filterGames(startDate, endDate, this.props.seasonsList[0], '', '', '', true, false);
    } else {
      this.props.getGames(startDate, 25, null, null);
    }
    const mobile = {
      startDate: date,
      value: this.state.filter.value,
      showAllGames: checked,
      page: 'home'
    };
    this.props.updateMobileState(mobile);
    this.setState({ ...this.state, showAllGames: checked });
  };

  loadDefaultGames = () => {
    const { getGames, seasonsList } = this.props;
    const { startDate, value } = this.state.filter;
    const date = startDate === '' ? Date.now() : startDate;
    const newSelectedDate = format(date, 'MM/DD/YYYY');
    const numberOfGamesToLoad = 25;

    if(!this.state.showAllGames){
      getGames(newSelectedDate, numberOfGamesToLoad, null, null);
    } else {
      const endDate = format(addDays(newSelectedDate, value), 'MM/DD/YYYY');
      this.props.filterGames(newSelectedDate, endDate, this.areSeasonsLoaded() ? seasonsList[0] : '', '', '', '', true, false);
    }
    this.setState({ ...this.state, additionalGamesLoaded: false, displayAdditionalGames: false });
  };

  loadDefaultGamesFromDates = (dateToUse, dateRange) => {
    const { getGames, seasonsList } = this.props;

    console.log(dateRange);

    const newSelectedDate = format(dateToUse, 'MM/DD/YYYY');
    const numberOfGamesToLoad = 25;

    if(!this.state.filter.showAllGames){
      getGames(newSelectedDate, numberOfGamesToLoad, null, null);
    } else {
      const endDate = format(addDays(newSelectedDate, dateRange), 'MM/DD/YYYY');
      this.props.filterGames(newSelectedDate, endDate, this.areSeasonsLoaded() ? seasonsList[0] : '', '', '', '', true, false);
    }
    this.setState({ ...this.state, additionalGamesLoaded: false, displayAdditionalGames: false });
  };

  loadAdditionalGames = () => {
    const { getUpdatedGames } = this.props;

    const newSelectedDate = format(this.state.filter.startDate, 'MM/DD/YYYY');
    const numberOfGamesToLoad = 120;

    getUpdatedGames(newSelectedDate, numberOfGamesToLoad, null, null);

    this.setState({ ...this.state, displayAdditionalGames: true });
  };

  checkForUmpire(object) {
    const maxUmps = object.umpAvailable !== null ? object.umpAvailable : 2;
    if (maxUmps <= object.umpsUsed) {
      return 'mobile-row-blue';
    } else if (object.umpsUsed > 0) {
      return 'mobile-row-yellow';
    } else {
      return 'mobile-row-red';
    }
  }

  calculateRowDate(object) {
    let objectDate = new Date(format(object.startDate, 'MM/DD/YYYY'));
    let daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    let weekDay = daysOfWeek[objectDate.getUTCDay()];
    let numberDay = objectDate.getUTCDate();
    return (
      <div className="row-date">
        <h2>{numberDay}</h2>
        <h4>{weekDay}</h4>
      </div>
    );
  }

  checkGameDates = (gameId, games) => {
    const { value, startDate } = this.state.filter;
    let startFilterDate = new Date(format(startDate, 'MM/DD/YYYY'));
    let endFilterDate = new Date(format(addDays(startFilterDate, value === 7 ? 8 : 1), 'MM/DD/YYYY'));

    let id = gameId;

    let gameStartDate = new Date(games[id].startDate);
    if (gameStartDate.getTime() < endFilterDate.getTime() && gameStartDate.getTime() >= startFilterDate.getTime()) {
      return (
        <div className="mobile-row-container" key={games[id].id}>
          {this.calculateRowDate(games[id])}
          <div className={this.checkForUmpire(games[id])} key={`upcomingGames-${id}`}>
            <div className="mobile-game-info-container" id={id}>
              <div className="sub-row">
                <Link to={`/selected-game/${gameId}`}>
                  <h3>{`${format(games[id].startDate, 'h:mm a')} @ ${games[id].facilityName}`}</h3>
                  <h4>
                    <span>{`${games[id].divisionName}`}</span>
                    {`${games[id].homeTeamName} (H) vs ${games[id].awayTeamName} (A)`}
                  </h4>
                </Link>
              </div>
            </div>
          </div>
        </div>
      );
    }
  };

  render() {
    const {
      games,
      gamesList,
      filteredGamesList
    } = this.props;

    let gamesListToDisplay = null;
    if (this.defaultGamesLoaded()) {
      gamesListToDisplay = this.state.filter.showAllGames ? filteredGamesList : gamesList;
    }

    return (
      <div className="home-mobile-container">
        <div className="form-container-home-upcoming-games">
          <div className="section-heading">
            <div className="section-title">
              <h2 className="left">UPCOMING GAMES</h2>
              <div>
                <label className="pt-label pt-inline checkbox-display-full-games">
                  Show All Games:
                  <input
                    type="checkbox"
                    value={this.state.filter.showAllGames}
                    onChange={this.handleCheckboxChange}
                    checked={this.state.filter.showAllGames}
                  />
                </label>
                <select onChange={this.handleMobileFilter} value={this.state.filter.value}>
                  <option value={this.state.filterType[this.state.filter.value === 7 ? 'day' : 'week'].value} key="Select">
                    Select Filter
                  </option>
                  {Object.values(this.state.filterType).map(type => {
                    return (
                      <option 
                        value={type.value}
                        key={type.text}
                      >
                        {type.text}
                      </option>
                    );
                  })}
                </select>
              </div>
            </div>
            <div className="container-date-picker">
              <button
                className="pt-button pt-minimal pt-icon-large pt-icon-chevron-left"
                onClick={this.handleSubtractDate}
              />
              <h2>{this.handleDateDisplay()}</h2>
              <button
                className="pt-button pt-minimal pt-icon-large pt-icon-chevron-right"
                onClick={this.handleAddDate}
              />
            </div>
          </div>
          <div className="mobile-games-container">
            {(this.defaultGamesLoaded() && !this.state.filter.showAllGames) || 
            (this.filterGamesLoaded() && this.state.filter.showAllGames) ? (
              <div>
                {gamesListToDisplay.length > 0 ? (
                  <div>
                    {gamesListToDisplay.map((id, index) => {
                      return <div key={games[id].id}>{this.checkGameDates(id, games)}</div>;
                    })}
                  </div>
                ) : (
                  <div className="mobile-empty-table"> No games during this period. </div>
                )}
              </div>
            ) : (
              <div className="mobile-empty-table"> Loading Games... </div>
            )}
          </div>
          {!this.state.displayAdditionalGames &&
          this.state.defaultGamesLoaded && 
          !this.state.showAllGames ? (
            <div className="load-additional-games-container">
              <a onClick={this.loadAdditionalGames}>Load More Games</a>
            </div>
          ) : (
            ''
          )}
        </div>
        {this.props.error !== null ? (
          <div>
            <ErrorPopup error={this.props.error} clearErrors={this.props.clearErrors} />
          </div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default HomeMobile;
