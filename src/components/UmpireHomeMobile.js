import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import MobileGameUmpire from './MobileGameUmpire';
import './HomeMobile.css';

class UmpireHomeMobile extends Component {
  state = {
    selectedGame: null,
    currentPage: 1,
    activeTop: true,
     selectedDate: [
      {
        month: "January",
      },
      {
        month: "February",
      },
      {
        month: "March",
      },
      {
        month: "April",
      },
      {
        month: "May", 
      },
      {
        month: "June", 
      },
      {
       month: "July", 
      },
      {
       month: "August", 
      },
      {
        month: "September", 
      },
      {
        month: "October",
       },
       {
        month: "November",
       }, 
       { 
        month: "December",
       }
    ],
  };

  // componentDidMount() {
  //   this.props.getUmpire();
  //   if (this.props.fetchingGames !== 'Loaded') {
  //     this.props.getGames('05/03/2017', 10, null, null);
  //   }
  // }

  handleGameSelect = selectedGame => {
    this.setState({ selectedGame });
    this.state.activeTop = false;
    //this.state.activeBot = true;
  };

  loadAdditionalGames = () => {
    const { games, gamesList, getGames } = this.props;
    const lastGameId = gamesList[gamesList.length - 1];
    const lastStartDate = format(games[lastGameId].startDate, 'MM-DD-YYYY HH:mm:ss');
    getGames('05-03-2017', 10, lastStartDate, lastGameId);
  };

  checkForUmpire(object){
    if(object.umpBase1Id != null && object.umpBase2Id != null && object.umpBase3Id != null && object.umpPlateId == null){
      return "mobile-row-blue";
    }
    else if(object.umpBase1Id != null || object.umpBase2Id != null || object.umpBase3Id != null || object.umpPlateId != null)
    {
      return "mobile-row-yellow";
    }
    else{
      return "mobile-row-red";
    }
  }

  calculateRowDate(object){
    let dateTest = new Date(format(object.startDate, 'YYYY/DD/MM'));
    dateTest = dateTest.toString();
    let weekDay = dateTest.split(' ')[0]
    let numberDay = dateTest.split(' ')[2]

    return (
    <div className="row-date">
      <h2>{numberDay}</h2>
      <h4>{weekDay}</h4>
    </div>
    );
  }

  backToGames(){
   // this.state.activeBot = false;
    this.setState({ activeTop: true });
    this.setState({ selectedGame: null });
  }

  classDetermine(){
    return (
    this.state.activeTop ? ("form-container-home-upcoming-games") : ("notActive")
    );
  }

  render() {
    const {
      games,
      gamesList,
      fetchingGames,
      umpires,
      umpiresList,
      umpiresFetchStatus,
      userType,
      memberId,
      assignUmpToGame,
      removeUmp
    } = this.props;
    const { selectedGame, activeTop } = this.state;
    return (
      <div className="home-mobile-container">
        <div className={this.classDetermine()}>
          <div className="section-heading">
            <h2 className="left">MY GAMES</h2>
            {/* <div class="profile-page-add-button"> */}
              <button class="add-game-button">Add Game</button>
            {/* </div> */}
          </div>
          <div className="mobile-games-container">
            {/* {games.map((id, index) => {
              return (
                <div className="mobile-row-container">
                {this.calculateRowDate(games[id])}
                <div className={this.checkForUmpire(games[id])} key={`upcomingGames-${id}`}>
                  <div className="mobile-game-info-container" id={id} onClick={() => this.handleGameSelect(id)}>
                    <div  className="sub-row">
                      <h3>
                        {`${format(games[id].startDate, 'h:mm a')}`}
                      </h3>
                      <h4>
                        <span>{`${games[id].divisionName}`}</span> 
                        {`${ games[id].facilityName}`}
                      </h4>
                    </div>
                  </div>
                </div>
              </div>
              );
            })} */}
          </div>
          <div className="load-additional-games-container">
            <a onClick={this.loadAdditionalGames}>Load More Games</a>
          </div>
        </div>
        <div className="form-container-home-game-details">
          {/* <div className="section-heading">
            <h2 className="left">GAME DETAILS</h2>
          </div> 
                      // <div className="mobile-game-details-container">
            //   <h3>Select a Game</h3>
            // </div>
          */}
          {selectedGame === null ? (
            <div/>
          ) : (
            <div className="mobile-game-details-container">
              <div className="row">
              <button className="submit-button-blue" onClick={() => this.backToGames()}>Back to games</button>
              </div>
              <div className="row">
                <h3 className="left">{games[selectedGame].divisionName}</h3>
              </div>
              <div className="row">
                <h3 className="left">
                  {games[selectedGame].homeTeamName} (H) vs {games[selectedGame].awayTeamName} (A)
                </h3>
              </div>
              <div className="row">
                <h3 className="left">{games[selectedGame].facilityName}</h3>
              </div>
              <div className="row-game-details">
                <h3 className="left">Plate:</h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpPlate"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
              <div className="row-game-details">
                <h3 className="left">B1: </h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase1"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
              <div className="row-game-details">
                <h3 className="left">B2: </h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase2"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
              <div className="row-game-details">
                <h3 className="left">B3:</h3>
                <MobileGameUmpire
                  games={games}
                  gameId={selectedGame}
                  umpires={umpires}
                  umpiresList={umpiresList}
                  umpiresFetchStatus={umpiresFetchStatus}
                  userType={userType}
                  memberId={memberId}
                  position="umpBase3"
                  removeUmp={removeUmp}
                  assignUmpToGame={assignUmpToGame}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default UmpireHomeMobile;
