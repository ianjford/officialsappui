import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { Checkbox, NumericInput } from '@blueprintjs/core';
//import debounce from 'lodash.debounce';
import TableSortPopover from './TableSortPopover';
import './UmpiresList.css';

class UmpiresList extends Component {
  state = {
    currentSeason: null,
    sortList: false,
    sortedUmpList: [],
    renderActiveUmps: true
  };

  componentDidMount() {
    const { getUmpiresList, fetchingUmpiresList, seasonsList } = this.props;
    if (fetchingUmpiresList !== 'Loading' || fetchingUmpiresList !== 'Loaded') {
      getUmpiresList();
    }
    if (seasonsList.length) {
      this.setState({ currentSeason: seasonsList[0] });
    }
  }

  handleCheckboxChange = e => {
    const { id, value } = e.target;
    const [memberId, column] = id.split('-');
    const prevValue =
      column === 'isActive'
        ? this.props.umpires[memberId][column]
        : this.props.umpires[memberId][this.state.currentSeason][column];
    let booleanVal;
    if (prevValue === true && value === 'on') {
      booleanVal = false;
    } else {
      booleanVal = value === 'on' ? true : false;
    }
    if (column === 'isActive') {
      this.props.updateUmp(memberId, null, null, booleanVal, this.state.currentSeason);
    } else if (column === 'isMentor') {
      this.props.updateUmp(memberId, null, booleanVal, null, this.state.currentSeason);
    }
  };

  handleUmpLevelChange = (memberId, newUmpLevel) => {
    this.props.updateUmp(memberId, newUmpLevel, null, null, this.state.currentSeason);
  };

  handleSeasonChange = e => {
    const { value } = e.target;
    const { seasonsList, seasons, getUmpiresList } = this.props;
    let selectedSeasonId;
    seasonsList.forEach(seasonId => {
      if (seasons[seasonId].name === value) {
        selectedSeasonId = seasons[seasonId].seasonId;
      }
    });

    this.setState({ currentSeason: selectedSeasonId });
    getUmpiresList(selectedSeasonId);
  };

  handleSort = (columnToSortBy, dataType, sortMethod) => {
    const { umpires, umpiresList } = this.props;
    const { currentSeason } = this.state;
    let sortedUmpList = [];
    let tempUmpList = [];
    tempUmpList = umpiresList.map(umpId => umpires[umpId]);
    sortedUmpList =
      dataType === 'string'
        ? tempUmpList.sort((a, b) => {
            let nameA = a[columnToSortBy].toLowerCase();
            let nameB = b[columnToSortBy].toLowerCase();
            if (sortMethod === 'asc') {
              if (nameA < nameB) {
                return -1;
              } else if (nameA > nameB) {
                return 1;
              } else {
                return 0;
              }
            } else {
              if (nameA < nameB) {
                return 1;
              } else if (nameA > nameB) {
                return -1;
              } else {
                return 0;
              }
            }
          })
        : tempUmpList.sort((a, b) => {
            if (sortMethod === 'asc') {
              return a[currentSeason][columnToSortBy] - b[currentSeason][columnToSortBy];
            } else {
              return b[currentSeason][columnToSortBy] - a[currentSeason][columnToSortBy];
            }
          });
    this.setState({ ...this.state, sortedUmpList, sortList: true });
  };

  handleMentorSort = (columnToSortBy, dataType, renderOnlyMentors) => {
    const { sortList, currentSeason } = this.state;
    const { umpires, umpiresList } = this.props;
    if (renderOnlyMentors === false && sortList) {
      this.setState({ ...this.state, sortList: false });
    } else {
      let sortedUmpList = [];
      umpiresList.forEach(umpId => {
        if (umpires[umpId][currentSeason].isMentor) {
          sortedUmpList.push(umpires[umpId]);
        }
      });
      this.setState({ ...this.state, sortList: true, sortedUmpList });
    }
  };

  handleActiveSort = (columnToSortBy, dataType, renderActive) => {
    const { renderActiveUmps } = this.state;
    if (renderActive && renderActiveUmps) {
      return;
    } else {
      this.setState({ ...this.state, renderActiveUmps: renderActive });
    }
  };

  render() {
    const { umpires, umpiresList, seasons, seasonsList } = this.props;
    const { currentSeason, renderActiveUmps, sortedUmpList, sortList } = this.state;
    return (
      <div className="report-list-container-main umpires-list-container">
        <div className="report-list-table-header">
          <h1 className="left">Umpire Table</h1>
          <div className="umpires-list-season-container">
            <h3>Season:</h3>
            <select className="pt-select" onChange={this.handleSeasonChange}>
              {seasonsList.map(seasonId => {
                return (
                  <option id={seasonId} key={seasonId}>
                    {seasons[seasonId].name}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <table className="umpires-list List-Table">
          <thead>
            <tr>
              <th className="large">Member ID</th>
              <th className="large tableLeft">First Name</th>
              <th className="large tableLeft">
                <TableSortPopover
                  title="Last Name"
                  dataType="string"
                  umpireObjKey="lastName"
                  boolButtonTitles={null}
                  handleSort={this.handleSort}
                />
              </th>
              <th className="large">
                <TableSortPopover
                  title="Umpire level"
                  dataType="number"
                  umpireObjKey="umpireLevel"
                  boolButtonTitles={null}
                  handleSort={this.handleSort}
                />
              </th>
              <th className="small">
                <TableSortPopover
                  title="Mentor"
                  dataType="boolean"
                  umpireObjKey="isMentor"
                  boolButtonTitles={['Mentors', 'All']}
                  handleSort={this.handleMentorSort}
                />
              </th>
              <th className="small">Total Games Plate</th>
              <th className="small">Total Games Base</th>
              <th className="small">
                <TableSortPopover
                  title="Active"
                  dataType="boolean"
                  umpireObjKey="isActive"
                  boolButtonTitles={['Active', 'All']}
                  handleSort={this.handleActiveSort}
                />
              </th>
            </tr>
          </thead>
          <tbody>
            {currentSeason === null ? (
              <tr />
            ) : sortList ? (
              sortedUmpList.map(ump => {
                const { isActive } = umpires[ump.memberId];
                const { isMentor, umpireLevel } = umpires[ump.memberId][currentSeason];
                let umpBaseTotal =
                  umpires[ump.memberId][currentSeason].umpBase1Total +
                  umpires[ump.memberId][currentSeason].umpBase2Total +
                  umpires[ump.memberId][currentSeason].umpBase3Total;
                if (renderActiveUmps === false) {
                  return (
                    <tr className="row-highlighter" key={ump.memberId}>
                      <td className="selection-highlighter large">{ump.memberId}</td>
                      <td className="selection-highlighter large tableLeft">{ump.firstName}</td>
                      <td className="selection-highlighter large tableLeft">{ump.lastName}</td>
                      <td className="selection-highlighter large">
                        <NumericInput
                          buttonPosition="none"
                          min={0}
                          max={4}
                          minorStepSize={1}
                          majorStepSize={1}
                          value={umpireLevel}
                          onValueChange={valueAsNumber => this.handleUmpLevelChange(ump.memberId, valueAsNumber)}
                        />
                      </td>
                      <td className="selection-highlighter checkbox small">
                        <Checkbox
                          id={`${ump.memberId}-isMentor`}
                          checked={isMentor}
                          onChange={this.handleCheckboxChange}
                        />
                      </td>
                      <td className="selection-highlighter small">{ump[currentSeason].umpPlateTotal}</td>
                      <td className="selection-highlighter small">{umpBaseTotal}</td>
                      <td className="selection-highlighter checkbox small">
                        <Checkbox
                          id={`${ump.memberId}-isActive`}
                          checked={isActive}
                          onChange={this.handleCheckboxChange}
                        />
                      </td>
                    </tr>
                  );
                } else {
                  if (isActive) {
                    return (
                      <tr className="row-highlighter" key={ump.memberId}>
                        <td className="selection-highlighter large">{ump.memberId}</td>
                        <td className="selection-highlighter large tableLeft">{ump.firstName}</td>
                        <td className="selection-highlighter large tableLeft">{ump.lastName}</td>
                        <td className="selection-highlighter large">
                          <NumericInput
                            buttonPosition="none"
                            min={0}
                            max={4}
                            minorStepSize={1}
                            majorStepSize={1}
                            value={umpireLevel}
                            onValueChange={valueAsNumber => this.handleUmpLevelChange(ump.memberId, valueAsNumber)}
                          />
                        </td>
                        <td className="selection-highlighter checkbox small">
                          <Checkbox
                            id={`${ump.memberId}-isMentor`}
                            checked={isMentor}
                            onChange={this.handleCheckboxChange}
                          />
                        </td>
                        <td className="selection-highlighter small">{ump[currentSeason].umpPlateTotal}</td>
                        <td className="selection-highlighter small">{umpBaseTotal}</td>
                        <td className="selection-highlighter checkbox small">
                          <Checkbox
                            id={`${ump.memberId}-isActive`}
                            checked={isActive}
                            onChange={this.handleCheckboxChange}
                          />
                        </td>
                      </tr>
                    );
                  }
                }
              })
            ) : (
              umpiresList.map((umpId, index) => {
                const { isActive } = umpires[umpId];
                const { isMentor } = umpires[umpId][currentSeason];
                let umpBaseTotal =
                  umpires[umpId][currentSeason].umpBase1Total +
                  umpires[umpId][currentSeason].umpBase2Total +
                  umpires[umpId][currentSeason].umpBase3Total;
                if (renderActiveUmps === false) {
                  return (
                    <tr className="row-highlighter" key={umpires[umpId].memberId}>
                      <td className="selection-highlighter large">{umpires[umpId].memberId}</td>
                      <td className="selection-highlighter large tableLeft">{umpires[umpId].firstName}</td>
                      <td className="selection-highlighter large tableLeft">{umpires[umpId].lastName}</td>
                      <td className="selection-highlighter large">
                        <NumericInput
                          buttonPosition="none"
                          min={0}
                          max={4}
                          minorStepSize={1}
                          majorStepSize={1}
                          value={umpires[umpId][currentSeason].umpireLevel}
                          onValueChange={valueAsNumber =>
                            this.handleUmpLevelChange(umpires[umpId].memberId, valueAsNumber)
                          }
                        />
                      </td>
                      <td className="selection-highlighter checkbox small">
                        <Checkbox
                          id={`${umpires[umpId].memberId}-isMentor`}
                          checked={isMentor}
                          onChange={this.handleCheckboxChange}
                        />
                      </td>
                      <td className="selection-highlighter small">{umpires[umpId][currentSeason].umpPlateTotal}</td>
                      <td className="selection-highlighter small">{umpBaseTotal}</td>
                      <td className="selection-highlighter checkbox small">
                        <Checkbox
                          id={`${umpires[umpId].memberId}-isActive`}
                          checked={isActive}
                          onChange={this.handleCheckboxChange}
                        />
                      </td>
                    </tr>
                  );
                } else {
                  if (isActive) {
                    return (
                      <tr className="row-highlighter" key={umpires[umpId].memberId}>
                        <td className="selection-highlighter large">{umpires[umpId].memberId}</td>
                        <td className="selection-highlighter large tableLeft">{umpires[umpId].firstName}</td>
                        <td className="selection-highlighter large tableLeft">{umpires[umpId].lastName}</td>
                        <td className="selection-highlighter large">
                          <NumericInput
                            buttonPosition="none"
                            min={0}
                            max={4}
                            minorStepSize={1}
                            majorStepSize={1}
                            value={umpires[umpId][currentSeason].umpireLevel}
                            onValueChange={valueAsNumber =>
                              this.handleUmpLevelChange(umpires[umpId].memberId, valueAsNumber)
                            }
                          />
                        </td>
                        <td className="selection-highlighter checkbox small">
                          <Checkbox
                            id={`${umpires[umpId].memberId}-isMentor`}
                            checked={isMentor}
                            onChange={this.handleCheckboxChange}
                          />
                        </td>
                        <td className="selection-highlighter small">{umpires[umpId][currentSeason].umpPlateTotal}</td>
                        <td className="selection-highlighter small">{umpBaseTotal}</td>
                        <td className="selection-highlighter checkbox small">
                          <Checkbox
                            id={`${umpires[umpId].memberId}-isActive`}
                            checked={isActive}
                            onChange={this.handleCheckboxChange}
                          />
                        </td>
                      </tr>
                    );
                  }
                }
              })
            )}
          </tbody>
        </table>
      </div>
    );
  }
}

export default UmpiresList;
