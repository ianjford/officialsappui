import React, { Component } from 'react';
import MentorContact from './MentorContact';
import Mentees from './Mentees';
import './MentorMenteeCard.css';

class MentorMenteeCard extends Component {
  state = {
    isHidden: true,
    hasError: false
  };

  componentDidMount() {
    const { memberId, seasonsList, userType, getMentorMenteeInfo } = this.props;
    const isHidden = userType !== 'umpire' && userType !== 'Admin' ? true : false;
    this.setState({ isHidden });
    if (!isHidden) {
      getMentorMenteeInfo(memberId, seasonsList[0]);
    }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    console.error({ error, info });
  }

  render() {
    const {
      isMentor,
      isMentee,
      mentees,
      mentorInfo,
      umpireFetchStatus,
      umpires,
      getUmpsNextGame,
      mentorMenteeInfoStatus
    } = this.props;
    const hiddenClass = this.state.isHidden ? 'hide' : '';

    if (this.state.hasError) {
      return <div className="mentor-mentee-loading pt-skeleton" />;
    } else {
      return (
        <div className={`mentor-mentee-card-container ${hiddenClass}`}>
          {isMentor ? (
            <Mentees
              mentees={mentees}
              umpireFetchStatus={umpireFetchStatus}
              umpires={umpires}
              getUmpsNextGame={getUmpsNextGame}
              mentorMenteeInfoStatus={mentorMenteeInfoStatus}
            />
          ) : isMentee ? (
            <MentorContact
              mentorInfo={mentorInfo}
              umpireFetchStatus={umpireFetchStatus}
              umpires={umpires}
              getUmpsNextGame={getUmpsNextGame}
            />
          ) : (
            <div className="mentor-mentee-loading pt-skeleton" />
          )}
        </div>
      );
    }
  }
}

export default MentorMenteeCard;
