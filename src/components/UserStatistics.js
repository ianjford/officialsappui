import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './UserStatistics.css';

class UserStatistics extends Component {
  state = {
    
  };

  componentDidMount() {
    this.props.getAllUmpireData();
  }

  render() {

    if(this.props.fetchingAllUmpireDataStatus === 'Loaded'){
      const user = this.props.umpires[this.props.memberId];
      if(user){
        return (
          <div className="user-statistics-container-main" >
            <div className="user-statistics-container-secondary">
              <div className="user-statistics-header">
                <h1>My Statistics</h1>
              </div>
              <div className="user-statistics-list">
                <div className="user-statistic">
                  <div className="user-statistic-title">
                    Umpire Level
                  </div>
                  <div className="user-statistic-content">
                    {this.props.umpireLevel}
                  </div>
                </div>
                <div className="user-statistic">
                  <div className="user-statistic-title">
                    Games Completed
                  </div>
                  <div  className="user-statistic-content">
                    {Number(user.basesCompleted) + Number(user.plateCompleted)}
                  </div>
                </div>
                <div className="user-statistic">
                  <div className="user-statistic-title">
                    Games Scheduled
                  </div>
                  <div  className="user-statistic-content">
                    {Number(user.basesCompleted) + Number(user.plateCompleted)}
                  </div>
                </div>
                <div className="user-statistic">
                  <div className="user-statistic-title">
                    Plate
                  </div>
                  <div  className="user-statistic-content">
                    {user.plateCompleted}
                  </div>
                </div>
                <div className="user-statistic">
                  <div className="user-statistic-title">
                    Base
                  </div>
                  <div className="user-statistic-content">
                    {user.basesCompleted}
                  </div>
                </div>
                <div className="user-statistic">
                  <div className="user-statistic-title">
                    Late Drops
                  </div>
                  <div className="user-statistic-content">
                    {user.lateDrops}
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      } else {
        return (
          <div className="user-statistics-container-main" >
            <div className="user-statistics-container-secondary">
              <div className="user-statistics-header">
                <h1>My Statistics</h1>
              </div>
              <div className="user-statistics-list">
                No Umpire Data
              </div>
            </div>
          </div>
        );         
      }
    } else {
      return (
        <div className="user-statistics-container-main" >
          <div className="user-statistics-container-secondary">
            <div className="user-statistics-header">
              <h1>My Statistics</h1>
            </div>
            <div className="user-statistics-list">
              Loading Data...
            </div>
          </div>
        </div>
      );
    }
  }
}

export default UserStatistics;
