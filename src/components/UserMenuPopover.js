import React from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';
import './UserMenuPopover.css';

const UserMenuPopover = ({ handleLogout, history, match }) => {
  return (
    <Menu>
      <MenuItem
        className="user-profile-link"
        text="Profile"
        iconName="pt-icon-person"
        onClick={() => history.push('/profile')}
      />
      <MenuItem className="logout" text="Log Out" iconName="pt-icon-log-out" onClick={handleLogout} />
    </Menu>
  );
};

export default UserMenuPopover;
