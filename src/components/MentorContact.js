import React, { Component } from 'react';
import format from 'date-fns/format';
import './MentorContact.css';

class MentorContact extends Component {
  componentDidMount() {
    const { mentorInfo, getUmpsNextGame } = this.props;
    if (
      mentorInfo.mentorId !== null &&
      (mentorInfo.fetchingNextGame !== 'Loading' || mentorInfo.fetchingNextGame !== 'Loaded')
    ) {
      getUmpsNextGame(mentorInfo.mentorId, 'mentor');
    }
  }

  render() {
    const { mentorInfo, umpires, umpireFetchStatus } = this.props;
    const { mentorId } = mentorInfo;
    return (
      <div className="mentor-container">
        <div className="header">
          <h1>My Mentor</h1>
        </div>
        {umpireFetchStatus === 'Loaded' ? (
          <div className="mentor-info-container">
            <h3>{mentorId !== null && mentorId !== undefined ? umpires[mentorId].name : ''}</h3>
            <div className="flex-row">
              <h4>
                <span className="pt-icon-standard pt-icon-phone" />
                {mentorId !== null && mentorId !== undefined ? umpires[mentorId].phoneNumber : ''}
              </h4>
            </div>
            <div className="flex-row">
              <h4>
                <span className="pt-icon-standard pt-icon-envelope" />
                {mentorId !== null && mentorId !== undefined ? umpires[mentorId].email : ''}
              </h4>
            </div>
            <div className="flex-row">
              <h4>
                {mentorId === null || mentorInfo.nextGame.gameId === null
                  ? 'Next Game: NA'
                  : `Next Game: ${format(mentorInfo.nextGame.startDate, 'M/D h:mm a')} @ ${
                      mentorInfo.nextGame.facilityName
                    }`}
              </h4>
            </div>
          </div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default MentorContact;
