import React from 'react';
import './TableSortPopoverContent.css';

const TableSortPopoverContent = props => {
  const { dataType, umpireObjKey, handleSort, boolButtonTitles } = props;
  if (dataType === 'boolean') {
    return (
      <div className="table-sort-popover-container">
        <button className="pt-button pt-minimal" onClick={() => handleSort(umpireObjKey, dataType, true)}>
          {`Display ${boolButtonTitles[0]}`}
        </button>
        <button className="pt-button pt-minimal" onClick={() => handleSort(umpireObjKey, dataType, false)}>
          {`Display ${boolButtonTitles[1]}`}
        </button>
      </div>
    );
  } else {
    const iconClassAsc = dataType === 'string' ? 'pt-icon-sort-alphabetical' : 'pt-icon-sort-numerical';
    const iconClassDesc = dataType === 'string' ? 'pt-icon-sort-alphabetical-desc' : 'pt-icon-sort-numerical-desc';
    return (
      <div className="table-sort-popover-container">
        <button
          className={`pt-button pt-minimal ${iconClassAsc}`}
          onClick={() => handleSort(umpireObjKey, dataType, 'asc')}
        >
          Sort asc
        </button>
        <button
          className={`pt-button pt-minimal ${iconClassDesc}`}
          onClick={() => handleSort(umpireObjKey, dataType, 'desc')}
        >
          Sort desc
        </button>
      </div>
    );
  }
};

export default TableSortPopoverContent;
