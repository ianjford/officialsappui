import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './ReportTable.css';

class VolunteerStatusTable extends Component {
  state = {
    filtering: false,
    filters: {
      division: 'all',
      level: 'all'
    }
  };

  componentDidMount(){
    this.props.getAllVolunteerData();
    this.props.getDivisions();
  }

  getDivisionAbbreviation = divisionName =>{
    let output = '';
    switch(divisionName){
      case "Boys Pioneer 8":
        output = 'BP';
        break;
      case "Boys American 9-10":
        output = 'BA';
        break;
      case "Boys National 11-12":
        output = 'BN';
        break;
      case "Boys Western 13-14":
        output = 'BW';
        break;
      case "Girls Pioneer 7-8":
        output = 'GP';
        break;
      case "Girls American 9-10":
        output = 'GA';
        break;
      case "Girls National 11-12":
        output = 'GN';
        break;
      case "Girls Western 13-18":
        output = 'GW';
        break;
      default: 
        break; 
    }
    return output;
  }

  handleSelectChange = event => {
    const { id, value } = event.target;
    //let newValue = value === 'all' ? '' : value;
    let newDivision = this.state.filters.division;
    let newLevel = this.state.filters.level;
    if(id === 'division'){
      newDivision = value;
    } else if(id === 'level'){
      newLevel = value;
    }
    this.setState({ filtering: true, filters: { division: newDivision, level: newLevel }});
  };

  removeFilters = () => {
    this.setState({
      filtering: false,
      filters: {
        division: 'all',
        level: 'all'
      },
    });
  };

  render() {
    const{ 
      volunteers,
      fetchingAllVolunteerDataStatus,
      divisions,
      divisionsList,
      fetchingDivisionsStatus
    } = this.props;

    let outputList = [];

    const allVolunteers = Object.values(volunteers);

    if(fetchingAllVolunteerDataStatus === 'Loaded' && fetchingDivisionsStatus === 'Loaded'){
      if(this.state.filtering){
        let levelFilter = this.state.filters.level !== 'all' ? this.state.filters.level : 'all';
        let divisionFilter = this.state.filters.division !== 'all' ? this.state.filters.division : 'all';

        allVolunteers
        .filter(volunteer => ((levelFilter === 'all' || volunteer.umpireLevel === levelFilter) &&
            (divisionFilter === 'all' || volunteer.division === divisionFilter) && 
            volunteer.memberId > 0))
        .map(volunteer => {
            return outputList.push(volunteer);
        }); 
        
      } else {
        allVolunteers
        .filter(volunteer => volunteer.memberId > 0)
        .map(volunteer => {
          return outputList.push(volunteer);
        });
      }
    }
     
    return (
      <div className="report-list-container-main volunteer-report-container">
        <div className="report-list-table-header">
          <h1 className="left">Volunteer Status</h1>
          <div className="report-filter">
            <div className="table-filters-container">
              <label className="pt-label pt-inline">
                Division:
                <select
                  className="pt-input"
                  id="division"
                  value={this.state.filters.division}
                  onChange={this.handleSelectChange}
                >
                  <option key="divisions-all" value="all">
                    All Division
                  </option>
                  {divisionsList.map(divisionId => {
                    return (
                      <option key={divisionId} value={divisions[divisionId].name}>
                        {divisions[divisionId].name}
                      </option>
                    );
                  })}
                </select>
              </label>
            </div>
            <div className="table-filters-container">
              <label className="pt-label pt-inline">
                Level:
                <select
                  className="pt-input"
                  id="level"
                  value={this.state.filters.level}
                  onChange={this.handleSelectChange}
                >
                  <option key="level-all" value="all">
                    All Level
                  </option>
                  <option key="level-one" value="1">
                    1
                  </option>
                  <option key="level-two" value="2">
                    2
                  </option>
                  <option key="level-three" value="3">
                    3
                  </option>
                  <option key="level-four" value="4">
                    4
                  </option>
                </select>
              </label>
            </div>
          </div>
        </div>
        <table className="volunteer-report List-Table">
          <thead>
            <tr>
              <th className="large">Name</th>
              <th className="xsmall">Level</th>
              <th className="small">Team</th>
              <th className="xsmall">Late Drops</th>
              <th className="xsmall">Plate Completed</th>
              <th className="xsmall">Base Completed</th>
              <th className="small">Games <br/> Complete <br /> Committed</th>
            </tr>
          </thead>
          <tbody>
            {outputList.map((volunteer, index) => {
              return (
                <tr className="row-highlighter" key={index}>
                  <td className="selection-highlighter large tableLeft">{volunteer.firstName} {volunteer.lastName}</td>
                  <td className="selection-highlighter xsmall">{volunteer.umpireLevel}</td>
                  <td className="selection-highlighter small tableLeft">{this.getDivisionAbbreviation(volunteer.division)} {volunteer.team}</td>
                  <td className="selection-highlighter xsmall">{volunteer.lateDrops}</td>
                  <td className="selection-highlighter xsmall">{volunteer.plate}</td>
                  <td className="selection-highlighter xsmall">{volunteer.bases}</td>
                  <td className="selection-highlighter small">{volunteer.gamesCompleted} / {volunteer.gamesCommitted}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}


export default VolunteerStatusTable;
