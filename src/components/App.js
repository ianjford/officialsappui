import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import ProtectedRoute from './ProtectedRoute';
import Login from './Login';
import Register from './Register';
import Reset from './Reset';
import MainContainer from '../containers/MainContainer';
import './App.css';

class App extends Component {
  render() {
    if (this.props.checkingJWT) {
      return <div className="App" />;
    } else {
      return (
        <div className="App">
          <Switch>
            <Route
              exact
              path="/login"
              render={props => (
                <Login handleLogin={this.props.handleLoginSuccess} isLoggedIn={this.props.isLoggedIn} {...props} />
              )}
            />
            <Route
              exact
              path="/register"
              render={props => <Register handleLogin={this.props.handleLoginSuccess} {...props} />}
            />
            <Route path="/reset" render={props => <Reset handleLogin={this.props.handleLoginSuccess} {...props} />} />
            <ProtectedRoute path="/" component={MainContainer} isLoggedIn={this.props.isLoggedIn} />
          </Switch>
        </div>
      );
    }
  }
}

export default App;
