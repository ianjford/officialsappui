import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './CoachesList.css';

class CoachesList extends Component {
  state = {};

  componentDidMount() {
    const { fetchingCoachesList, getCoachesList } = this.props;
    if (fetchingCoachesList !== 'Loading' || fetchingCoachesList !== 'Loaded') {
      getCoachesList();
    }
  }

  render() {
    const { coachesList } = this.props;
    return (
      <div className="coaches-list-container report-list-container-main">
        <div className="report-list-table-header">
          <h1 className="left">Coaches Table</h1>
        </div>
        <table className="coaches-list List-Table">
          <thead>
            <tr>
              <th>Member ID</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Division</th>
              <th>Team</th>
            </tr>
          </thead>
          <tbody>
            {coachesList.map(coach => {
              return (
                <tr className="row-highlighter" key={`${coach.memberId}-${coach.teamName}`}>
                  <td className="selection-highlighter">{coach.memberId}</td>
                  <td className="selection-highlighter tableLeft">{coach.firstName}</td>
                  <td className="selection-highlighter tableLeft">{coach.lastName}</td>
                  <td className="selection-highlighter tableLeft">{coach.divisionName}</td>
                  <td className="selection-highlighter tableLeft">{coach.teamName}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default CoachesList;
