import React, { Component } from 'react';
import ComboBoxSearch from './ComboBoxSearch';
import './GameUmpire.css';

class GameUmpire extends Component {
  state = {
    selectedUmp: {},
    error: '',
  };

  handleRemoveUmp = (umpireId, gameId, plate) => {
    this.props.removeUmp(umpireId, gameId, plate);
  };

  handleSelectUmp = (selectedOption, gameId, position) => {
    if (selectedOption) {
      const { value, label } = selectedOption;
      this.setState((prevState, props) => ({
        selectedUmp: {
          [position]: {
            name: label,
            umpireId: value,
            gameId,
            position
          }
        }
      }));
    } else {
      this.setState({ selectedUmp: { [position]: null } });
    }
  };

  handleAssignUmp = (gameId, position) => {
    if (this.state.selectedUmp[position]) {
      const { umpireId } = this.state.selectedUmp[position];
      if(umpireId === this.props.games[gameId].umpPlateId || umpireId === this.props.games[gameId].umpBase1Id || 
        umpireId === this.props.games[gameId].umpBase2Id || umpireId === this.props.games[gameId].umpBase3Id ){
        this.setState({ error: 'Umpire is already assigned to a plate in this game.'});
      } else {
        this.props.assignUmpToGame(umpireId, gameId, position);
      }
    } else {
      this.setState({ error: 'Please select an umpire.' });
    }
  };

  handleUmpSignUp = (memberId, gameId, position) => {
    this.props.assignUmpToGame(memberId, gameId, position);
  };

  render() {
    const { games, gameId, umpiresList, isUmpire, isAdmin, isCoach, memberId } = this.props;
    let assignedToPlate = (games[gameId].umpPlateId === memberId);
    let assignedToBase1 = (games[gameId].umpBase1Id === memberId);
    let assignedToBase2 = (games[gameId].umpBase2Id === memberId);
    let assignedToBase3 = (games[gameId].umpBase3Id === memberId);
    let alreadyAssignedThisGame = assignedToBase3 || assignedToBase2 || assignedToBase1 || assignedToPlate;
    let assignableUmpires = (games[gameId].umpAvailable === null || games[gameId].umpAvailable === '') ? 2 : games[gameId].umpAvailable;
    
    if(isAdmin){
      return (
        <div className="game-umpires-container">
          <div className="umpPlate-container">
            {games[gameId].umpPlateId === null ? (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                <ComboBoxSearch
                  key="top"
                  placeholder="Type to search umpires"
                  items={umpiresList}
                  onItemChange={selectedOption => this.handleSelectUmp(selectedOption, gameId, 'umpPlate')}
                />
                <button
                  className="pt-button pt-intent-primary"
                  onClick={() => this.handleAssignUmp(gameId, 'umpPlate')}
                >
                  Assign
                </button>
              </div>
            ) : (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                {games[gameId].umpPlateName !== null && games[gameId].umpPlateName !== ' ' ? (
                  <div className="ump-name">
                    {`${games[gameId].umpPlateName}`}
                    <span>{`(L${games[gameId].umpPlateLevel})`}</span>
                  </div>
                ) : (
                  <div className="ump-name">No umpire</div>
                )}
                <button
                  className="pt-button pt-intent-danger"
                  onClick={() => this.handleRemoveUmp(games[gameId].umpPlateId, gameId, 'umpPlate')}
                >
                  X
                </button>
               
              </div>
            )}
          </div>
          <div className="umpBase1-container">
            {games[gameId].umpBase1Id === null ? (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                <ComboBoxSearch
                  key="top"
                  placeholder="Type to search umpires"
                  items={umpiresList}
                  onItemChange={selectedOption => this.handleSelectUmp(selectedOption, gameId, 'umpBase1')}
                />
                <button
                  className="pt-button pt-intent-primary"
                  onClick={() => this.handleAssignUmp(gameId, 'umpBase1')}
                >
                  Assign
                </button>
              </div>
            ) : (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                {games[gameId].umpBase1Name !== null ? (
                  <div className="ump-name">
                    {`${games[gameId].umpBase1Name}`}
                    <span>{`(L${games[gameId].umpBase1Level})`}</span>
                  </div>
                ) : (
                  <div className="ump-name">No umpire</div>
                )} 
                <button
                  className="pt-button pt-intent-danger"
                  onClick={() => this.handleRemoveUmp(games[gameId].umpBase1Id, gameId, 'umpBase1')}
                >
                  X
                </button>
              </div>
            )}
          </div>
          {assignableUmpires >= 3 ? (
            <div className="umpBase2-container">
              {games[gameId].umpBase2Id === null ? (
                <div className="umpBase2-innerContainer">
                  <h3>B2:</h3>
                  <ComboBoxSearch
                    key="top"
                    placeholder="Type to search umpires"
                    items={umpiresList}
                    onItemChange={selectedOption => this.handleSelectUmp(selectedOption, gameId, 'umpBase2')}
                  />
                  <button
                    className="pt-button pt-intent-primary"
                    onClick={() => this.handleAssignUmp(gameId, 'umpBase2')}
                  >
                    Assign
                  </button>
                </div>
              ) : (
                <div className="umpBase2-innerContainer">
                  <h3>B2:</h3>
                  {games[gameId].umpBase2Name !== null ? (
                    <div className="ump-name">
                      {`${games[gameId].umpBase2Name}`}
                      <span>{`(L${games[gameId].umpBase2Level})`}</span>
                    </div>
                  ) : (
                    <div className="ump-name">No umpire</div>
                  )}
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase2Id, gameId, 'umpBase2')}
                  >
                    X
                  </button>
                </div>
              )}
            </div>
          ): ('')}
          {assignableUmpires >= 4 ? (
            <div className="umpBase3-container">
              {games[gameId].umpBase3Id === null ? (
                <div className="umpBase3-innerContainer">
                  <h3>B3:</h3>
                  <ComboBoxSearch
                    key="top"
                    placeholder="Type to search umpires"
                    items={umpiresList}
                    onItemChange={selectedOption => this.handleSelectUmp(selectedOption, gameId, 'umpBase3')}
                  />
                  <button
                    className="pt-button pt-intent-primary"
                    onClick={() => this.handleAssignUmp(gameId, 'umpBase3')}
                  >
                    Assign
                  </button>
                </div>
              ) : (
                <div className="umpBase3-innerContainer">
                  <h3>B3:</h3>
                  {games[gameId].umpBase3Name !== null ? (
                    <div className="ump-name">
                      {`${games[gameId].umpBase3Name}`}
                      <span>{`(L${games[gameId].umpBase3Level})`}</span>
                    </div>
                  ) : (
                    <div className="ump-name">No umpire</div>
                  )}
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase3Id, gameId, 'umpBase3')}
                  >
                    X
                  </button>
                </div>
              )}
            </div>
          ): ('')}
        </div>
      );
    } else if(isUmpire){
      return(
        <div className="game-umpires-container">
          <div className="umpPlate-container">
            {games[gameId].umpPlateId === null ? (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                <div className="ump-name">No umpire</div>
                {!alreadyAssignedThisGame ? (
                  <button
                    className="pt-button pt-intent-primary"
                    onClick={() => this.handleUmpSignUp(memberId, gameId, 'umpPlate')}
                  >
                    Sign Up
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            ) : (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                <div className="ump-name">
                  {`${games[gameId].umpPlateName}`}
                  <span>{`(L${games[gameId].umpPlateLevel})`}</span>
                </div>
                {games[gameId].umpPlateId === memberId ? (
                  <div>
                    <button
                      className="pt-button pt-intent-danger"
                      onClick={() => this.handleRemoveUmp(games[gameId].umpPlateId, gameId, 'umpPlate')}
                    >
                      X
                    </button>
                  </div>
                  ) : (
                    <div className="button-placeholder" />
                  )}
              </div>
            )}
          </div>
          <div className="umpBase1-container">
            {games[gameId].umpBase1Id === null ? (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                <div className="ump-name">No umpire</div>
                {!alreadyAssignedThisGame ? (
                  <button
                    className="pt-button pt-intent-primary"
                    onClick={() => this.handleUmpSignUp(memberId, gameId, 'umpBase1')}
                  >
                    Sign Up
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            ) : (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                <div className="umpName">
                  {`${games[gameId].umpBase1Name}`}
                  <span>{`(L${games[gameId].umpBase1Level})`}</span>
                </div>
                {games[gameId].umpBase1Id === memberId ? (
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase1Id, gameId, 'umpBase1')}
                  >
                    X
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            )}
          </div>
          {assignableUmpires >= 3 ? (
          <div className="umpBase2-container">
            {games[gameId].umpBase2Id === null ? (
              <div className="umpBase2-innerContainer">
                <h3>B2:</h3>
                <div className="ump-name">No umpire</div>
                {!alreadyAssignedThisGame ? (
                  <button
                    className="pt-button pt-intent-primary"
                    onClick={() => this.handleUmpSignUp(memberId, gameId, 'umpBase2')}
                  >
                    Sign Up
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            ) : (
              <div className="umpBase2-innerContainer">
                <h3>B2:</h3>
                <div className="ump-name">
                  {`${games[gameId].umpPlateName}`}
                  <span>{`(L${games[gameId].umpPlateLevel})`}</span>
                </div>
                <div className="button-placeholder" />
              </div>
            )}
          </div>
          ): ('')}
          {assignableUmpires >= 4 ? (
          <div className="umpBase3-container">
            {games[gameId].umpBase3Id === null ? (
              <div className="umpBase3-innerContainer">
                <h3>B3:</h3>
                <div className="ump-name">No umpire</div>
                {!alreadyAssignedThisGame ? (
                  <button
                    className="pt-button pt-intent-primary"
                    onClick={() => this.handleUmpSignUp(memberId, gameId, 'umpBase3')}
                  >
                    Sign Up
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            ) : (
              <div className="umpBase3-innerContainer">
                <h3>B3:</h3>
                <div className="umpName">
                  {`${games[gameId].umpBase3Name}`}
                  <span>{`(L${games[gameId].umpBase3Level})`}</span>
                </div>
                {games[gameId].umpBase1Id === memberId ? (
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase1Id, gameId, 'umpBase3')}
                  >
                    X
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            )}
          </div>
          ) : ('')}
        </div>
      );
    } else if(isCoach){
      return (
        <div className="game-umpires-container">
          <div className="umpPlate-container">
            {games[gameId].umpPlateId === null ? (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                <div className="ump-name">
                  {`${games[gameId].umpPlateName}`}
                  <span>{`(L${games[gameId].umpPlateLevel})`}</span>
                </div>
                <div className="button-placeholder" />
              </div>
            )}
          </div>
          <div className="umpBase1-container">
            {games[gameId].umpBase1Id === null ? (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                <div className="umpName">
                  {`${games[gameId].umpBase1Name}`}
                  <span>{`(L${games[gameId].umpBase1Level})`}</span>
                </div>
                {games[gameId].umpBase1Id === memberId ? (
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase1Id, gameId, 'umpBase1')}
                  >
                    X
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            )}
          </div>
          {assignableUmpires >= 3 ? (
          <div className="umpBase2-container">
            {games[gameId].umpBase2Id === null ? (
              <div className="umpBase2-innerContainer">
                <h3>B2:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpBase2-innerContainer">
                <h3>B2:</h3>
                <div className="ump-name">
                  {`${games[gameId].umpPlateName}`}
                  <span>{`(L${games[gameId].umpPlateLevel})`}</span>
                </div>
                <div className="button-placeholder" />
              </div>
            )}
          </div>
          ): ('')}
          {assignableUmpires >= 4 ? (
          <div className="umpBase3-container">
            {games[gameId].umpBase3Id === null ? (
              <div className="umpBase3-innerContainer">
                <h3>B3:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpBase3-innerContainer">
                <h3>B3:</h3>
                <div className="umpName">
                  {`${games[gameId].umpBase3Name}`}
                  <span>{`(L${games[gameId].umpBase3Level})`}</span>
                </div>
                {games[gameId].umpBase1Id === memberId ? (
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase1Id, gameId, 'umpBase3')}
                  >
                    X
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            )}
          </div>
          ) : ('')}
        </div>
      );
    } else {
     return (
        <div className="game-umpires-container">
          <div className="umpPlate-container">
            {games[gameId].umpPlateId === null ? (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpPlate-innerContainer">
                <h3>P:</h3>
                <div className="ump-name">
                  {`${games[gameId].umpPlateName}`}
                  <span>{`(L${games[gameId].umpPlateLevel})`}</span>
                </div>
                <div className="button-placeholder" />
              </div>
            )}
          </div>
          <div className="umpBase1-container">
            {games[gameId].umpBase1Id === null ? (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpBase1-innerContainer">
                <h3>B1:</h3>
                <div className="umpName">
                  {`${games[gameId].umpBase1Name}`}
                  <span>{`(L${games[gameId].umpBase1Level})`}</span>
                </div>
                {games[gameId].umpBase1Id === memberId ? (
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase1Id, gameId, 'umpBase1')}
                  >
                    X
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            )}
          </div>
          {assignableUmpires >= 3 ? (
          <div className="umpBase2-container">
            {games[gameId].umpBase2Id === null ? (
              <div className="umpBase2-innerContainer">
                <h3>B2:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpBase2-innerContainer">
                <h3>B2:</h3>
                <div className="ump-name">
                  {`${games[gameId].umpPlateName}`}
                  <span>{`(L${games[gameId].umpPlateLevel})`}</span>
                </div>
                <div className="button-placeholder" />
              </div>
            )}
          </div>
          ): ('')}
          {assignableUmpires >= 4 ? (
          <div className="umpBase3-container">
            {games[gameId].umpBase3Id === null ? (
              <div className="umpBase3-innerContainer">
                <h3>B3:</h3>
                <div className="ump-name">No umpire</div>
                <div className="button-placeholder" />
              </div>
            ) : (
              <div className="umpBase3-innerContainer">
                <h3>B3:</h3>
                <div className="umpName">
                  {`${games[gameId].umpBase3Name}`}
                  <span>{`(L${games[gameId].umpBase3Level})`}</span>
                </div>
                {games[gameId].umpBase1Id === memberId ? (
                  <button
                    className="pt-button pt-intent-danger"
                    onClick={() => this.handleRemoveUmp(games[gameId].umpBase1Id, gameId, 'umpBase3')}
                  >
                    X
                  </button>
                ) : (
                  <div className="button-placeholder" />
                )}
              </div>
            )}
          </div>
          ) : ('')}
        </div>
      );
    }
  }
}

export default GameUmpire;
