import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Register.css';
import logoBlueCrew from './../assets/logoBlueCrew.png';

class Register extends Component {
  state = {
    email: '',
    firstName: '',
    lastName: '',
    phone: '',
    password1: '',
    password2: '',
    userType: '',
    error: '',
    errorInput: ''
  };

  handleEmail = event => {
    const email = event.target.value;
    if (this.state.errorInput === 'email' || this.state.errorInput === 'All') {
      this.setState((prevState, props) => ({ email, error: '', errorInput: '' }));
    }
    this.setState((prevState, props) => ({ email }));
  };

  handlePassword = event => {
    const { id, value } = event.target;
    if (this.state.errorInput === 'password' || this.state.errorInput === 'All') {
      this.setState((prevState, props) => ({ [id]: value, error: '', errorInput: '' }));
    }
    this.setState({ [id]: value });
  };

  handleName = event => {
    const { id, value } = event.target;
    this.setState((prevState, props) => ({ [id]: value, error: '', errorInput: '' }));
  };

  handlePhone = event => {
    const phone = event.target.value;
    this.setState((prevState, props) => ({ phone }));
  };

  handleSelect = event => {
    const userType = event.target.value;
    if (userType !== 'default') {
      this.setState((prevState, props) => ({ userType }));
    }
  };

  handleSubmit = event => {
    event.preventDefault();
    const { email, password1, password2, firstName, lastName, phone, userType } = this.state;
    if (email === '') {
      this.setState({ error: 'Please enter a valid email.', errorInput: 'email' });
      return;
    }
    if (password1 !== password2) {
      this.setState((prevState, props) => ({
        error: 'Passwords do not match.',
        errorInput: 'password'
      }));
      return;
    } else if (password1 === '' || password2 === '') {
      this.setState((prevState, props) => ({
        error: 'Please enter and confirm password.',
        errorInput: 'password'
      }));
      return;
    } else if (userType === '') {
      this.setState((prevState, props) => ({
        error: 'Please select a user type',
        errorInput: 'userType'
      }));
    }
    fetch('/api/user/register', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email,
        password: password1,
        firstName,
        lastName,
        phoneNumber: phone,
        userType
      })
    })
      .then(res => res.json())
      .then(res => {
        const { token, user } = res;
        window.localStorage.setItem('kodaro-token', token);
        this.props.handleLogin(token, user);
        if (user.userType === 'Admin' || user.userType === 'Parent') {
          this.props.history.push('/', { from: '/register' });
        } else if (user.userType === 'umpire' && user.umpireLevel > 0) {
          this.props.history.push('/', { from: '/register' });
        } else {
          this.props.history.push('/welcome', { from: '/login' });
        }
      })
      .catch(error => {
        this.setState({ error: 'Unable to register. Please retry or contact Kodaro support.' });
      });
  };

  render() {
    //const emailClass = this.state.errorInput === 'email' || this.state.errorInput === 'All' ? 'pt-intent-danger' : '';
    //const passwordClass = this.state.errorInput === 'password' || this.state.errorInput === 'All' ? 'pt-intent-danger' : '';

    return (
      <div className="register-container">
        <div className="form-container">
          <div className="register-header">
            <img src={logoBlueCrew} height="auto" width="200" alt="Kodaro logo" />
          </div>
          <div>
            <input
              className="login-inputs"
              type="text"
              ref={input => (this.emailInput = input)}
              onChange={this.handleEmail}
              value={this.state.email}
              placeholder="Email"
            />
          </div>
          <div>
            <input
              className="login-inputs"
              id="password1"
              type="password"
              ref={input => (this.password1Input = input)}
              onChange={this.handlePassword}
              value={this.state.password1}
              placeholder="Password"
            />
          </div>
          <div>
            <input
              className="login-inputs"
              id="password2"
              type="password"
              ref={input => (this.password2Input = input)}
              onChange={this.handlePassword}
              value={this.state.password2}
              placeholder="Confirm Password"
            />
          </div>
          <div>
            <input
              className="login-inputs"
              id="firstName"
              type="text"
              ref={input => (this.nameInput = input)}
              onChange={this.handleName}
              value={this.state.firstName}
              placeholder="First Name"
            />
          </div>
          <div>
            <input
              className="login-inputs"
              id="lastName"
              type="text"
              ref={input => (this.nameInput = input)}
              onChange={this.handleName}
              value={this.state.lastName}
              placeholder="Last Name"
            />
          </div>
          <div>
            <input
              className="login-inputs"
              type="text"
              ref={input => (this.phoneInput = input)}
              onChange={this.handlePhone}
              value={this.state.phone}
              placeholder="Mobile Phone Number"
            />
          </div>
          <div className="select-container">
            <select className="login-inputs" ref={select => (this.userType = select)} onChange={this.handleSelect}>
              <option value="default">I'm signing up as...</option>
              <option value="umpire">Umpire</option>
              <option value="coach">Coach</option>
              <option value="member">Member</option>
            </select>
          </div>
          <div className="register-error">
            <p>{this.state.error}</p>
          </div>
          <div className="submit-btn-container-blue">
            <button className="submit-button-blue" onClick={this.handleSubmit}>
              Sign Up
            </button>
          </div>
          <div className="login-footer">
            <Link to="/login">I have an account</Link>
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
