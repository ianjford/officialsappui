import React from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';
import './ListsMenuPopOver.css';

const ListsMenuPopover = ({ handleLogout, history, location, match }) => {
  return (
    <Menu>
      <MenuItem text="Coaches" onClick={() => history.push('/list/coaches', { from: location.pathname })} />
      <MenuItem text="Mentors" onClick={() => history.push('/list/mentors', { from: location.pathname })} />
      <MenuItem text="Members" onClick={() => history.push('/list/members', { from: location.pathname })} />
      <MenuItem text="Players" onClick={() => history.push('/list/players', { from: location.pathname })} />
      <MenuItem text="Umpires" onClick={() => history.push('/list/umpires', { from: location.pathname })} />
      <MenuItem text="Games" onClick={() => history.push('/list/games', { from: location.pathname })} />
    </Menu>
  );
};

export default ListsMenuPopover;
