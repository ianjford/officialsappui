import React, { Component } from 'react';
import Select from 'react-select';
import '../../node_modules/react-select/dist/react-select.css';
import './ComboBoxSearch.css';

class ComboBoxSearch extends Component {
  state = {
    selectedOption: this.props.selectedOption || '',
    mobile: this.props.mobile
  };

  componentWillReceiveProps(nextProps){
    if(this.props !== nextProps){
      this.setState({ mobile: nextProps.mobile, selectedOption: nextProps.selectedOption });
    }
  }

  handleOnChange = selectedOption => {
    this.props.onItemChange(selectedOption);
    this.setState({ mobile: this.state.mobile, selectedOption });
  };

  renderOption = option => {
    return (
      <div className="select-option-custom-container" aria-selected="true" role="option" aria-label={option.label}>
        <span className="option-name">{option.label}</span>
        <span>{`(L${option.level})`}</span>
      </div>
    );
  };

  renderValue = option => {
    return (
      <div className="Select-value">
        <span className="Select-value-label" role="option" aria-selected="true">{`${option.label}  (L${
          option.level
        })`}</span>
      </div>
    );
  };

  render() {
    const { selectedOption, mobile } = this.state;
    const value = selectedOption && selectedOption.value;
    return (
      <div>
        {mobile ? (
          <div className="mobile-comboBoxSearch-container">
            <Select
              name="comboBoxSearch"
              value={value}
              onChange={this.handleOnChange}
              options={this.props.items}
              optionRenderer={this.renderOption}
              valueRenderer={this.renderValue}
              searchable={true}
              placeholder={this.props.placeholder}
            />
          </div>
        ) : (
          <div className="comboBoxSearch-container">
            <Select
              name="comboBoxSearch"
              value={value}
              onChange={this.handleOnChange}
              options={this.props.items}
              optionRenderer={this.renderOption}
              valueRenderer={this.renderValue}
              searchable={true}
              placeholder={this.props.placeholder}
            />
          </div>
        )}
      </div>
    );
  }
}

export default ComboBoxSearch;
