import React, { Component } from 'react';
import format from 'date-fns/format';
import './Mentees.css';

class Mentees extends Component {
  componentDidMount() {
    const { mentees, getUmpsNextGame } = this.props;
    if (
      mentees.mentee1 !== null &&
      (mentees.fetchingNextGame.mentee1 !== 'Loading' || mentees.fetchingNextGame.mentee1 !== 'Loaded')
    ) {
      getUmpsNextGame(mentees.mentee1, 'mentee1');
    }
    if (
      mentees.mentee2 !== null &&
      (mentees.fetchingNextGame.mentee2 !== 'Loading' || mentees.fetchingNextGame.mentee2 !== 'Loaded')
    ) {
      getUmpsNextGame(mentees.mentee2, 'mentee2');
    }
    if (
      mentees.mentee3 !== null &&
      (mentees.fetchingNextGame.mentee3 !== 'Loading' || mentees.fetchingNextGame.mentee3 !== 'Loaded')
    ) {
      getUmpsNextGame(mentees.mentee3, 'mentee3');
    }
    if (
      mentees.mentee4 !== null &&
      (mentees.fetchingNextGame.mentee4 !== 'Loading' || mentees.fetchingNextGame.mentee4 !== 'Loaded')
    ) {
      getUmpsNextGame(mentees.mentee4, 'mentee4');
    }
    if (
      mentees.mentee5 !== null &&
      (mentees.fetchingNextGame.mentee5 !== 'Loading' || mentees.fetchingNextGame.mentee5 !== 'Loaded')
    ) {
      getUmpsNextGame(mentees.mentee5, 'mentee5');
    }
  }

  render() {
    const { mentees, umpires, mentorMenteeInfoStatus } = this.props;
    const mentee1Class = mentees.mentee1 === null ? 'hide' : '';
    const mentee2Class = mentees.mentee2 === null ? 'hide' : '';
    const mentee3Class = mentees.mentee3 === null ? 'hide' : '';
    const mentee4Class = mentees.mentee4 === null ? 'hide' : '';
    const mentee5Class = mentees.mentee5 === null ? 'hide' : '';
    return (
      <div className="mentees-container">
        <div className="header">
          <h1>My Mentees</h1>
        </div>
        {mentorMenteeInfoStatus === 'Loaded' ? (
          <div className="mentees-info-container">
            <div className={`col ${mentee1Class}`}>
              <div className="flex-row">
                <h3>
                  {mentees.mentee1 !== null && umpires.hasOwnProperty(mentees.mentee1)
                    ? umpires[mentees.mentee1].name
                    : ''}
                </h3>
              </div>
              <div className="mentee-contact-info flex-row">
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-phone" />
                    {mentees.mentee1 !== null && umpires.hasOwnProperty(mentees.mentee1)
                      ? umpires[mentees.mentee1].phoneNumber
                      : ''}
                  </h4>
                </div>
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-envelope" />
                    {mentees.mentee1 !== null && umpires.hasOwnProperty(mentees.mentee1)
                      ? umpires[mentees.mentee1].email
                      : ''}
                  </h4>
                </div>
              </div>
              <div className="flex-row">
                <h4>
                  {mentees.nextGame.mentee1 === null || mentees.nextGame.mentee1.gameId === null
                    ? 'Next Game: NA'
                    : `Next Game: ${format(mentees.nextGame.mentee1.startDate, 'M/D h:mm a')} @ ${
                        mentees.nextGame.mentee1.facilityName
                      }`}
                </h4>
              </div>
            </div>
            <div className={`col ${mentee2Class}`}>
              <div className="flex-row">
                <h3>
                  {mentees.mentee2 !== null && umpires.hasOwnProperty(mentees.mentee2)
                    ? umpires[mentees.mentee2].name
                    : ''}
                </h3>
              </div>
              <div className="mentee-contact-info flex-row">
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-phone" />
                    {mentees.mentee2 !== null && umpires.hasOwnProperty(mentees.mentee2)
                      ? umpires[mentees.mentee2].phoneNumber
                      : ''}
                  </h4>
                </div>
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-envelope" />
                    {mentees.mentee2 !== null && umpires.hasOwnProperty(mentees.mentee2)
                      ? umpires[mentees.mentee2].email
                      : ''}
                  </h4>
                </div>
              </div>
              <div className="flex-row">
                <h4>
                  {mentees.nextGame.mentee2 === null || mentees.nextGame.mentee2.gameId === null
                    ? 'Next Game: NA'
                    : `Next Game: ${format(mentees.nextGame.mentee2.startDate, 'M/D h:mm a')} @ ${
                        mentees.nextGame.mentee2.facilityName
                      }`}
                </h4>
              </div>
            </div>
            <div className={`col ${mentee3Class}`}>
              <div className="flex-row">
                <h3>
                  {mentees.mentee3 !== null && umpires.hasOwnProperty(mentees.mentee3)
                    ? umpires[mentees.mentee3].name
                    : ''}
                </h3>
              </div>
              <div className="mentee-contact-info flex-row">
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-phone" />
                    {mentees.mentee3 !== null && umpires.hasOwnProperty(mentees.mentee3)
                      ? umpires[mentees.mentee3].phoneNumber
                      : ''}
                  </h4>
                </div>
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-envelope" />
                    {mentees.mentee3 !== null && umpires.hasOwnProperty(mentees.mentee3)
                      ? umpires[mentees.mentee3].email
                      : ''}
                  </h4>
                </div>
              </div>
              <div className="flex-row">
                <h4>
                  {mentees.nextGame.mentee3 === null || mentees.nextGame.mentee3.gameId === null
                    ? 'Next Game: NA'
                    : `Next Game: ${format(mentees.nextGame.mentee3.startDate, 'M/D h:mm a')} @ ${
                        mentees.nextGame.mentee3.facilityName
                      }`}
                </h4>
              </div>
            </div>
            <div className={`col ${mentee4Class}`}>
              <div className="flex-row">
                <h3>
                  {mentees.mentee4 !== null && umpires.hasOwnProperty(mentees.mentee4)
                    ? umpires[mentees.mentee4].name
                    : ''}
                </h3>
              </div>
              <div className="mentee-contact-info flex-row">
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-phone" />
                    {mentees.mentee4 !== null && umpires.hasOwnProperty(mentees.mentee4)
                      ? umpires[mentees.mentee4].phoneNumber
                      : ''}
                  </h4>
                </div>
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-envelope" />
                    {mentees.mentee4 !== null && umpires.hasOwnProperty(mentees.mentee4)
                      ? umpires[mentees.mentee4].email
                      : ''}
                  </h4>
                </div>
              </div>
              <div className="flex-row">
                <h4>
                  {mentees.nextGame.mentee4 === null || mentees.nextGame.mentee4.gameId === null
                    ? 'Next Game: NA'
                    : `Next Game: ${format(mentees.nextGame.mentee4.startDate, 'M/D h:mm a')} @ ${
                        mentees.nextGame.mentee4.facilityName
                      }`}
                </h4>
              </div>
            </div>
            <div className={`col ${mentee5Class}`}>
              <div className="flex-row">
                <h3>
                  {mentees.mentee5 !== null && umpires.hasOwnProperty(mentees.mentee5)
                    ? umpires[mentees.mentee5].name
                    : ''}
                </h3>
              </div>
              <div className="mentee-contact-info flex-row">
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-phone" />
                    {mentees.mentee5 !== null && umpires.hasOwnProperty(mentees.mentee5)
                      ? umpires[mentees.mentee5].phoneNumber
                      : ''}
                  </h4>
                </div>
                <div className="sub-col">
                  <h4>
                    <span className="pt-icon-standard pt-icon-envelope" />
                    {mentees.mentee5 !== null && umpires.hasOwnProperty(mentees.mentee5)
                      ? umpires[mentees.mentee5].email
                      : ''}
                  </h4>
                </div>
              </div>
              <div className="flex-row">
                <h4>
                  {mentees.nextGame.mentee5 === null || mentees.nextGame.mentee5.gameId === null
                    ? 'Next Game: NA'
                    : `Next Game: ${format(mentees.nextGame.mentee5.startDate, 'M/D h:mm a')} @ ${
                        mentees.nextGame.mentee5.facilityName
                      }`}
                </h4>
              </div>
            </div>
          </div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default Mentees;
