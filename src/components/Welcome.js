import React, { Component } from 'react';
//import { requestAddCoachToTeam } from '../actions/api';
import './Welcome.css';
import logoBlueCrew from './../assets/logoBlueCrew.png';

class Welcome extends Component {
  state = {
    season: null,
    division: null,
    team: null,
    error: null
  };

  componentDidMount() {
    this.props.getSeasonsByActive(true);
    this.props.getDivisions();
  }

  handleSelects = event => {
    const { id, value } = event.target;
    this.setState(
      (prevState, props) => ({ [id]: value }),
      this.loadTeams(this.divisionSelect.value, this.seasonSelect.value)
    );
  };

  loadTeams = (divisionId, seasonId) => {
    if (divisionId !== null && seasonId !== null) {
      this.props.getTeams(divisionId, seasonId);
    }
  };

  handleSubmit = () => {
    const { teamsList, addCoachToTeam } = this.props;
    if (this.seasonSelect.value !== null && this.teamSelect.value !== null) {
      addCoachToTeam(this.seasonSelect.value, this.teamSelect.value, teamsList[this.teamSelect.value]);
      this.props.history.push('/', { from: '/welcome' });
    }
  };

  render() {
    const { userName, userType, seasonsList, seasons, divisionsList, divisions, teamsList, teams } = this.props;
    return (
      <div className="welcome-container">
        <div className="welcome-container-home">
          <div className="welcome-login-header">
            <img src={logoBlueCrew} height="auto" width="200" alt="Kodaro logo" />
          </div>
          <div className="welcome-header">
            <h3>{`Congratulations ${userName}!`}</h3>
            <h3>Welcome to the PHBA Blue Crew App</h3>
          </div>
          <div>
            {userType === 'umpire' ? (
              <p>
                Thank you for you for signing up to be an Umpire. After you receive your approval email, please sign
                back into the app to schedule your games.
              </p>
            ) : (
              <div className="selections">
                <h4>Please select the season and the team you're coaching.</h4>
                <div className="select-container">
                  <label className="pt-label pt-inline">
                    Select Season:
                    <select
                      id="season"
                      className="pt-select"
                      onChange={this.handleSelects}
                      ref={select => (this.seasonSelect = select)}
                    >
                      {seasonsList.map(seasonId => {
                        return (
                          <option value={seasonId} key={seasonId}>
                            {seasons[seasonId].name}
                          </option>
                        );
                      })}
                    </select>
                  </label>
                </div>
                <div className="select-container">
                  <label className="pt-label pt-inline">
                    Select Division:
                    <select
                      id="division"
                      className="pt-select"
                      onChange={this.handleSelects}
                      ref={select => (this.divisionSelect = select)}
                    >
                      {divisionsList.map(divisionId => {
                        return (
                          <option value={divisionId} key={divisionId}>
                            {divisions[divisionId].name}
                          </option>
                        );
                      })}
                    </select>
                  </label>
                </div>
                <div className="select-container">
                  <label className="pt-label pt-inline">
                    Select Team:
                    <select
                      id="team"
                      className="pt-select"
                      onChange={this.handleSelects}
                      ref={select => (this.teamSelect = select)}
                    >
                      {teamsList.map(teamId => {
                        if (teamId === this.state.team) {
                          return (
                            <option selected key={teamId} value={teamId}>
                              {teams[teamId].name}
                            </option>
                          );
                        } else {
                          return (
                            <option value={teamId} key={teamId}>
                              {teams[teamId].name}
                            </option>
                          );
                        }
                      })}
                    </select>
                  </label>
                </div>
                <div className="login-error">
                  <p>{this.state.error}</p>
                </div>
              </div>
            )}
            {userType === 'coach' ? (
              <div className="submit-btn-container">
                <button className="submit-button-white" onClick={this.handleSubmit}>
                  Submit
                </button>
              </div>
            ) : (
              <div />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Welcome;
