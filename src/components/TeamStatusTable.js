import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './ReportTable.css';

class TeamStatusTable extends Component {
  state = {
    filtering: false,
    filters: {
      division: ''
    },
    allDivisionsList: []
  };

  componentDidMount(){
    this.props.getDivisions();
    this.props.getAllTeamData();
  }

  componentWillReceiveProps(nextProps){
    if(nextProps !== this.props){
      let allDivisions = [];
      if(nextProps.fetchingDivisionsStatus === 'Loaded'){
        
        const { divisionsList, parentDivisionsList } = nextProps;

        for(var division in divisionsList){
          if(!allDivisions.includes(divisionsList[division]))
            allDivisions.push(divisionsList[division]);
        }

        for(var parentDivision in parentDivisionsList){
          if(!allDivisions.includes(parentDivisionsList[parentDivision]))
            allDivisions.push(parentDivisionsList[parentDivision]);
        }
      }

      if(allDivisions.length > 0){
        this.setState({ ...this.state, allDivisionsList: allDivisions });
      }
    }
  }

  getDivisionAbbreviation = divisionName =>{
    let output = '';
    switch(divisionName){
      case "Boys Instructional 4":
        output = 'BI(4)';
        break;
      case "Boys Instructional 5-6":
        output = 'BI(5-6)';
        break;
      case "Boys Intermediate 7":
        output = 'BI(7)';
        break;
      case "Boys Pioneer 8":
        output = 'BP';
        break;
      case "Boys American 9-10":
        output = 'BA';
        break;
      case "Boys National 11-12":
        output = 'BN';
        break;
      case "Boys Western 13-14":
        output = 'BW';
        break;
      case "Girls Instructional 4-6":
        output = 'GI';
        break;
      case "Girls Pioneer 7-8":
        output = 'GP';
        break;
      case "Girls American 9-10":
        output = 'GA';
        break;
      case "Girls National 11-12":
        output = 'GN';
        break;
      case "Girls Western 13-18":
        output = 'GW';
        break;
      default: 
        break; 
    }
    return output;
  }

  handleSelectChange = event => {
    const { value } = event.target;
    this.setState({ filtering: true, filters: { division: value }});
  };

  removeFilters = () => {
    this.setState({
      filtering: false,
      filters: {
        division: ''
      },
    });
  };

  // ump - gamesSignedFor (gamesCompleted)
  // total games (gamesCompleted)

  render() {
    const {
      teams,
      teamsList,
      fetchingAllTeamDataStatus,
      divisions,
      fetchingDivisionsStatus
    } = this.props;

    let outputList = [];

    if(fetchingAllTeamDataStatus === 'Loaded' && fetchingDivisionsStatus === 'Loaded'){
      if(this.state.filtering && this.state.filters.division !== 'all'){
        teamsList
        .filter(team => teams[team].division === this.state.filters.division)
        .map(team => {
          return outputList.push(team);
        });
      } else {
        teamsList
        .filter(team => teams[team].division !== "Boys Instructional 4" &&
             teams[team].division !== "Boys Instructional 5-6" &&
             teams[team].division !== "Boys Intermediate 7" &&
             teams[team].division !== "Girls Instructional 4-6")
        .map(team => {
          return outputList.push(team);
        });
      }
    }

    return (
      <div className="report-list-container-main team-report-container">
        <div className="report-list-table-header ">
          <h1 className="left">Team Status Table</h1>
          <div className="report-filter">
            <div className="table-filters-container">
              <label className="pt-label pt-inline">
                Division:
                <select
                  className="pt-input"
                  id="division"
                  value={this.state.filters.division}
                  onChange={this.handleSelectChange}
                >
                  <option key="divisions-all" value="all">
                    All Divisions
                  </option>
                  {this.state.allDivisionsList.map(division => {
                    return (
                      <option key={division} value={divisions[division].name}>
                        {divisions[division].name}
                      </option>
                    );
                  })}
                </select>
              </label>
            </div>
          </div>
        </div>
        <table className="team-report List-Table">
          <thead>
            <tr>
              <th className="large">Team</th>
              <th className="small">Umpire 1</th> 
              <th className="small">Umpire 2</th>
              <th className="small">Umpire 3</th>
              <th className="small">Umpire 4</th>
              <th className="xsmall">Committed</th>
              <th className="xsmall">Completed</th>
            </tr>
          </thead>
          <tbody>
            {outputList.map((team, index) => {
              return (
                  <tr className="row-highlighter" key={index}>
                    <td className="selection-highlighter large tableLeft">{this.getDivisionAbbreviation(teams[team].division)} {teams[team].team}</td>
                    <td className="selection-highlighter small tableLeft">{teams[team].umpire1}</td>
                    <td className="selection-highlighter small tableLeft">{teams[team].umpire2}</td>
                    <td className="selection-highlighter small tableLeft">{teams[team].umpire3}</td>
                    <td className="selection-highlighter small tableLeft">{teams[team].umpire4}</td>
                    <td className="selection-highlighter xsmall">{teams[team].gamesCommitted}</td>
                    <td className="selection-highlighter xsmall">{teams[team].gamesCompleted}</td>
                  </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default TeamStatusTable;
