import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { Dialog } from '@blueprintjs/core'; 
import './ErrorPopup.css';

class ErrorPopup extends Component{
	state = {
		errorLoaded: false,
		isOpen: false
	};

	componentDidMount() {
		const { errorLoaded } = this.state;

		if(!errorLoaded){
			this.setState({ errorLoaded: true, isOpen: this.props.error !== null });
		}
	}

	componentWillReceiveProps(nextProps){
		if(nextProps.error !== null){
			this.setState({ isOpen: true });
		}
	}

	handlePopupClose = () =>{
		this.setState({ isOpen: false });
		this.props.clearErrors();
	}

	render() {
		const { error } = this.props;
		const { errorLoaded } = this.state;

		let outputText = 'Unhandled error in server.';

		if(errorLoaded && error && error !== null && error !== {}){
			outputText = error;
			console.log(error);
		}

		return(
			<Dialog isOpen={this.state.isOpen} className="error-dialog">
				<label className="pt-label pt-inline error-content">
					{outputText}
				</label>
				<div className="dialog-btn-container close-error-dialog">
                  <button className="pt-button pt-intent-primary" onClick={this.handlePopupClose}>
                    Close
                  </button>
                </div>
			</Dialog>
		);
	}
}

export default ErrorPopup;