import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const AdminRoute = ({ component: Component, isAdmin, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        isAdmin === true ? (
          <Component isAdmin={isAdmin} {...rest} />
        ) : (
          <Redirect to={{ pathname: props.location.pathname }} />
        )
      }
    />
  );
};

export default AdminRoute;
