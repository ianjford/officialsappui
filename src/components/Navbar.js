import React from 'react';
//import { Link } from 'react-router-dom';
import Media from 'react-media';
//import { Popover, PopoverInteractionKind, Position } from '@blueprintjs/core';
//import UserMenuPopover from './UserMenuPopover';
import NavbarMobile from './NavbarMobile';
import NavbarDesktop from './NavbarDesktop';
import './Navbar.css';

const Navbar = ({ handleLogout, history, location, match, userName, userType }) => {
  return (
    <Media query="(max-width: 499px)">
      {matches =>
        matches ? (
          <NavbarMobile
            handleLogout={handleLogout}
            history={history}
            location={location}
            match={match}
            userName={userName}
            userType={userType}
          />
        ) : (
          <NavbarDesktop
            handleLogout={handleLogout}
            history={history}
            location={location}
            match={match}
            userName={userName}
            userType={userType}
          />
        )
      }
    </Media>
  );
};

export default Navbar;
