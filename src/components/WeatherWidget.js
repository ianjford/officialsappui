import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './WeatherWidget.css';

class WeatherWidget extends Component {
  state = {
    current: {
      temperature: null,
      condition: null,
      icon: null,
      wind: null
    },
    morning: {
      temperature: null,
      condition: null,
      icon: null,
      wind: null
    },
    afternoon: {
      temperature: null,
      condition: null,
      icon: null,
      wind: null
    },
    night: {
      temperature: null,
      condition: null,
      icon: null,
      wind: null
    },
    isLoading: true
  };

  componentDidMount() {
    this.props.getDailyWeatherForecast();
  }

  componentWillReceiveProps(nextProps){
    if(this.props !== nextProps){
      if(nextProps.fetchingWeatherData === 'Loaded'){
        let newState = {
          current: {
            temperature: null,
            condition: null,
            icon: null,
            wind: null
          },
          morning: {
            temperature: null,
            condition: null,
            icon: null,
            wind: null
          },
          afternoon: {
            temperature: null,
            condition: null,
            icon: null,
            wind: null
          },
          night: {
            temperature: null,
            condition: null,
            icon: null,
            wind: null
          }
        };

        const { forecastsList } = nextProps;

        forecastsList
        .filter(hour => hour.timeOfDay === 'now')
        .map(hour => {
          return(
            newState.current.temperature = hour.temp,
            newState.current.condition = hour.condition,
            newState.current.icon = hour.iconUrl.replace('http', 'https'),
            newState.current.wind = hour.wspd
          );
        });
        
        //Morning
        forecastsList
        .filter(hour => hour.timeOfDay === 'morning')
        .map(hour => {
          return (
            newState.morning.temperature = hour.temp,
            newState.morning.condition = hour.condition,
            newState.morning.icon = hour.iconUrl.replace('http', 'https'),
            newState.morning.wind = hour.wspd
          );
        });
        
        //Afternoon
        forecastsList
        .filter(hour => hour.timeOfDay === 'afternoon')
        .map(hour => {
          return (
            newState.afternoon.temperature = hour.temp,
            newState.afternoon.condition = hour.condition,
            newState.afternoon.icon = hour.iconUrl.replace('http', 'https'),
            newState.afternoon.wind = hour.wspd
          );
        });
        
        //Night
        forecastsList
        .filter(hour => hour.timeOfDay === 'night')
        .map(hour => {
          return (
            newState.night.temperature = hour.temp,
            newState.night.condition = hour.condition,
            newState.night.icon = hour.iconUrl.replace('http', 'https'),
            newState.night.wind = hour.wspd
          );
        });

        //Set state to new state obejct.
        this.setState({
          current: newState.current,
          morning: newState.morning,
          afternoon: newState.afternoon,
          night: newState.night,
          isLoading: false
        });
      }
    }
  }

  render() {
    const { isLoading, current, morning, afternoon, night } = this.state;

    if (isLoading) {
      return (
        <div className="Weather-Widget-container-main">
          <div className="Weather-Widget-container-secondary ">
            <div className="weather-widget-header">
              <h1>Weather for Pleasant Hill</h1>
              <div id="pt-skeleton" />
            </div>
            <ul id="weather-card" className="weather-cards">
              <li>
                <h5 className="pt-skeleton">Current</h5>
                <div>
                  <p className="pt-skeleton">Current Temperature&deg;</p>
                  <div>
                    <p className="pt-skeleton">current condition</p>
                  </div>
                  <p className="pt-skeleton">
                    <span>Space Space</span>
                  </p>
                  <p className="pt-skeleton">current wind mps</p>
                </div>
              </li>
              <li>
                <h5 className="pt-skeleton">Current</h5>
                <div>
                  <p className="pt-skeleton">Current Temperature&deg;</p>
                  <div>
                    <p className="pt-skeleton">current condition</p>
                  </div>
                  <p className="pt-skeleton">
                    <span>Space Space</span>
                  </p>
                  <p className="pt-skeleton">current wind mps</p>
                </div>
              </li>
              <li>
                <h5 className="pt-skeleton">Current</h5>
                <div>
                  <p className="pt-skeleton">Current Temperature&deg;</p>
                  <div>
                    <p className="pt-skeleton">current condition</p>
                  </div>
                  <p className="pt-skeleton">
                    <span>Space Space</span>
                  </p>
                  <p className="pt-skeleton">current wind mps</p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      );
    } else {
      return (
        <div className="Weather-Widget-container-main">
          <div className="Weather-Widget-container-secondary ">
            <div className="weather-widget-header">
              <h1 className="left">Weather for Pleasant Hill</h1>
              <div id="current-weather">
                <div>
                  <img id="current-weather-image" alt="current" src={current.icon} />
                </div>
                <div id="current-weather-data">
                  <p>{current.temperature}&deg;</p>
                  <p>{current.condition}</p>
                </div>
              </div>
            </div>
            <ul id="weather-card" className="weather-cards">
              <li>
                <h5>Morning</h5>
                <div>
                  <p>{morning.temperature}&deg;</p>
                  <div id="condition">
                    <p>{morning.condition}</p>
                  </div>
                  <p>
                    <span>
                      <img alt="morning" src={morning.icon} />
                    </span>
                  </p>
                </div>
              </li>
              <li>
                <h5>Afternoon</h5>
                <div>
                  <p>{afternoon.temperature}&deg;</p>
                  <div id="condition">
                    <p>{afternoon.condition}</p>
                  </div>
                  <p>
                    <span>
                      <img alt="afternoon" src={afternoon.icon} />
                    </span>
                  </p>
                </div>
              </li>
              <li>
                <h5>Night</h5>
                <div>
                  <p>{night.temperature}&deg;</p>
                  <div id="condition">
                    <p>{night.condition}</p>
                  </div>
                  <p>
                    <span>
                      <img alt="night" src={night.icon} />
                    </span>
                  </p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      );
    }
  }
}

export default WeatherWidget;
