import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Media from 'react-media';
import NavbarContainer from '../containers/NavbarContainer';
import SelectedGameMobileContainer from '../containers/SelectedGameMobileContainer';
import UmpiresListContainer from '../containers/UmpiresListContainer';
import MembersListContainer from '../containers/MembersListContainer';
import CoachesListContainer from '../containers/CoachesListContainer';
import PlayersListContainer from '../containers/PlayersListContainer';
import DailySummaryContainer from '../containers/DailySummaryContainer';
import MentorTableContainer from '../containers/MentorTableContainer';
import MentorMenteeCardContainer from '../containers/MentorMenteeCardContainer';
import ProfilePageContainer from '../containers/ProfilePageContainer';
import TeamStatusTableContainer from '../containers/TeamStatusTableContainer';
import UmpireGoalsTableContainer from '../containers/UmpireGoalsTableContainer';
import YTDSummaryContainer from '../containers/YTDSummaryContainer';
import VolunteerStatusTableContainer from '../containers/VolunteerStatusTableContainer';
import GamesListContainer from '../containers/GamesListContainer';
import WelcomeContainer from '../containers/WelcomeContainer';
import HomeContainer from '../containers/HomeContainer';
import HomeMobileContainer from '../containers/HomeMobileContainer';
import MyGamesMobileContainer from '../containers/MyGamesMobileContainer';

const Main = props => {
  const {
    umpireLevel,
    userType
  } = props;
  return (
    <div>
      <NavbarContainer />
      <Switch>
        <Route exact path="/dailySummary" render={() => <DailySummaryContainer />} />
        <Route exact path="/mentor-mentees" render={() => <MentorMenteeCardContainer />} />
        <Route exact path="/list/umpires" render={() => <UmpiresListContainer />} />
        <Route exact path="/list/members" render={() => <MembersListContainer />} />
        <Route exact path="/list/coaches" render={() => <CoachesListContainer />} />
        <Route exact path="/list/players" render={() => <PlayersListContainer />} />
        <Route exact path="/list/mentors" render={() => <MentorTableContainer />} />
        <Route exact path="/list/games" render={() => <GamesListContainer />} />
        <Route exact path="/reports/team/status" render={() => <TeamStatusTableContainer />} />
        <Route exact path="/reports/umpire/goals" render={() => <UmpireGoalsTableContainer />} />
        <Route exact path="/reports/summary" render={() => <YTDSummaryContainer />} />
        <Route exact path="/reports/volunteers" render={() => <VolunteerStatusTableContainer />} />
        <Route exact path="/profile" render={() => <ProfilePageContainer />} />
        <Route exact path="/selected-game/:gameId" render={() => <SelectedGameMobileContainer />}/>
        <Route exact path="/my-games" render={() => <MyGamesMobileContainer />}/>
        <Route exact path="/welcome" render={() => <WelcomeContainer />}/>
        <Route
          path="/"
          render={() => {
            if (userType === 'umpire' && umpireLevel < 1) {
              return <Redirect to={{ pathname: '/welcome' }} />;
            } else {
              return (
                <Media query="(max-width: 499px)">
                  {matches => matches ? <HomeMobileContainer /> : <HomeContainer />}
                </Media>
              );
            }
          }}
        />
      </Switch>
    </div>
  );
};

export default Main;
