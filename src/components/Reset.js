import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import ResetLink from './ResetLink';
import PasswordReset from './PasswordReset';
import './Reset.css';

class Reset extends Component {
  render() {
    return (
      <div className="login-container">
        <Switch>
          <Route exact path="/reset/link" render={props => <ResetLink {...props} />} />
          <Route
            path="/reset/:email/:token"
            render={props => <PasswordReset handleLogin={this.props.handleLogin} {...props} />}
          />
        </Switch>
      </div>
    );
  }
}

export default Reset;
