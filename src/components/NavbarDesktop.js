import React from 'react';
import { Link } from 'react-router-dom';
//import Media from 'react-media';
import { Popover, PopoverInteractionKind, Position } from '@blueprintjs/core';
import UserMenuPopover from './UserMenuPopover';
import ListsMenuPopOver from './ListsMenuPopOver';
import ReportsMenuPopOver from './ReportsMenuPopOver';
import './NavbarMobile.css';
import UmpirePic from './../assets/baseball-umpire.png';
import phbaLogo from './../assets/PHBA-Blue-Crew-Logo.png';

const NavbarDesktop = ({ handleLogout, history, location, match, userName, userType }) => {
  return (
    <nav className="pt-navbar pt-fixed-top">
      <div className="pt-navbar-group pt-align-left">
        <div className="pt-navbar-heading">
          <Link to="/">
            <img src={phbaLogo} height="40" width="auto" alt="Kodaro logo" />
          </Link>
        </div>
      </div>
      <div className="pt-navbar-group pt-align-right">
        {userType === 'Admin' ? (
          <div>
            <Popover
              content={<ListsMenuPopOver history={history} location={location} match={match} />}
              interactionKind={PopoverInteractionKind.CLICK}
              position={Position.BOTTOM}
            >
              <button className="pt-button pt-minimal">Lists</button>
            </Popover>
            <Popover
              content={<ReportsMenuPopOver history={history} location={location} match={match} />}
              interactionKind={PopoverInteractionKind.CLICK}
              position={Position.BOTTOM}
            >
              <button className="pt-button pt-minimal">Reports</button>
            </Popover>
          </div>
        ) : (
          <div />
        )}
        <span className="pt-navbar-divider" />
        <img src={UmpirePic} height="40" width="auto" alt="Kodaro logo" />
        <Popover
          content={<UserMenuPopover handleLogout={handleLogout} history={history} match={match} />}
          interactionKind={PopoverInteractionKind.CLICK}
          position={Position.BOTTOM}
        >
          <button className="pt-button pt-minimal" data-test="user-menu-btn">
            {`Welcome, ${userName} `}
            <span className="pt-icon-standard pt-icon-chevron-down" />
          </button>
        </Popover>
      </div>
    </nav>
  );
};

export default NavbarDesktop;

// <div>
//   <button className="pt-button pt-minimal" onClick={() => history.push('/dailySummary') }>Todays Games</button>
// </div>
