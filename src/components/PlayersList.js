import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './PlayersList.css';

class PlayersList extends Component {
  state = {
    currentSeason: null
  };

  componentDidMount() {
    const { getPlayersList, fetchingPlayersList, seasonsList } = this.props;
    if (fetchingPlayersList !== 'Loading' || fetchingPlayersList !== 'Loaded') {
      if (seasonsList.length) {
        getPlayersList(seasonsList[0]);
      } else {
        getPlayersList();
      }
    }
  }

  render() {
    return (
      <div className="report-list-container-main players-list-container">
        <div className="report-list-table-header">
          <h1 className="left">Players Table</h1>
        </div>
        <table className="players-list List-Table">
          <thead>
            <tr>
              <th className="small">Member ID</th>
              <th className="small">First Name</th>
              <th className="small">Last Name</th>
              <th className="small">Guardian 1</th>
              <th className="small">Guardian 2</th>
              <th className="large">Division</th>
              <th className="small">Team</th>
            </tr>
          </thead>
          <tbody>
            {this.props.playersList
              .filter(player => player.division !== 'Division' || player.team !== 'Team')
              .map((player, index) => {
                return (
                  <tr className="row-highlighter" key={`${player.memberId}-${index}`}>
                    <td className="selection-highlighter small">{player.memberId}</td>
                    <td className="selection-highlighter small tableLeft">{player.firstName}</td>
                    <td className="selection-highlighter small tableLeft">{player.lastName}</td>
                    <td className="selection-highlighter small tableLeft">{player.guardian1Name}</td>
                    <td className="selection-highlighter small tableLeft">{player.guardian2Name}</td>
                    <td className="selection-highlighter large tableLeft">{player.division}</td>
                    <td className="selection-highlighter small tableLeft">{player.team}</td>
                  </tr>
                );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default PlayersList;
