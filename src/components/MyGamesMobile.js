import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { format } from 'date-fns';
import './HomeMobile.css';
import ErrorPopup from './ErrorPopup';

class MyGamesMobile extends Component {
  state = {
    seasonsLoaded: false,
    coachesTeamsLoaded: false,
    defaultGamesLoaded: false,
    isUpdating: false
  };

  componentDidMount() {
    const { getUmpire, getSeasonsByActive } = this.props;

    getUmpire();
    getSeasonsByActive(true);

    let newMobile;

    let date = Date.now();
    let today = format(date, 'MM/DD/YYYY');

    if(this.props.mobile.length > 0){
      newMobile = { ...this.props.mobile, page: 'my-games' }; 
    } else {
      newMobile = { startDate: today, value: 7, page: 'my-games', showAllGames: false}
    }
    
    console.log(this.props.mobile);
    this.props.updateMobileState(newMobile);

    this.setState({ seasonsLoaded: false, coachesTeamsLoaded: false, defaultGamesLoaded: false, isUpdating: false });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      const {
        seasonsList,
        fetchingCoachTeamsStatus,
        fetchingSeasonsStatus,
        fetchingMyGamesStatus,
        memberId,
        getCoachesTeams,
        updateUmpireStatus
      } = this.props;
      const { coachesTeamsLoaded, defaultGamesLoaded } = this.state;

      let seasonsCheck = this.areSeasonsLoaded();
      //let isCoach = this.isCoach();
      let defaultGamesCheck = defaultGamesLoaded;
      let coachesTeamsCheck = coachesTeamsLoaded;

      if (!seasonsCheck)
        seasonsCheck = nextProps.fetchingSeasonsStatus === 'Loaded' || fetchingSeasonsStatus === 'Loaded';

      if (!coachesTeamsCheck) {
        coachesTeamsCheck = nextProps.fetchingCoachTeamsStatus === 'Loaded' || fetchingCoachTeamsStatus === 'Loaded';
      }

      if (!defaultGamesCheck) {
        defaultGamesCheck = fetchingMyGamesStatus !== 'Loaded' && nextProps.fetchingMyGamesStatus === 'Loaded';
      }

      //console.log(nextProps);

      const isUpdating = nextProps.fetchingMyGamesStatus === 'Loading';

      if (seasonsCheck) {
        //seasonsCheck = this.areSeasonsLoaded();
        if (!coachesTeamsCheck) {
          getCoachesTeams(memberId, seasonsList[0]);
        }

        if (!defaultGamesCheck && coachesTeamsLoaded && !isUpdating) {
          this.loadDefaultGames(nextProps.teamsCoaching);
        }
      }

      if (this.areDefaultGamesLoaded() && updateUmpireStatus !== nextProps.updateUmpireStatus) {
        this.updateMyGames();
      }

      this.setState({
        seasonsLoaded: seasonsCheck,
        coachesTeamsLoaded: coachesTeamsCheck,
        defaultGamesLoaded: defaultGamesCheck,
        isUpdating: isUpdating
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { seasonsLoaded, coachesTeamsLoaded, defaultGamesLoaded, isUpdating } = this.state;
    if (seasonsLoaded && coachesTeamsLoaded && (!defaultGamesLoaded && !isUpdating)) {
      this.loadDefaultGames(this.props.teamsCoaching);
    }
  }

  isAdmin = () => {
    return this.props.userType === 'Admin';
  };

  isUmpire = () => {
    return this.props.umpireLevel !== null;
    //return false;
  };

  isCoach = () => {
    return this.props.teamsCoaching.length > 0;
    //return true;
  };

  areDefaultGamesLoaded = () => {
    const { fetchingMyGamesStatus } = this.props;
    return fetchingMyGamesStatus === 'Loaded' || fetchingMyGamesStatus === 'Error';
  };

  areSeasonsLoaded = () => {
    const { fetchingSeasonsStatus } = this.props;
    return fetchingSeasonsStatus === 'Loaded' || fetchingSeasonsStatus === 'Error';
  };

  areTeamsCoachingLoaded = () => {
    const { fetchingCoachTeamsStatus } = this.props;
    return fetchingCoachTeamsStatus === 'Loaded' || fetchingCoachTeamsStatus === 'Error';
  };

  loadDefaultGames = teamsBeingCoached => {
    const { seasonsList, memberId, getMyGames } = this.props;

    const isUmpire = this.isUmpire();
    //const isAdmin = this.isAdmin();
    const isCoach = this.isCoach();
    let myTeams = '';
    if (teamsBeingCoached.length > 0) {
      for (let i = 0; i < teamsBeingCoached.length; i++) {
        if (i === teamsBeingCoached.length - 1) myTeams += teamsBeingCoached[i].teamId;
        else myTeams += teamsBeingCoached[i].teamId + ',';
      }
    }
    const nullValue = '';

    let seasonId = seasonsList[0];
    let teamIds = isCoach ? myTeams : nullValue;
    let umpireId = isUmpire ? memberId : nullValue;
    let today = format(Date.now(), 'MM/DD/YYYY');

    getMyGames(seasonId, teamIds, umpireId, today);
  };

  updateMyGames = () => {
    const { seasonsList, memberId, getUpdatedMyGames, teamsCoaching } = this.props;

    const isUmpire = this.isUmpire();
    //const isAdmin = this.isAdmin();
    const isCoach = this.isCoach();
    let myTeams = '';
    if (teamsCoaching.length > 0) {
      for (let i = 0; i < teamsCoaching.length; i++) {
        if (i === teamsCoaching.length - 1) myTeams += teamsCoaching[i].teamId;
        else myTeams += teamsCoaching[i].teamId + ',';
      }
    }
    const nullValue = '';

    let seasonId = seasonsList[0];
    let teamIds = isCoach ? myTeams : nullValue;
    let umpireId = isUmpire ? memberId : nullValue;

    getUpdatedMyGames(seasonId, teamIds, umpireId);
  };

  checkForUmpire(object) {
    const maxUmps = object.umpAvailable !== null ? object.umpAvailable : 2;
    if (maxUmps <= object.umpsUsed) {
      return 'mobile-row-blue';
    } else if (object.umpsUsed > 0) {
      return 'mobile-row-yellow';
    } else {
      return 'mobile-row-red';
    }
  }

  calculateRowDate(object) {
    let objectDate = new Date(format(object.startDate, 'MM/DD/YYYY'));
    let daysOfWeek = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    let weekDay = daysOfWeek[objectDate.getUTCDay()];
    let numberDay = objectDate.getUTCDate();

    return (
      <div className="row-date">
        <h2>{numberDay}</h2>
        <h4>{weekDay}</h4>
      </div>
    );
  }

  checkGameDates = (gameId, games) => {
    let id = gameId;
    return (
      <div className="mobile-row-container" key={games[id].id}>
        {this.calculateRowDate(games[id])}
        <div className={this.checkForUmpire(games[id])} key={`upcomingGames-${id}`}>
          <div className="mobile-game-info-container" id={id}>
            <div className="sub-row">
              <Link to={`/selected-game/${games[id].id}`}>
                <h3>{`${format(games[id].startDate, 'h:mm a')} @ ${games[id].facilityName}`}</h3>
                <h4>
                  <span>{`${games[id].divisionName}`}</span>
                  {`${games[id].homeTeamName} (H) vs ${games[id].awayTeamName} (A)`}
                </h4>
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const {
      myGamesList,
      games,
    } = this.props;

    console.log(this.props.mobile);

    return (
      <div className="home-mobile-container">
        <div className="form-container-my-upcoming-games">
          <div className="section-heading">
            <div className="section-title">
              <h2 className="left">MY GAMES</h2>
            </div>
          </div>
          <div className="mobile-games-container">
            {!this.state.isUpdating && this.state.defaultGamesLoaded && this.areDefaultGamesLoaded() ? (
              <div>
                {myGamesList.length > 0 && Object.values(games).length > 0 ? (
                  <div>
                    {myGamesList.map((id, index) => {
                      return <div key={games[id].id}>{this.checkGameDates(id, games)}</div>;
                    })}
                  </div>
                ) : (
                  <div className="mobile-empty-table"> Not assigned to any games. </div>
                )}
              </div>
            ) : (
              <div className="mobile-empty-table"> Loading Games... </div>
            )}
          </div>
        </div>
        {this.props.error !== null ? (
          <div>
            <ErrorPopup error={this.props.error} clearErrors={this.props.clearErrors} />
          </div>
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default MyGamesMobile;
