import React, { Component } from "react";
//import { Link } from "react-router-dom";
import "./DailySummary.css";

class DailySummary extends Component {
  state = {};

  componentDidMount() {
    const { fetchingDailySummary, getDailySummary } = this.props;
    if (
      fetchingDailySummary !== "Loading" ||
      fetchingDailySummary !== "Loaded"
    ) {
      getDailySummary();
    }
  }

  render() {
    const { dailySummary } = this.props;
    return (
      <div className="daily-list-container report-list-container-main">
        <div className="report-list-table-header">
          <h1 className="left">Daily Summary</h1>
        </div>
        <table className="daily-list List-Table">
          <thead>
            <tr>
              <th>Time</th>
              <th>Division</th>
              <th>Game</th>
              <th>Home Plate</th>
              <th>1st Base</th>
              <th>2nd Base</th>
              <th>3rd Base</th>
            </tr>
          </thead>
          <tbody>
            {dailySummary.map(game => {
              return (
                <tr className="row-highlighter" key={game.gameId}>
                  <td className="selection-highlighter">{game.startDate}</td>
                  <td className="selection-highlighter">{game.division}</td>
                  <td className="selection-highlighter tableLeft">{`${
                    game.homeTeamName
                  } vs ${game.awayTeamName}`}</td>
                  <td className="selection-highlighter tableLeft">
                    {game.plate}
                  </td>
                  <td className="selection-highlighter tableLeft">
                    {game.base1}
                  </td>
                  <td className="selection-highlighter tableLeft">
                    {game.base2}
                  </td>
                  <td className="selection-highlighter tableLeft">
                    {game.base3}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default DailySummary;
