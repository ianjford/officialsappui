import React, { Component } from 'react';
import { Dialog } from '@blueprintjs/core';
import Media from 'react-media';
import findIndex from 'lodash.findindex';
import SortableCardList from './SortableCardList';
import SortableCardListMobile from './SortableCardListMobile';
import './ProfilePage.css';

class ProfilePage extends Component {
  state = {
    isOpen: false,
    dialogOpenFor: null,
    volunteerList: [],
    coachList: []
  };

  componentDidMount() {
    const {
      divisionsList,
      memberId,
      seasonsList,
      teamsVolunteering,
      getCoachesTeams,
      getTeams,
      getTeamsVolunteering,
      getDivisions, 
      teamsCoaching
    } = this.props;
    getDivisions();

    if (seasonsList.length) {
      getCoachesTeams(memberId, seasonsList[0]);
      getTeamsVolunteering(memberId, seasonsList[0]);
    } else {
      //if the seasonList hasn't been loaded from the server, send null as second arg to let server get current season
      getCoachesTeams(memberId, null);
      getTeamsVolunteering(memberId, null);
    }
    if (divisionsList.length) {
      getTeams(divisionsList[0], seasonsList[0]);
    }
    this.setState({ volunteerList: teamsVolunteering, coachList: teamsCoaching });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ volunteerList: nextProps.teamsVolunteering, coachList: nextProps.teamsCoaching });
  }

  handleNewList = newList => {
    this.setState({ ...this.state, volunteerList: newList });
    const { memberId, updateTeamVolunteer } = this.props;
    updateTeamVolunteer(memberId, null, null, 'teamPriority', newList, 'update');
  };

  toggleDialog = (event, sectionThatOpenedDialog) => {
    event.preventDefault();
    this.setState({ isOpen: !this.state.isOpen, dialogOpenFor: sectionThatOpenedDialog });
  };

  handleDivisionSelect = event => {
    const { value } = event.target;
    this.props.getTeams(value, this.props.seasonsList[0]);
    this.setState({ ...this.state, selectedDivision: value });
  };

  handleTeamSelect = event => {
    const { value } = event.target;
    this.setState({ ...this.state, selectedTeamId: value, selectedTeam: this.props.teams[value].name });
  };

  handleGamesCommitted = (teamId, value) => {
    const { memberId, teamsVolunteering, updateTeamVolunteer } = this.props;
    const index = findIndex(teamsVolunteering, teamObj => teamObj.teamId === teamId);
    const { seasonId } = teamsVolunteering[index];
    updateTeamVolunteer(memberId, seasonId, teamId, 'gamesCommitted', value, 'update');
  };

  handleTeamPriority = (teamId, value) => {
    const { memberId, teamsVolunteering, updateTeamVolunteer } = this.props;
    const index = findIndex(teamsVolunteering, teamObj => teamObj.teamId === teamId);
    const { seasonId } = teamsVolunteering[index];
    updateTeamVolunteer(memberId, seasonId, teamId, 'teamPriority', value, 'update');
  };

  handleAddTeam = () => {
    const { dialogOpenFor } = this.state;
    const {
      addCoachToTeam,
      memberId,
      seasonsList,
      teams,
      teamsVolunteering,
      updateTeamVolunteer,
      divisions
    } = this.props;
    const initialTeamPriority = teamsVolunteering.length + 1;
    if (dialogOpenFor === 'volunteer') {
      this.setState({
        ...this.state,
        volunteerList: [
          ...this.state.volunteerList,
          {
            teamId: this.teamSelect.value,
            seasonId: seasonsList[0],
            teamName: teams[this.teamSelect.value].name,
            gamesCommitted: 1,
            teamPriority: initialTeamPriority
          }
        ]
      });
      updateTeamVolunteer(memberId, seasonsList[0], this.teamSelect.value, 'teamId', initialTeamPriority, 'insert');
    } else if (dialogOpenFor === 'coach') {
      const teamId = this.teamSelect.value;
      const teamName = teams[teamId].name;
      const divisionName = divisions[this.divisionSelect.value].name;

      this.setState({
        ...this.state,
        coachList: [
          ...this.state.coachList,
          {
            teamId: teamId,
            seasonId: seasonsList[0],
            teamName: teamName,
          }
        ]
      });
      addCoachToTeam(seasonsList[0], teamId, teamName, divisionName);
    }
    this.setState({ isOpen: false });
  };

  handleRemoveTeam = (team, index) => {
    const { memberId, updateTeamVolunteer } = this.props;
    const { volunteerList } = this.state;
    const newList = [...volunteerList.slice(0, index), ...volunteerList.slice(index + 1)];
    this.setState({ volunteerList: newList });
    updateTeamVolunteer(memberId, team.seasonId, team.teamId, 'teamId', newList, 'delete');
  };

  handleRemoveTeamCoaching = (team, index) => {
    const { memberId, removeCoachFromTeam, seasonsList } = this.props;
    const { coachList } = this.state;
    const newList = [...coachList.slice(0, index), ...coachList.slice(index + 1)];
    this.setState({ coachList: newList });
    removeCoachFromTeam(memberId, team.teamId, seasonsList.length ? seasonsList[0] : null);
  };

  render() {
    const { divisions, divisionsList, parentDivisionsList, name, email, phone, teams, teamsCoaching, teamsList } = this.props;
    const { volunteerList, coachList } = this.state;

    let allDivisionsList = divisionsList.concat(parentDivisionsList);

    console.log(this.state);

    return (
      <div className="user-profile-container">
        <div className="profile-container">
          <div className="card">
            <div className="header">
              <h1>Profile</h1>
            </div>
            <div className="contact-info-container">
              <h3>{name}</h3>
              <div className="phone-email-container">
                <h4>
                  <span className="pt-icon-standard pt-icon-phone" />
                  {phone}
                </h4>
                <h4>
                  <span className="pt-icon-standard pt-icon-envelope" />
                  {email}
                </h4>
              </div>
            </div>
          </div>
        </div>
        <div className="profile-container">
          <div className="card">
            <div className="header">
              <h1>Teams Represented</h1>
              <div className="btn-container">
                <button
                  className="pt-button pt-minimal pt-icon-plus"
                  onClick={e => this.toggleDialog(e, 'volunteer')}
                />
              </div>
            </div>
            <div className="volunteer-container">
              {volunteerList.length === 0 ? (
                <h4>You are not assigned as a volunteer</h4>
              ) : (
                <Media query="(hover: none)">
                  {matches =>
                    matches ? (
                      <SortableCardListMobile
                        list={volunteerList}
                        handleNewList={this.handleNewList}
                        handleGamesCommitted={this.handleGamesCommitted}
                        handleRemoveTeam={this.handleRemoveTeam}
                      />
                    ) : (
                      <SortableCardList
                        list={volunteerList}
                        handleNewList={this.handleNewList}
                        handleGamesCommitted={this.handleGamesCommitted}
                        handleRemoveTeam={this.handleRemoveTeam}
                      />
                    )
                  }
                </Media>
              )}
            </div>
          </div>
        </div>
        <div className="profile-container">
          <div className="card">
            <div className="header">
              <h1>Teams Coaching</h1>
              <div className="btn-container">
                <button 
                  className="pt-button pt-minimal pt-icon-plus" 
                  onClick={e => this.toggleDialog(e, 'coach')} 
                />
              </div>
            </div>
            <div className="coaching-container">
              {teamsCoaching.length === 0 ? (
                <h4 className="no-coach-teams">You are not assigned as a coach</h4>
              ) : (
                coachList.map((team, teamsCoachingIndex) => {
                  return (
                    <div className="coach-team" key={team.teamId}>
                      <div className="coach-team-details">
                        <h4>{team.teamName}</h4>
                        <h5>{team.divisionName}</h5>
                      </div>
                      <div className="team-coach-btn-container">
                        <button
                          className="pt-button pt-intent-danger pt-icon-trash"
                          onClick={() => this.handleRemoveTeamCoaching(team, teamsCoachingIndex)}
                        />
                      </div>
                    </div>
                  );
                })
              )}
            </div>
          </div>
        </div>
        <Dialog className="profile-dialog" isOpen={this.state.isOpen} onClose={this.toggleDialog} title="Select Team">
          <div className="pt-dialog-body">
            <label className="pt-label">
              Select Division:
              <select
                name="divisions"
                id="divisions"
                className="pt-select"
                onChange={this.handleDivisionSelect}
                ref={select => (this.divisionSelect = select)}
              >
                {this.state.dialogOpenFor === 'volunteer' ? 
                  allDivisionsList.map(divisionId => {
                    return (
                      <option id={divisionId} key={divisionId} value={divisionId}>
                        {divisions[divisionId].name}
                      </option>
                    );
                  })
                :
                  divisionsList.map(divisionId => {
                    return (
                      <option id={divisionId} key={divisionId} value={divisionId}>
                        {divisions[divisionId].name}
                      </option>
                    );
                  })
                }
              </select>
            </label>
            <label className="pt-label">
              Select Team:
              <select
                name="teams"
                id="teams"
                className="pt-select"
                onChange={this.handleTeamSelect}
                ref={select => (this.teamSelect = select)}
              >
                {teamsList.map(teamId => {
                  return (
                    <option id={teamId} key={teamId} value={teamId}>
                      {teams[teamId].name}
                    </option>
                  );
                })}
              </select>
            </label>
            <div className="dialog-btn-container">
              <button className="pt-button pt-intent-primary" onClick={this.handleAddTeam}>
                Add Team
              </button>
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default ProfilePage;
