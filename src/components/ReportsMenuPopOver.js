import React from 'react';
import { Menu, MenuItem } from '@blueprintjs/core';

const ReportMenuPopover = ({ history, location, match }) => {
  return (
    <Menu className="report-menu">
      <MenuItem text="Team Status" onClick={() => history.push('/reports/team/status', { from: location.pathname })} />
      <MenuItem
        text="Umpire Statistics"
        onClick={() => history.push('/reports/umpire/goals', { from: location.pathname })}
      />
      <MenuItem
        text="Volunteer Status"
        onClick={() => history.push('/reports/volunteers', { from: location.pathname })}
      />
      <MenuItem text="YTD Summary" onClick={() => history.push('/reports/summary', { from: location.pathname })} />
    </Menu>
  );
};

export default ReportMenuPopover;
