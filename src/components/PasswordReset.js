import React, { Component } from 'react';
import './PasswordReset.css';
import logoBlueCrew from './../assets/logoBlueCrew.png';

class PasswordReset extends Component {
  state = {
    email: '',
    password1: '',
    password2: '',
    token: '',
    error: ''
  };

  handleKeyPress = event => {
    if (event.key === 'Enter') {
      this.handleSubmit(event);
    }
  };

  handlePassword = event => {
    const { id, value } = event.target;
    if (this.state.error !== '') {
      this.setState((prevState, props) => ({ [id]: value, error: '' }));
    }
    this.setState({ [id]: value });
  };

  handleSubmit = event => {
    event.preventDefault();
    const { email, password1, password2, token } = this.state;
    if (password1 !== password2) {
      this.setState((prevState, props) => ({ error: 'Passwords do not match.' }));
      return;
    } else if (password1 === '' || password2 === '') {
      this.setState((prevState, props) => ({ error: 'Please enter and confirm password.' }));
      return;
    }
    fetch('/api/user/update/password', {
      method: 'post',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email,
        password: password1,
        resetPasswordToken: token
      })
    })
      .then(res => res.json())
      .then(jsonRes => {
        const { token, user, data } = jsonRes;
        if (data) {
          window.localStorage.setItem('kodaro-token', token);
          this.props.handleLogin(token, user, data);
          this.props.history.push('/', { from: '/login' });
          return;
        } else {
          this.props.history.push('/login'); //TODO: refactor to better handle empty data object
        }
      })
      .catch(error => {
        this.setState({ error: 'Unable to reset password. Please retry or contact Kodaro support.' });
      });
  };

  componentWillMount() {
    const { email, token } = this.props.match.params;
    this.setState({ email, token });
  }

  componentDidMount() {
    if (this.password1Input) {
      this.password1Input.select();
    }
  }

  render() {
    //const passwordClass = this.state.error !== '' ? 'pt-intent-danger' : '';
    return (
      // <div className="reset-password-container">
      <div className="form-container">
        <div className="reset-password-header">
          <img src={logoBlueCrew} height="auto" width="200" alt="Kodaro logo" />
          <p>Enter and comfirm your new password below.</p>
        </div>
        <div>
          <input
            className="login-inputs"
            type="text"
            ref={input => (this.emailInput = input)}
            value={this.state.email}
            placeholder="Email"
            readOnly
          />
        </div>
        <div>
          <input
            className="login-inputs"
            id="password1"
            type="password"
            ref={input => (this.password1Input = input)}
            onChange={this.handlePassword}
            onKeyPress={this.handleKeyPress}
            value={this.state.password1}
            placeholder="Enter New Password"
          />
        </div>
        <div>
          <input
            className="login-inputs"
            id="password2"
            type="password"
            ref={input => (this.password2Input = input)}
            onChange={this.handlePassword}
            onKeyPress={this.handleKeyPress}
            value={this.state.password2}
            placeholder="Confirm New Password"
          />
        </div>
        <div className="reset-password-error">
          <p>{this.state.error}</p>
        </div>
        <div className="reset-password-button">
          <button className="submit-button-blue" onClick={this.handleSubmit}>
            Reset Password
          </button>
        </div>
      </div>
      //</div>
    );
  }
}

export default PasswordReset;
