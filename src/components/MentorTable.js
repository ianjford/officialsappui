import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import ComboBoxSearch from './ComboBoxSearch';
import './MentorTable.css';

class MentorTable extends Component {
  state = {
    currentSeason: null,
    selectedUmp: {},
    error: ''
  };

  componentDidMount() {
    const {
      fetchingMentorsList,
      getMentorsList,
      seasonsList,
      fetchingUmpireList,
      getUmpInfo
    } = this.props;
    if (fetchingMentorsList !== 'Loading' || fetchingMentorsList !== 'Loaded') {
      if (seasonsList.length) {
        getMentorsList(seasonsList[0]);
      } else {
        getMentorsList(null);
      }
    }
    if (fetchingUmpireList === '' || fetchingUmpireList === 'Error') {
      getUmpInfo();
    }
    if (seasonsList.length) {
      this.setState({ ...this.state, currentSeason: seasonsList[0] });
    }
  }

  handleSeasonChange = e => {
    const { value } = e.target;
    const { seasonsList, seasons, getMentorsList } = this.props;
    let selectedSeasonId;
    seasonsList.forEach(seasonId => {
      if (seasons[seasonId].name === value) {
        selectedSeasonId = seasons[seasonId].seasonId;
      }
    });

    this.setState({ currentSeason: selectedSeasonId });
    getMentorsList(selectedSeasonId);
  };

  handleSelectUmp = (selectedUmp, mentorId, column) => {
    if (selectedUmp) {
      const { value, label } = selectedUmp;
      this.setState((prevState, props) => ({
        selectedUmp: {
          [column]: {
            name: label,
            menteeId: value,
            mentorId,
            column
          }
        }
      }));
    } else {
      this.setState({ selectedUmp: { [column]: null } });
    }
  };

  handleAssignMentee = (mentorId, column) => {
    if (this.state.selectedUmp[column] !== null) {
      const { menteeId } = this.state.selectedUmp[column];
      this.props.updateMentorsMentees(mentorId, menteeId, column, this.state.currentSeason, 'add');
    } else {
      this.setState({ error: 'Please select an umpire.' });
    }
  };

  handleRemoveMentee = (mentorId, menteeId, column) => {
    this.props.updateMentorsMentees(mentorId, menteeId, column, this.state.currentSeason, 'remove');
  };

  render() {
    const { mentorsList, seasonsList, seasons, umpires, umpiresList } = this.props;
    return (
      <div className="mentors-list-container report-list-container-main">
        <div className="report-list-table-header">
          <h1 className="left">Mentor Table</h1>
          <div className="mentors-list-season-container">
            <h3>Season:</h3>
            <select className="pt-select" onChange={this.handleSeasonChange}>
              {seasonsList.map(seasonId => {
                return (
                  <option id={seasonId} key={seasonId}>
                    {seasons[seasonId].name}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <table className="mentors-list List-Table">
          <thead>
            <tr>
              <th className="small">Mentor</th>
              <th className="large">Mentee 1</th>
              <th className="large">Mentee 2</th>
              <th className="large">Mentee 3</th>
              <th className="large">Mentee 4</th>
              <th className="large">Mentee 5</th>
            </tr>
          </thead>
          <tbody>
            {mentorsList.map(mentor => {
              return (
                <tr className="row-highlighter" key={mentor.memberId}>
                  <td className="selection-highlighter small tableLeft mentor">{umpires[mentor.memberId].name}</td>
                  <td className="selection-highlighter large">
                    {mentor.mentee1Id === null ? (
                      <div className="mentee-add-container">
                        <ComboBoxSearch
                          placeholder="Select umpire"
                          items={umpiresList}
                          onItemChange={selectedOption =>
                            this.handleSelectUmp(selectedOption, mentor.memberId, 'mentee1')
                          }
                        />
                        <button
                          className="pt-button pt-intent-primary pt-icon-plus"
                          onClick={() => this.handleAssignMentee(mentor.memberId, 'mentee1')}
                        />
                      </div>
                    ) : (
                      <div className="mentee-remove-container">
                        <span>{umpires[mentor.mentee1Id].name}</span>
                        <button
                          className="pt-button pt-intent-danger pt-icon-trash"
                          onClick={() => this.handleRemoveMentee(mentor.memberId, mentor.mentee1Id, 'mentee1')}
                        />
                      </div>
                    )}
                  </td>
                  <td className="selection-highlighter large">
                    {mentor.mentee2Id === null ? (
                      <div className="mentee-add-container">
                        <ComboBoxSearch
                          placeholder="Select umpire"
                          items={umpiresList}
                          onItemChange={selectedOption =>
                            this.handleSelectUmp(selectedOption, mentor.memberId, 'mentee2')
                          }
                        />
                        <button
                          className="pt-button pt-intent-primary pt-icon-plus"
                          onClick={() => this.handleAssignMentee(mentor.memberId, 'mentee2')}
                        />
                      </div>
                    ) : (
                      <div className="mentee-remove-container">
                        <span>{umpires[mentor.mentee2Id].name}</span>
                        <button
                          className="pt-button pt-intent-danger pt-icon-trash"
                          onClick={() => this.handleRemoveMentee(mentor.memberId, mentor.mentee2Id, 'mentee2')}
                        />
                      </div>
                    )}
                  </td>
                  <td className="selection-highlighter large">
                    {mentor.mentee3Id === null ? (
                      <div className="mentee-add-container">
                        <ComboBoxSearch
                          placeholder="Select umpire"
                          items={umpiresList}
                          onItemChange={selectedOption =>
                            this.handleSelectUmp(selectedOption, mentor.memberId, 'mentee3')
                          }
                        />
                        <button
                          className="pt-button pt-intent-primary pt-icon-plus"
                          onClick={() => this.handleAssignMentee(mentor.memberId, 'mentee3')}
                        />
                      </div>
                    ) : (
                      <div className="mentee-remove-container">
                        <span>{umpires[mentor.mentee3Id].name}</span>
                        <button
                          className="pt-button pt-intent-danger pt-icon-trash"
                          onClick={() => this.handleRemoveMentee(mentor.memberId, mentor.mentee3Id, 'mentee3')}
                        />
                      </div>
                    )}
                  </td>
                  <td className="selection-highlighter large">
                    {mentor.mentee4Id === null ? (
                      <div className="mentee-add-container">
                        <ComboBoxSearch
                          placeholder="Select umpire"
                          items={umpiresList}
                          onItemChange={selectedOption =>
                            this.handleSelectUmp(selectedOption, mentor.memberId, 'mentee4')
                          }
                        />
                        <button
                          className="pt-button pt-intent-primary pt-icon-plus"
                          onClick={() => this.handleAssignMentee(mentor.memberId, 'mentee4')}
                        />
                      </div>
                    ) : (
                      <div className="mentee-remove-container">
                        <span>{umpires[mentor.mentee4Id].name}</span>
                        <button
                          className="pt-button pt-intent-danger pt-icon-trash"
                          onClick={() => this.handleRemoveMentee(mentor.memberId, mentor.mentee4Id, 'mentee4')}
                        />
                      </div>
                    )}
                  </td>
                  <td className="selection-highlighter large">
                    {mentor.mentee5Id === null ? (
                      <div className="mentee-add-container">
                        <ComboBoxSearch
                          placeholder="Select umpire"
                          items={umpiresList}
                          onItemChange={selectedOption =>
                            this.handleSelectUmp(selectedOption, mentor.memberId, 'mentee5')
                          }
                        />
                        <button
                          className="pt-button pt-intent-primary pt-icon-plus"
                          onClick={() => this.handleAssignMentee(mentor.memberId, 'mentee5')}
                        />
                      </div>
                    ) : (
                      <div className="mentee-remove-container">
                        <span>{umpires[mentor.mentee5Id].name}</span>
                        <button
                          className="pt-button pt-intent-danger pt-icon-trash"
                          onClick={() => this.handleRemoveMentee(mentor.memberId, mentor.mentee5Id, 'mentee5')}
                        />
                      </div>
                    )}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default MentorTable;
