import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { Link } from 'react-router-dom';
import './MobileMenu.css';

const MobileMenu = props => {
  return (
    <div className="MobileMenu">
      <Menu isOpen={false}>
        <Link onClick={props.handleOnClick} to="/">
          Dashboard
        </Link>
        <Link onClick={props.handleOnClick} to="/my-games">
          My Games
        </Link>
        <Link onClick={props.handleOnClick} to="/mentor-mentees">
          Mentor/Mentees
        </Link>
        <Link onClick={props.handleOnClick} to="/profile">
          Profile
        </Link>
        <a onClick={props.handleLogout}>Log Out</a>
      </Menu>
    </div>
  );
};

export default MobileMenu;
