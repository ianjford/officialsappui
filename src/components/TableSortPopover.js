import React, { Component } from 'react';
import { Popover, PopoverInteractionKind, Position } from '@blueprintjs/core';
import TableSortPopoverContent from './TableSortPopoverContent';

class TableSortPopover extends Component {
  render() {
    return (
      <div>
        <Popover
          content={
            <TableSortPopoverContent
              title={this.props.title}
              dataType={this.props.dataType}
              umpireObjKey={this.props.umpireObjKey}
              handleSort={this.props.handleSort}
              boolButtonTitles={this.props.boolButtonTitles}
            />
          }
          interactionKind={PopoverInteractionKind.HOVER}
          position={Position.BOTTOM}
        >
          <button className="pt-button pt-minimal table-sort">
            {this.props.title}
            <span className="pt-icon-standard pt-icon-sort" />
          </button>
        </Popover>
      </div>
    );
  }
}

export default TableSortPopover;
