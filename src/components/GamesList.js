import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { NumericInput, Dialog } from '@blueprintjs/core';
import { DateRangePicker } from '@blueprintjs/datetime';
import format from 'date-fns/format';
import addDays from 'date-fns/add_days';
import GameUmpire from '../components/GameUmpire';
import ComboBoxSearch from './ComboBoxSearch';
import './GamesList.css';

class GamesList extends Component {
  state = {
    loadedGames: false,
    isOpen: false,
    filteringGames: false,
    filters: {
      startDate: new Date(Date.now()),
      endDate: addDays(new Date(Date.now()), 1),
      season: '',
      division: '',
      team: '',
      umpire: '',
      levelNeeded: '',
      levelAssigned: ''
    },
    selectedOption: ''
  };

  componentDidMount(){
    const {
      getSeasonsByActive,
      getDivisions,
      getTeams,
      getUmpInfo
    } = this.props;

    getSeasonsByActive(true);
    getDivisions();
    getTeams();
    getUmpInfo();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      let haveGamesLoaded = this.state.loadedGames;
      if (!haveGamesLoaded && nextProps.fetchingFilteredGamesStatus === 'Loaded') {
        haveGamesLoaded = true;
      }

      if (nextProps.fetchingSeasonsStatus === 'Loaded' &&
        !haveGamesLoaded &&
        nextProps.fetchingFilteredGamesStatus !== 'Loading' &&
         nextProps.fetchingFilteredGamesStatus !== 'Loaded' &&
         this.props.fetchingFilteredGamesStatus !== 'Loading' &&
         this.props.fetchingFilteredGamesStatus !== 'Loading'){
        //let date = format(nextProps.seasons[nextProps.seasonsList[0]].start, 'MM/DD/YYYY'); // use seasons start and end dates
        //let endDate = format(nextProps.seasons[nextProps.seasonsList[0]].finish, 'MM/DD/YYYY');
        //let nullValue = '';
        this.handleFilterGames();
      }

      this.setState(
        (prevState, props) => ({
          ...prevState,
          loadedGames: haveGamesLoaded
        })
      )
    }
  }

  handleSelectChange = event => {
    const { id, value } = event.target;
    let newValue = value === 'all' ? '' : value;
    if (id === 'division') {
      const teamValue = this.state.filters.division !== value ? '' : this.state.filters.team;
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, division: newValue, team: teamValue }
        }),
        this.handleFilterGames
      );
      this.props.getTeams(value, this.props.seasonsList[0]);
    } else {
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, [id]: newValue }
        }),
        this.handleFilterGames
      );
    }
  };

  handleFilterDate = selectedDate => {
    this.setState(
      (prevState, props) => ({
        ...prevState,
        filteringGames: true,
        filters: { ...prevState.filters, startDate: selectedDate[0], endDate: selectedDate[1] }
      }),
      this.handleFilterGames
    );
  };

  handleUmpireChange = selectedOption => {
    if (selectedOption !== null) {
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, umpire: selectedOption.value },
          selectedOption
        }),
        this.handleFilterGames
      );
    } else {
      this.setState(
        (prevState, props) => ({
          ...prevState,
          filteringGames: true,
          filters: { ...prevState.filters, umpire: '' },
          selectedOption: ''
        }),
        this.handleFilterGames
      );
    }
  };

  removeFilters = () => {
    this.setState(
      (prevState, props) => ({
        ...prevState,
        filteringGames: false,
        filters: {
          startDate:  new Date(Date.now()),
          endDate: addDays(new Date(Date.now()), 1),
          season: '',
          division: '',
          team: '',
          umpire: '',
          levelNeeded: '',
          levelAssigned: ''
        },
        selectedOption: ''
      }),
      this.handleFilterGames
    );
  };

  handleFilterGames = () => {
    let endDate =
      this.state.filters.endDate === null
        ? this.state.filters.startDate
        : this.state.filters.endDate;
    endDate = endDate === null ? null : format(endDate, 'MM/DD/YYYY');
    this.props.filterGames(
      format(this.state.filters.startDate, 'MM/DD/YYYY'),
      endDate,
      this.state.filters.season,
      this.state.filters.division,
      this.state.filters.team,
      this.state.filters.umpire,
      true,
      true
    );
  };

  handleFilterDialog = event => {
    this.setState({ ...this.state, isOpen: !this.state.isOpen });
  };

  isAdmin = () => {
    return this.props.userType === 'Admin';
  };

  handleUmpsAvailableChange = (gameId, umpsAvailable) => {
    if(umpsAvailable && umpsAvailable <= 4 && umpsAvailable >= 2){
      this.props.updateUmpAvailableForGame(gameId, umpsAvailable);
    }
  };

  getGameUmpireLevel = game => {
    let result = 5;

    switch(game.divisionId){
      case "22303":
        //"boys intstructional 4"
        break;
      case "22304":
        //"boys instructional 5-6"
        break;
      case "22305":
        //"boys intermediate 7"
        break;
      case "22306":
        //"boys pioneer 8"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10)){
          if(game.umpPlateLevel >= 3 || game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            result = 2;
          } else {
            if(game.umpBase1Level && game.umpBase1Level > 0){
              result = 3;
            } else {
              result = 1;
            }
          }
        } else {
          console.log("full");
        }
        break;
      case "22309":
        //"boys american 9-10"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10)){
          if(game.umpPlateLevel >= 3 || game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            result = 2;
          } else {
            if(game.umpBase1Level && game.umpBase1Level > 0){
              result = 3;
            } else {
              result = 1;
            }
          }
        } else {
          console.log("full");
        }
        break;
      case "22310":
        //"boys national 11-12"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10) && 
          ((game.umpPlateLevel === null || game.umpPlateLevel === 'null') || 
            (game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3))){
          result = 3;
        } else {
          result = 4;
        }
        break;
      case "22311":
        //"boys western 13-14"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10) && (game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3)){
          result = 3;
        } else {
          result = 4;
        }
        break;
      case "22312":
        //"boys majors 15-18"
        break;
      case "22315":
        //"girls instructional 4-6"
        break;
      case "22316":
        //"girls pioneer 7-8"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10)){
          if(game.umpPlateLevel >= 3 || game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            result = 2;
          } else {
                        console.log(game.umpBase1Level && game.umpBase1Level > 0);

            if(game.umpBase1Level && game.umpBase1Level > 0){
              result = 3;
            } else {
              result = 1;
            }
          }
        } else {
          console.log("full");
        }
        break;
      case "22317":
        //"girls american 9-10"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10)){
          if(game.umpPlateLevel >= 3 || game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3){
            result = 2;
          } else {
            console.log(game.umpBase1Level && game.umpBase1Level > 0);
            if(game.umpBase1Level && game.umpBase1Level > 0){
              result = 3;
            } else {
              result = 1;
            }
          }
        } else {
          console.log("full");
        }
        break;
      case "22318":
        //"girls national 11-12"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10) && 
          ((game.umpPlateLevel === null || game.umpPlateLevel === 'null') || 
            (game.umpBase1Level >= 3 || game.umpBase2Level >= 3 || game.umpBase3Level >= 3))){
          result = 3;
        } else {
          result = 4;
        }
        break;
      case "22319":
        //"girls western 13-18"
        if(game.umpAvailable > parseInt(game.umpsUsed, 10) && game.umpPlateLevel >= 3){
          result = 3;
        } else {
          result = 4;
        }
        break;
      default:
        break;
    }
    return result;
  }

  render() {
    const {
      filteredGamesList,
      games,
      umpiresFetchStatus,
      fetchingFilteredGamesStatus,
      fetchingSeasonsStatus,
      fetchingDivisionsStatus,
      fetchingTeamsStatus,
      umpires,
      umpiresList,
      memberId,
      seasons,
      seasonsList,
      divisions,
      divisionsList,
      teams,
      teamsList
    } = this.props;

    let outputList = [];

    if(this.state.filters.levelAssigned !== '' && this.state.filters.levelNeeded !== ''){
      filteredGamesList
      .filter(game => (this.state.filters.levelAssigned === games[game].umpPlateLevel ||
          this.state.filters.levelAssigned === games[game].umpBase1Level ||
          this.state.filters.levelAssigned === games[game].umpBase2Level ||
          this.state.filters.levelAssigned === games[game].umpBase3Level) &&
          (this.getGameUmpireLevel(games[game]) <= this.state.filters.levelNeeded))
      .map((game) => {
        return outputList.push(game);
      });
    } else if(this.state.filters.levelAssigned !== '' && this.state.filters.levelNeeded === ''){
      filteredGamesList
      .filter(game => this.state.filters.levelAssigned === games[game].umpPlateLevel ||
          this.state.filters.levelAssigned === games[game].umpBase1Level ||
          this.state.filters.levelAssigned === games[game].umpBase2Level ||
          this.state.filters.levelAssigned === games[game].umpBase3Level)
      .map((game) => {
        return outputList.push(game);
      });
    } else if(this.state.filters.levelAssigned === '' && this.state.filters.levelNeeded !== ''){
      filteredGamesList
      .filter(game => this.getGameUmpireLevel(games[game]) <= this.state.filters.levelNeeded)
      .map((game) => {
        return outputList.push(game);
      });
    } else {
      outputList = filteredGamesList;
    }

    return (
      <div className="games-list-container report-list-container-main">
        <div className="report-list-table-header">
          <h1 className="left">Games Table</h1>
           <div className="filter-objects">
            <div className="game-filters-btn">
              <button className="pt-button" onClick={this.handleFilterDialog}>
                Filters
              </button>
            </div>
            <Dialog
              className="games-filter-dialog"
              isOpen={this.state.isOpen}
              title="Game Filters"
              onClose={this.handleFilterDialog}
            >
              <div className="game-filters-container">
                <label className="pt-label pt-inline date-range">
                  Select Date:
                  <DateRangePicker
                    format="MM/DD/YYYY"
                    allowSingleDayRange={true}
                    closeOnSelection={true}
                    value={[this.state.filters.startDate, this.state.filters.endDate]}
                    onChange={this.handleFilterDate}
                    shortcuts={false}
                  />
                </label>
                <label className="pt-label pt-inline">
                  Season:
                  <select
                    className="pt-input"
                    id="season"
                    value={this.state.filters.season}
                    onChange={this.handleSelectChange}
                  >
                    <option key="seasons-all" value="all">
                      All Seasons
                    </option>
                    {seasonsList.map(seasonId => {
                      return (
                        <option key={seasonId} value={seasonId}>
                          {seasons[seasonId].name}
                        </option>
                      );
                    })}
                  </select>
                </label>
                <label className="pt-label pt-inline">
                  Division:
                  <select
                    className="pt-input"
                    id="division"
                    value={this.state.filters.division}
                    onChange={this.handleSelectChange}
                  >
                    <option key="divisions-all" value="all">
                      All Division
                    </option>
                    {divisionsList.map(divisionId => {
                      return (
                        <option key={divisionId} value={divisionId}>
                          {divisions[divisionId].name}
                        </option>
                      );
                    })}
                  </select>
                </label>
                <label className="pt-label pt-inline">
                  Teams:
                  <select
                    className="pt-input"
                    id="team"
                    value={this.state.filters.team}
                    onChange={this.handleSelectChange}
                  >
                    <option key="teams-all" value="all">
                      All Teams
                    </option>
                    {teamsList.map(teamId => {
                      return (
                        <option key={teamId} value={teamId}>
                          {teams[teamId].name}
                        </option>
                      );
                    })}
                  </select>
                </label>
                <label className="pt-label pt-inline umpire-label">
                  Umpire:
                  <ComboBoxSearch
                    className="umpire-select"
                    placeholder="Select umpire"
                    items={umpiresList}
                    selectedOption={this.state.selectedOption}
                    onItemChange={selectedOption => this.handleUmpireChange(selectedOption)}
                  />
                </label>
                <label className="pt-label pt-inline level-label">
                  Level Assigned:
                  <select
                    className="pt-input"
                    id="levelAssigned"
                    value={this.state.filters.levelAssigned}
                    onChange={this.handleSelectChange}
                  >
                    <option key="levels-all" value="all">
                      All Levels
                    </option>
                    <option key="levels-one" value="1">
                      1
                    </option>
                    <option key="levels-two" value="2">
                      2
                    </option> 
                    <option key="levels-three" value="3">
                      3
                    </option>
                    <option key="levels-four" value="4">
                      4
                    </option>
                  </select>
                </label>
                <label className="pt-label pt-inline level-label">
                  Level Needed:
                  <select
                    className="pt-input"
                    id="levelNeeded"
                    value={this.state.filters.levelNeeded}
                    onChange={this.handleSelectChange}
                  >
                    <option key="levels-all" value="all">
                      All Levels
                    </option>
                    <option key="levels-one" value="1">
                      1
                    </option>
                    <option key="levels-two" value="2">
                      2
                    </option> 
                    <option key="levels-three" value="3">
                      3
                    </option>
                    <option key="levels-four" value="4">
                      4
                    </option>
                  </select>
                </label>
                <div className="game-filters-clear-btn">
                  <button className="pt-button" onClick={this.removeFilters}>
                    Clear
                  </button>
                </div>
              </div>
            </Dialog>
          </div>
        </div>
        <table className="games-list List-Table">
          <thead>
            <tr>
              <th>Game Information</th>
              <th>Umpires</th>
            </tr>
          </thead>
          <tbody>
            {fetchingFilteredGamesStatus === 'Loaded' && fetchingSeasonsStatus === 'Loaded' && 
            fetchingDivisionsStatus === 'Loaded' && fetchingTeamsStatus === 'Loaded' ? 
              outputList.length > 0 ? (
                outputList.map((id, index) => {
                  return (
                    <tr className="row-highlighter" key={games[id].id}>
                      <td className="row" key={`todayGames-${id}`}>
                        <div className="game-date-location-container">
                          <h3 className="game-time">{format(games[id].startDate, 'ddd MMM D, h:mm a')}</h3>
                          <h3>{`Field: ${games[id].facilityName}`}</h3>
                          <h3>{games[id].divisionName}</h3>
                          <h3>{`${games[id].homeTeamName} v. ${games[id].awayTeamName}`}</h3> 
                          <div className="ump-available">
                            <h3>Umps Required:</h3>
                            <div className="ump-available-input">
                              <NumericInput
                                buttonPosition="none"
                                min={2}
                                max={4}
                                minorStepSize={1}
                                majorStepSize={1}
                                value={games[id].umpAvailable}
                                onValueChange={valueAsNumber => this.handleUmpsAvailableChange(games[id].id, valueAsNumber)}
                              />
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>
                        <GameUmpire
                          games={games}
                          gameId={id}
                          umpires={umpires}
                          umpiresList={umpiresList}
                          umpiresFetchStatus={umpiresFetchStatus}
                          memberId={memberId}
                          isUmpire={false}
                          isAdmin={this.isAdmin()}
                          isCoach={false}
                          assignUmpToGame={this.props.assignUmpToGame}
                          removeUmp={this.props.removeUmp}
                          allowsAssigning={true}
                        />
                      </td>
                    </tr>
                  );
                })
                ): (
                  <tr className="row-highlighter">
                    <td>No games available, try adjusting your filter.</td>
                  </tr>
                )
            :
              <tr className="row-highlighter">
                <td>Loading Games....</td>
              </tr>
            }
          </tbody>
        </table>
      </div>
    );
  }
}

export default GamesList;
