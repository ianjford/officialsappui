import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import './ReportTable.css';

class UmpireGoalsTable extends Component {
  state = {
    filtering: false,
    filters: {
      level: ''
    }
  };

  componentDidMount(){
    this.props.getAllUmpireData();
  }

  returnGamesCompletedForUmpire = (umpire) => {
    let committedGames = parseInt(umpire.volGamesCommitted, 10);
    let completedGames =  parseInt(umpire.basesCompleted, 10) +  parseInt(umpire.plateCompleted, 10);
    return committedGames + "(" + completedGames + ")";
  }

  handleSelectChange = event => {
    const { value } = event.target;
    this.setState({ filtering: true, filters: { level: value }});
  };

  removeFilters = () => {
    this.setState({
      filtering: false,
      filters: {
        level: ''
      },
    });
  };

  umpireTotals = umpire => {
    let completed = Number(umpire.plateCompleted) + Number(umpire.basesCompleted);
    let committed = Number(umpire.plateCommitted) + Number(umpire.baseCommitted);
    return `${completed} / ${committed}`;
  }

  render() {
    const {
      umpires,
      umpiresList,
      fetchingAllUmpireDataStatus,
    } = this.props;

    let outputList = [];

    if(fetchingAllUmpireDataStatus === 'Loaded'){
      if(this.state.filtering && this.state.filters.level !== 'all'){
        umpiresList
        .filter(umpire => umpires[umpire].umpireLevel === this.state.filters.level && umpires[umpire].memberId > 0)
        .map(umpire => {
          return outputList.push(umpire);
        });
      } else {
        umpiresList
        .filter(umpire => umpires[umpire].memberId > 0)
        .map(umpire => {
          return outputList.push(umpire);
        })
      }
    }

    return (
      <div className="report-list-container-main umpire-report-container">
        <div className="report-list-table-header">
          <h1 className="left">Umpire Statistics Table</h1>
          <div className="report-filter">
            <div className="table-filters-container">
              <label className="pt-label pt-inline">
                Level:
                <select
                  className="pt-input"
                  id="level"
                  value={this.state.filters.level}
                  onChange={this.handleSelectChange}
                >
                  <option key="level-all" value="all">
                    All Level
                  </option>
                  <option key="level-zero" value="0">
                    0
                  </option>
                  <option key="level-one" value="1">
                    1
                  </option>
                  <option key="level-two" value="2">
                    2
                  </option>
                  <option key="level-three" value="3">
                    3
                  </option>
                  <option key="level-four" value="4">
                    4
                  </option>
                </select>
              </label>
            </div>
          </div>
        </div>
        <table className="umpire-report List-Table">
          <thead>
            <tr>
              <th className="large">Name</th>
              <th className="xsmall">Level</th>
              <th className="xsmall">Late Drops</th>
              <th className="small">Plate <br/> Complete /<br /> Assigned</th>
              <th className="small">Base <br/> Complete /<br /> Assigned</th>
              <th className="small">Volunteer <br/> Complete /<br /> Committed</th>
              <th className="small">Total <br /> Completed</th>
            </tr>
          </thead>
          <tbody>
            {outputList.map((umpire, index) => {
              return (
                <tr className="row-highlighter" key={index}>
                  <td className="selection-highlighter large tableLeft prevent-wrapping">{umpires[umpire].firstName} {umpires[umpire].lastName}</td>
                  <td className="selection-highlighter xsmall">{umpires[umpire].umpireLevel}</td>
                  <td className="selection-highlighter xsmall">{umpires[umpire].lateDrops}</td>
                  <td className="selection-highlighter small">{umpires[umpire].plateCompleted} / {umpires[umpire].plateCommitted}</td>
                  <td className="selection-highlighter small">{umpires[umpire].basesCompleted} / {umpires[umpire].baseCommitted}</td>
                  <td className="selection-highlighter small">{umpires[umpire].volGamesCompleted} / {umpires[umpire].volGamesCommitted}</td>
                  <td className="selection-highlighter small">{this.umpireTotals(umpires[umpire])}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default UmpireGoalsTable;
