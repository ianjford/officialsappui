import React, { Component } from 'react';
import { findDOMNode } from 'react-dom';
import { DragSource, DropTarget } from 'react-dnd';
import { NumericInput } from '@blueprintjs/core';
//import flow from 'lodash.flow';

const cardSource = {
  beginDrag(props) {
    return {
      id: props.id,
      index: props.index
    };
  }
};

const cardTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    if (dragIndex === hoverIndex) {
      return;
    }

    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
    const clientOffset = monitor.getClientOffset();
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }

    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }
    props.moveCard(dragIndex, hoverIndex);
    monitor.getItem().index = hoverIndex;
  }
};

class TeamVolunteerCard extends Component {
  render() {
    const {
      connectDragSource,
      connectDropTarget,
      team,
      handleGamesCommitted,
      handleRemoveTeam
    } = this.props;
    //const opacity = isDragging ? 0 : 1;
    return connectDragSource(
      connectDropTarget(
        <div className="team-volunteer-row" key={team.teamId}>
          <div className="team-volunteer-btn-container">
            <span className="pt-icon-large pt-icon-drag-handle-vertical" />
          </div>
          <label className="pt-label team-name">
            Team:
            <input type="text" className="pt-input" value={`${team.teamName}   (${team.shortDivisionName})`} readOnly />
          </label>
          <label className="pt-label games-committed-input">
            Games Committed:
            <NumericInput
              buttonPosition="none"
              min={0}
              max={100}
              minorStepSize={1}
              majorStepSize={1}
              value={team.gamesCommitted}
              onValueChange={valueAsNumber => handleGamesCommitted(team.teamId, valueAsNumber)}
            />
          </label>
          <div className="team-volunteer-btn-container">
            <button
              className="pt-button pt-intent-danger pt-icon-trash"
              onClick={() => handleRemoveTeam(team, this.props.index)}
            />
          </div>
        </div>
      )
    );
  }
}

TeamVolunteerCard = DragSource('card', cardSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging()
}))(TeamVolunteerCard);
TeamVolunteerCard = DropTarget('card', cardTarget, connect => ({
  connectDropTarget: connect.dropTarget()
}))(TeamVolunteerCard);

export default TeamVolunteerCard;
