import React, { Component } from 'react';
//import { Link } from 'react-router-dom';
import { Checkbox, Dialog } from '@blueprintjs/core';
import './MembersList.css';

class MembersList extends Component {
  state = {
    isOpen: false,
    selectedMember: null,
    selectedMemberIndex: null
  };
  componentDidMount() {
    const { getMembersList, fetchingMembersList } = this.props;
    if (fetchingMembersList !== 'Loading' || fetchingMembersList !== 'Loaded') {
      getMembersList();
    }
  }

  udateMemberAdmin = () => {
    let updatedMember = { ...this.state.selectedMember, isAdmin: !this.state.selectedMember.isAdmin };
    this.setState({ selectedMember: updatedMember });
  };

  updateMemberUmp = () => {
    let updatedMember = { ...this.state.selectedMember, isUmpire: !this.state.selectedMember.isUmpire };
    this.setState({ selectedMember: updatedMember });
  };

  updateMember = () => {
    const { selectedMember, selectedMemberIndex } = this.state;
    const member = this.props.membersList[selectedMemberIndex];
    if (selectedMember.isAdmin !== member.isAdmin) {
      this.props.userRoleUpdate(selectedMember.memberId, selectedMember.email, selectedMember.isAdmin);
    } else if (selectedMember.isUmpire !== member.isUmpire) {
      this.props.updateUmp(selectedMember.memberId, 0, null, selectedMember.isUmpire, null);
    }
    this.setState({ isOpen: false });
  };

  toggleDialog = event => {
    event.preventDefault();
    const { id } = event.target;
    const [index] = id.split('-');
    const selectedMember = this.props.membersList[index];
    this.setState({ isOpen: !this.state.isOpen, selectedMember, selectedMemberIndex: index });
  };

  closeDialog = event => {
    event.preventDefault();
    this.setState({ isOpen: false, selectedMember: null, selectedMemberIndex: null });
  };

  render() {
    const { isOpen, selectedMember } = this.state;
    return (
      <div className="report-list-container-main members-list-container">
        <div className="report-list-table-header">
          <h1 className="left">Member Table</h1>
        </div>
        <table className="List-Table">
          <thead>
            <tr>
              <th className="small">Member ID</th>
              <th className="small">First Name</th>
              <th className="small">Last Name</th>
              <th className="large">Email</th>
              <th className="small">Phone Number</th>
              <th className="large">Player Name's</th>
              <th className="xsmall" />
            </tr>
          </thead>
          <tbody>
            {this.props.membersList.map((member, index) => {
              return (
                <tr className="row-highlighter" key={member.memberId}>
                  <td className="selection-highlighter small">{member.memberId}</td>
                  <td className="selection-highlighter small">{member.firstName}</td>
                  <td className="selection-highlighter small">{member.lastName}</td>
                  <td className="selection-highlighter large">{member.email}</td>
                  <td className="selection-highlighter small">{member.cellPhone}</td>
                  <td className="selection-highlighter large">{member.playerNames}</td>
                  <td className="selection-highlighter xsmall">
                    <button
                      className="pt-button pt-minimal member-table-edit"
                      id={`${index}-${member.memberId}`}
                      onClick={this.toggleDialog}
                    >
                      Edit
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Dialog className="members-table-dialog" title="Edit Member" isOpen={isOpen} onClose={this.closeDialog}>
          <div className="pt-dialog-body">
            <label className="pt-label pt-inline">
              Member Id:
              <input
                type="text"
                className="pt-input"
                value={selectedMember === null ? '' : selectedMember.memberId}
                readOnly
              />
            </label>
            <label className="pt-label pt-inline">
              First Name:
              <input
                type="text"
                className="pt-input"
                value={selectedMember === null ? '' : selectedMember.firstName}
                readOnly
              />
            </label>
            <label className="pt-label pt-inline">
              Last Name:
              <input
                type="text"
                className="pt-input"
                value={selectedMember === null ? '' : selectedMember.lastName}
                readOnly
              />
            </label>
            <label className="pt-label pt-inline">
              Email:
              <input
                type="text"
                className="pt-input"
                value={selectedMember === null ? '' : selectedMember.email}
                readOnly
              />
            </label>
            <label className="pt-label pt-inline">
              Phone Number:
              <input
                type="text"
                className="pt-input"
                value={selectedMember === null ? '' : selectedMember.cellPhone}
                readOnly
              />
            </label>
            <label className="pt-label pt-inline">
              Umpire:
              <Checkbox
                id="isUmpire"
                checked={selectedMember === null ? '' : selectedMember.isUmpire}
                onChange={this.updateMemberUmp}
              />
            </label>
            <label className="pt-label pt-inline">
              Site Admin:
              <Checkbox
                id="isAdmin"
                checked={selectedMember === null ? '' : selectedMember.isAdmin}
                onChange={this.udateMemberAdmin}
              />
            </label>
            <div className="dialog-btn-container">
              <button className="pt-button" onClick={this.updateMember}>
                Save
              </button>
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

export default MembersList;