import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import configureStore from './store/index';
import rootSaga from './sagas/index';
import AppContainer from './containers/AppContainer';
import { checkJWT } from './actions/userActions';
import '@blueprintjs/core/dist/blueprint.css';
import '@blueprintjs/datetime/dist/blueprint-datetime.css';
import './index.css';
import { unregister } from './registerServiceWorker';

const store = configureStore();
store.runSaga(rootSaga);
const jwt = window.localStorage.getItem('kodaro-token');

if (jwt) {
  store.dispatch(checkJWT(jwt));
}

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <AppContainer />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

unregister();
