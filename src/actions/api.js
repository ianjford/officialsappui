const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json'
};

export const requestCheckJWT = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/user/checkJWT', {
    method: 'post',
    headers
  });
  const { status } = response;
  const { error, user, data } = await response.json();
  return { status, error, user, data };
};

export const requestUserUpdate = async (token, name, phone) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/user/update/profile', {
    method: 'post',
    headers,
    body: JSON.stringify({
      name,
      phone
    })
  });
  const { status } = response;
  const { error } = await response.json();
  return { status, error };
};

export const requestPasswordChange = async (token, password) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/user/update/password', {
    method: 'post',
    headers,
    body: JSON.stringify({
      password
    })
  });
  const { status } = response;
  const { error } = await response.json();
  return { status, error };
};

export const requestGames = async (
  token,
  numRows = 10,
  lastStartDate = null,
  lastGameId = null,
  startDate,
  limitByDate
) => {
  headers['Authorization'] = token;
  const response = await fetch(
    `/api/games?numRows=${numRows}&startDate=${startDate}&limitByDate=${limitByDate}&lastStartDate=${lastStartDate}&lastGameId=${lastGameId}`,
    {
      method: 'get',
      headers
    }
  );
  const { status } = response;
  const { error, gamesList, games } = await response.json();
  return { status, error, gamesList, games };
};

export const requestUmpInfo = async (token, memberId) => {
  headers['Authorization'] = token;
  const urlString = memberId ? `/api/umpire/info/${memberId}` : '/api/umpire/selectList';
  const response = await fetch(urlString, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, umpires, umpiresList } = await response.json();
  return { status, error, umpires, umpiresList };
};

export const requestUmpSelectList = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/umpire/selectList', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, umpires, umpiresList } = await response.json();
  return { status, error, umpires, umpiresList };
};

export const requestSeasons = async token => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/seasons`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, seasonsList, seasons } = await response.json();
  return { status, error, seasonsList, seasons };
};

export const requestSeasonsByActive = async (token, activeSeason) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/seasons/${activeSeason}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, seasonsList, seasons } = await response.json();
  return { status, error, seasonsList, seasons };
};

export const requestDivisions = async token => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/divisions`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, divisions, divisionsList, parentDivisionsList } = await response.json();
  return { status, error, divisions, divisionsList, parentDivisionsList };
};

export const requestTeams = async (token, seasonId, divisionId) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/teams?seasonId=${seasonId}&divisionId=${divisionId}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, teams, teamsList } = await response.json();
  return { status, error, teams, teamsList };
};

export const requestAddCoachToTeam = async (token, teamId, seasonId) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/coach/addTeam', {
    method: 'post',
    headers,
    body: JSON.stringify({ seasonId, teamId })
  });
  const { status } = response;
  const { error } = await response.json();
  return { status, error };
};

export const requestRemoveCoachFromTeam = async (token, memberId, teamId, seasonId) => {
  headers['Authorization'] = token;
  console.log(memberId + "memberId, " + teamId + " teamId, " + seasonId + "seasonId");
  const response = await fetch('/api/coach/removeTeam', {
    method: 'post',
    headers,
    body: JSON.stringify({ memberId, teamId, seasonId })
  });
  const { status } = response;
  const { error } = await response.json();
  return { memberId, teamId, seasonId, status, error };
};

export const requestAssignUmpToGame = async (token, umpireId, gameId, plate) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/umpire/add', {
    method: 'post',
    headers,
    body: JSON.stringify({ umpireId, gameId, plate })
  });
  const { status } = response;
  const { error, updatedGame } = await response.json();
  return { status, error, updatedGame };
};

export const requestRemoveUmpFromGame = async (token, umpireId, gameId, plate) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/umpire/remove', {
    method: 'post',
    headers,
    body: JSON.stringify({ umpireId, gameId, plate })
  });
  const { status } = response;
  const { error, updatedGame } = await response.json();
  return { status, error, updatedGame };
};

export const requestMyGames = async (token, season, team, umpire, startDate) => {
  headers['Authorization'] = token;
  const response = await fetch(
    `/api/games/myGames?season=${season}&team=${team}&umpire=${umpire}&startDate=${startDate}`,
    {
      method: 'get',
      headers
    }
  );
  const { status } = response;
  const { error, myGamesList, games } = await response.json();
  return { status, myGamesList, games, error };
};

export const requestFilteredGames = async (token, startDate, endDate, season, division, team, umpire, showAllGames, limitResults) => {
  headers['Authorization'] = token;
  const response = await fetch(
    `/api/games/filter?startDate=${startDate}&endDate=${endDate}&season=${season}&division=${division}&team=${team}&umpire=${umpire}&showAllGames=${showAllGames}&limitResults=${limitResults}`,
    {
      method: 'get',
      headers
    }
  );
  const { status } = response;
  const { error, filteredGamesList, games } = await response.json();
  return { status, filteredGamesList, games, error };
};

export const requestCoachesList = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/coach/list', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, coachesList } = await response.json();
  return { status, error, coachesList };
};

export const requestDailySummary = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/games/dailySummary', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, dailySummary } = await response.json();
  return { status, error, dailySummary };
};

export const requestUmpiresList = async (token, seasonId = null) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/umpire/list?seasonId=${seasonId}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, umpiresList, umpires } = await response.json();
  return { status, error, umpiresList, umpires };
};

export const requestUmpireUpdate = async (token, memberId, umpireLevel, isMentor, isActive, seasonId) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/umpire/update', {
    method: 'post',
    headers,
    body: JSON.stringify({
      memberId,
      umpireLevel,
      isMentor,
      isActive,
      seasonId
    })
  });
  const { status } = response;
  const res = await response.json();
  return { status, error: res.error, updatedUmp: res };
};

export const requestPlayersList = async (token, seasonId) => {
  headers['Authorization'] = token;
  const urlString = seasonId === null ? '/api/player' : `/api/player/filter/${seasonId}`;
  const response = await fetch(urlString, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, playersList } = await response.json();
  return { status, error, playersList };
};

export const requestMembersList = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/member', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, membersList, members } = await response.json();
  return { status, error, membersList, members };
};

export const requestMentorsList = async (token, seasonId) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/umpire/mentor/list?seasonId=${seasonId}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, mentorsList } = await response.json();
  return { status, error, mentorsList };
};

export const requestMenteeUpdate = async (token, mentorId, menteeId, column, seasonId, operation) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/umpire/mentees/update', {
    method: 'post',
    headers,
    body: JSON.stringify({
      mentorId,
      menteeId,
      column,
      seasonId,
      operation
    })
  });
  const { status } = response;
  const { error, updatedMentees } = await response.json();
  return { status, error, updatedMentees };
};

export const requestMentorMenteeInfo = async (token, memberId, seasonId) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/umpire/isMentorMentee/${memberId}/${seasonId}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, mentorMenteeInfo } = await response.json();
  return { status, error, mentorMenteeInfo };
};

export const requestUmpiresNextGame = async (token, memberId) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/umpire/nextGame/${memberId}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, nextGame } = await response.json();
  return { status, error, nextGame };
};

export const requestCoachesTeams = async (token, memberId, seasonId = null) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/coach/teams/${memberId}/${seasonId}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, teamsList } = await response.json();
  return { status, error, teamsList };
};

export const requestTeamsVolunteering = async (token, memberId, seasonId = null) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/teams/volunteer/${memberId}/${seasonId}`, {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, teamsList } = await response.json();
  return { status, error, teamsList };
};

export const requestTeamVolunteerUpdate = async (token, memberId, seasonId, teamId, column, value, operation) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/teams/volunteer/update', {
    method: 'post',
    headers,
    body: JSON.stringify({
      memberId,
      seasonId,
      teamId,
      column,
      value,
      operation
    })
  });
  const { status } = response;
  const { error, teamsList } = await response.json();
  return { status, error, teamsList };
};

export const requestUpdateUserRole = async (token, memberId, email, isAdmin) => {
  headers['Authorization'] = token;
  const response = await fetch('/api/user/update/role', {
    method: 'post',
    headers,
    body: JSON.stringify({
      memberId,
      email,
      isAdmin
    })
  });
  const { status } = response;
  const { error, user } = await response.json();
  return { status, error, user };
};

export const requestAllUmpireData = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/reports/getAllUmpireData', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, umpires, umpiresList } = await response.json();
  return { status, error, umpires, umpiresList };
};

export const requestAllVolunteerData = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/reports/getAllVolunteerData', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, volunteers, volunteersList } = await response.json();
  return { status, error, volunteers, volunteersList };
};

export const requestAllTeamData = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/reports/getAllTeamData', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, teams, teamsList } = await response.json();
  return { status, error, teams, teamsList };
};

export const requestYtdDataSummary = async token => {
  headers['Authorization'] = token;
  const response = await fetch('/api/reports/ytdSummaryData', {
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, seasons, seasonsList } = await response.json();
  return { status, error, seasons, seasonsList };
};

export const requestUpdateUmpAvailableForGame = async (token, gameID, umpireAvailable) => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/games/umpAvailable`, {
    method: 'post',
    headers,
    body: JSON.stringify({ gameId: gameID, umpAvailable: umpireAvailable })
  });
  const { status } = response;
  const { error, gameId, umpAvailable } = await response.json();
  return { status, error, gameId, umpAvailable };
};

export const requestDailyWeatherForecast = async token => {
  headers['Authorization'] = token;
  const response = await fetch(`/api/weather/todaysForecast/fd68d0ce-d3f5-4f2b-8b6b-8768cc7e20f7/PST`, { 
    method: 'get',
    headers
  });
  const { status } = response;
  const { error, forecasts, forecastsList } = await response.json();
  return { error, status, forecasts, forecastsList };
}