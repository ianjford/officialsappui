import * as actions from './actionConstants';

export const getUmpInfo = memberId => ({
  type: actions.GET_UMP_INFO,
  memberId
});

export const getUmpInfoSuccess = (umpires, umpiresList) => ({
  type: actions.GET_UMP_INFO_SUCCESS,
  umpires,
  umpiresList
});

export const getUmpInfoError = (memberId, error) => ({
  type: actions.GET_UMP_INFO_ERROR,
  error
});

export const getUmpiresList = seasonId => ({
  type: actions.GET_UMPIRES_LIST
});

export const getUmpiresListSuccess = (seasonId, umpiresList, umpires) => ({
  type: actions.GET_UMPIRES_LIST_SUCCESS,
  umpiresList,
  umpires
});

export const getUmpiresListError = (seasonId, error) => ({
  type: actions.GET_UMPIRES_LIST_ERROR,
  error
});

export const assignUmpToGame = (umpireId, gameId, plate) => ({
  type: actions.ASSIGN_UMP_TO_GAME,
  umpireId,
  gameId,
  plate
});

export const assignUmpToGameSuccess = (umpireId, gameId, updatedGame) => ({
  type: actions.ASSIGN_UMP_TO_GAME_SUCCESS,
  umpireId,
  gameId,
  updatedGame
});

export const assignUmpToGameError = (umpireId, gameId, plate, error, updatedGame) => ({
  type: actions.ASSIGN_UMP_TO_GAME_ERROR,
  umpireId,
  gameId,
  plate,
  error,
  updatedGame
});

export const removeUmp = (umpireId, gameId, plate) => ({
  type: actions.REMOVE_UMP_FROM_GAME,
  umpireId,
  gameId,
  plate
});

export const removeUmpSuccess = (umpireId, gameId, updatedGame) => ({
  type: actions.REMOVE_UMP_FROM_GAME_SUCCESS,
  umpireId,
  gameId,
  updatedGame
});

export const removeUmpError = (umpireId, gameId, plate, error, updatedGame) => ({
  type: actions.REMOVE_UMP_FROM_GAME_ERROR,
  umpireId,
  gameId,
  plate,
  error,
  updatedGame
});

export const updateUmp = (memberId, umpireLevel, isMentor, isActive, seasonId) => ({
  type: actions.UPDATE_UMP,
  memberId,
  umpireLevel,
  isMentor,
  isActive,
  seasonId
});

export const updateUmpSuccess = (memberId, seasonId, updatedUmp) => ({
  type: actions.UPDATE_UMP_SUCCESS,
  memberId,
  seasonId,
  updatedUmp
});

export const updateUmpError = (memberId, seasonId, error) => ({
  type: actions.UPDATE_UMP_ERROR,
  memberId,
  seasonId,
  error
});

export const updateMentorsMentees = (mentorId, menteeId, column, seasonId, operation) => ({
  type: actions.UPDATE_MENTEES,
  mentorId,
  menteeId,
  column,
  seasonId,
  operation
});

export const updateMentorsMenteesSuccess = updatedMentees => ({
  type: actions.UPDATE_MENTEES_SUCCESS,
  updatedMentees
});

export const updateMentorsMenteesError = (mentorId, error) => ({
  type: actions.UPDATE_MENTEES_ERROR,
  mentorId,
  error
});

export const getMentorMenteeInfo = (memberId, seasonId) => ({
  type: actions.GET_MENTOR_MENTEE_INFO,
  memberId,
  seasonId
});

export const getMentorMenteeInfoSuccess = mentorMenteeInfo => ({
  type: actions.GET_MENTOR_MENTEE_INFO_SUCCESS,
  mentorMenteeInfo
});

export const getMentorMenteeInfoError = error => ({
  type: actions.GET_MENTOR_MENTEE_INFO_ERROR,
  error
});

export const getUmpsNextGame = (memberId, umpType) => ({
  type: actions.GET_UMPS_NEXT_GAME,
  memberId,
  umpType
});

export const getUmpsNextGameSuccess = (memberId, nextGame, umpType) => ({
  type: actions.GET_UMPS_NEXT_GAME_SUCCESS,
  memberId,
  nextGame,
  umpType
});

export const getUmpsNextGameError = (memberId, error, umpType) => ({
  type: actions.GET_UMPS_NEXT_GAME_ERROR,
  memberId,
  error,
  umpType
});
