import * as actions from './actionConstants';

export const getTeams = (divisionId, seasonId) => ({
  type: actions.GET_TEAMS,
  divisionId,
  seasonId
});

export const getTeamsSuccess = (teamsList, teams) => ({
  type: actions.GET_TEAMS_SUCCESS,
  teamsList,
  teams
});

export const getTeamsError = error => ({
  type: actions.GET_TEAMS_ERROR,
  error
});

export const getTeamsVolunteering = (memberId, seasonId) => ({
  type: actions.GET_TEAMS_VOLUNTEERING,
  memberId,
  seasonId
});

export const getTeamsVolunteeringSuccess = teamsList => ({
  type: actions.GET_TEAMS_VOLUNTEERING_SUCCESS,
  teamsList
});

export const getTeamsVolunteeringError = error => ({
  type: actions.GET_TEAMS_VOLUNTEERING_ERROR,
  error
});

export const updateTeamVolunteer = (memberId, seasonId, teamId, column, value, operation) => ({
  type: actions.UPDATE_TEAM_VOLUNTEER,
  memberId,
  seasonId,
  teamId,
  column,
  value,
  operation
});

export const updateTeamVolunteerSuccess = teamsList => ({
  type: actions.UPDATE_TEAM_VOLUNTEER_SUCCESS,
  teamsList
});

export const updateTeamVolunteerError = error => ({
  type: actions.UPDATE_TEAM_VOLUNTEER_ERROR,
  error
});
