import * as actions from './actionConstants';

export const getAllUmpireData = () => ({
  type: actions.GET_ALL_UMPIRE_DATA,
});

export const getAllUmpireDataSuccess = (umpires, umpiresList) => ({
  type: actions.GET_ALL_UMPIRE_DATA_SUCCESS,
  umpires,
  umpiresList
});

export const getAllUmpireDataError = error => ({
  type: actions.GET_ALL_UMPIRE_DATA_ERROR,
  error
});

export const getAllVolunteerData = () => ({
  type: actions.GET_ALL_VOLUNTEER_DATA,
});

export const getAllVolunteerDataSuccess = (volunteers, volunteersList) => ({
  type: actions.GET_ALL_VOLUNTEER_DATA_SUCCESS,
  volunteers,
  volunteersList
});

export const getAllVolunteerDataError = error => ({
  type: actions.GET_ALL_VOLUNTEER_DATA_ERROR,
  error
});

export const getAllTeamData = () => ({
  type: actions.GET_ALL_TEAM_DATA,
});

export const getAllTeamDataSuccess = (teams, teamsList) => ({
  type: actions.GET_ALL_TEAM_DATA_SUCCESS,
  teams,
  teamsList
});

export const getAllTeamDataError = error => ({
  type: actions.GET_ALL_TEAM_DATA_ERROR,
  error
});

export const getYtdSummaryData = () => ({
  type: actions.GET_YTD_DATA,
});

export const getYtdSummaryDataSuccess = (seasons, seasonsList) => ({
  type: actions.GET_YTD_DATA_SUCCESS,
  seasons,
  seasonsList
});

export const getYtdSummaryDataError = error => ({
  type: actions.GET_YTD_DATA_ERROR,
  error
});

export const getDailyWeatherForecast = () => ({
  type: actions.GET_DAILY_WEATHER_FORECAST
})

export const getDailyWeatherForecastSuccess = (forecasts, forecastsList) => ({
  type: actions.GET_DAILY_WEATHER_FORECAST_SUCCESS,
  forecasts,
  forecastsList
})

export const getDailyWeatherForecastError = error => ({
  type: actions.GET_DAILY_WEATHER_FORECAST_ERROR,
  error
})