import * as actions from './actionConstants';

export const addCoachToTeam = (seasonId, teamId, teamName, divisionName) => ({
  type: actions.ADD_COACH_TO_TEAM,
  seasonId,
  teamId,
  teamName,
  divisionName
});

export const addCoachToTeamSuccess = (teamId, teamName, divisionName) => ({
  type: actions.ADD_COACH_TO_TEAM_SUCCESS,
  teamId,
  teamName,
  divisionName
});

export const addCoachToTeamError = error => ({
  type: actions.ADD_COACH_TO_TEAM_ERROR,
  error
});

export const removeCoachFromTeam = (memberId, teamId, seasonId) => ({
  type: actions.REMOVE_COACH_FROM_TEAM,
  memberId,
  teamId,
  seasonId
});

export const removeCoachFromTeamSuccess = (memberId, teamId, seasonId) => ({
  type: actions.REMOVE_COACH_FROM_TEAM_SUCCESS,
  memberId,
  teamId,
  seasonId
});

export const removeCoachFromTeamError = error => ({
  type: actions.REMOVE_COACH_FROM_TEAM_ERROR,
  error
});

export const getCoachesList = () => ({
  type: actions.GET_COACHES_LIST
});

export const getCoachesListSuccess = coachesList => ({
  type: actions.GET_COACHES_LIST_SUCCESS,
  coachesList
});

export const getCoachesListError = error => ({
  type: actions.GET_COACHES_LIST_ERROR,
  error
});

export const getCoachesTeams = (memberId, seasonId) => ({
  type: actions.GET_COACHES_TEAMS,
  memberId,
  seasonId
});

export const getCoachesTeamsSuccess = teamsList => ({
  type: actions.GET_COACHES_TEAMS_SUCCESS,
  teamsList
});

export const getCoachesTeamsError = error => ({
  type: actions.GET_COACHES_TEAMS_ERROR,
  error
});
