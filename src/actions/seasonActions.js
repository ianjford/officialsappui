import * as actions from './actionConstants';

export const getSeasons = () => ({
	type: actions.GET_SEASONS
});

export const getSeasonsSuccess = (seasonsList, seasons) => ({
	type: actions.GET_SEASONS_SUCCESS,
	seasonsList,
	seasons
});

export const getSeasonsError = error => ({
	type: actions.GET_SEASONS_ERROR,
	error
});

export const getSeasonsByActive = activeSeason => ({
	type: actions.GET_SEASONS_BY_ACTIVE,
	activeSeason
});

/*export const getSeasonsByActiveSuccess = (seasonsList, seasons) => ({
	type: actions.GET_SEASONS_BY_ACTIVE_SUCCESS,
	seasonsList,
	seasons
});

export const getSeasonsByActiveError = error => ({
	type: actions.GET_SEASONS_BY_ACTIVE_ERROR,
	error
});
*/
