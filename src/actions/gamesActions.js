import * as actions from './actionConstants';

export const getGames = (startDate, numRows, lastStartDate, lastGameId) => ({
  type: actions.GET_GAMES,
  startDate,
  numRows,
  lastStartDate,
  lastGameId
});

export const getGamesSuccess = (gamesList, games) => ({
  type: actions.GET_GAMES_SUCCESS,
  gamesList,
  games
});

export const getGamesError = error => ({
  type: actions.GET_GAMES_ERROR,
  error
});

export const getUpdatedGames = (startDate, numRows, lastStartDate, lastGameId) => ({
  type: actions.UPDATED_GAMES,
  startDate,
  numRows,
  lastStartDate,
  lastGameId
});

export const getUpdatedGamesSuccess = (gamesList, games) => ({
  type: actions.UPDATED_GAMES_SUCCESS,
  gamesList,
  games
});

export const getUpdatedMyGames = (season, team, umpire, startDate) => ({
  type: actions.UPDATED_MY_GAMES,
  season,
  team,
  umpire,
  startDate
});

export const getUpdatedMyGamesSuccess = (myGamesList, games) => ({
  type: actions.UPDATED_MY_GAMES_SUCCESS,
  myGamesList,
  games
});

export const getMyGames = (season, team, umpire, startDate) => ({
  type: actions.GET_MY_GAMES,
  season,
  team,
  umpire,
  startDate
});

export const getMyGamesSuccess = (myGamesList, games) => ({
  type: actions.GET_MY_GAMES_SUCCESS,
  myGamesList,
  games
});

export const getMyGamesError = error => ({
  type: actions.GET_MY_GAMES_ERROR,
  error
});

export const getGamesByDate = (startDate, numRows, lastStartDate, lastGameId) => ({
  type: actions.GET_GAMES_BY_DATE,
  startDate,
  numRows,
  lastStartDate,
  lastGameId
});

export const getGamesByDateSuccess = (startDate, gamesList, games) => ({
  type: actions.GET_GAMES_BY_DATE_SUCCESS,
  startDate,
  gamesList,
  games
});

export const getGamesByDateError = error => ({
  type: actions.GET_GAMES_BY_DATE_ERROR,
  error
});

export const filterGames = (startDate, endDate, season, division, team, umpire, showAllGames, limitResults) => ({
  type: actions.FILTER_GAMES,
  startDate,
  endDate,
  season,
  division,
  team,
  umpire,
  showAllGames,
  limitResults
});

export const filterGamesSuccess = (filteredGamesList, games) => ({
  type: actions.FILTER_GAMES_SUCCESS,
  filteredGamesList,
  games
});

export const filterGamesError = error => ({
  type: actions.FILTER_GAMES_ERROR,
  error
});

export const getDailySummary = () => ({
  type: actions.GET_DAILY_SUMMARY
});

export const getDailySummarySuccess = dailySummary => ({
  type: actions.GET_DAILY_SUMMARY_SUCCESS,
  dailySummary
});

export const getDailySummaryError = error => ({
  type: actions.GET_DAILY_SUMMARY_ERROR,
  error
});

export const updateUmpAvailableForGame = (gameId, umpAvailable) => ({
  type: actions.UPDATE_UMP_AVAILABLE_FOR_GAME,
  gameId,
  umpAvailable
});

export const updateUmpAvailableForGameSuccess = (gameId, umpAvailable) => ({
  type: actions.UPDATE_UMP_AVAILABLE_FOR_GAME_SUCCESS,
  gameId,
  umpAvailable
});

export const updateUmpAvailableForGameError = (error) => ({
  type: actions.UPDATE_UMP_AVAILABLE_FOR_GAME_ERROR,
  error
});
