import * as actions from './actionConstants';

export const updateMobileState = mobile => ({
  type: actions.UPDATE_MOBILE_STATE,
  mobile
});

export const updateMobileStateSuccess = mobile => ({
  type: actions.UPDATE_MOBILE_STATE_SUCCESS,
  mobile
});

export const updateMobileStateError = error => ({
  type: actions.UPDATE_MOBILE_STATE_ERROR,
  error
});