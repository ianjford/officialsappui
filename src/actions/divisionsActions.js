import * as actions from './actionConstants';

export const getDivisions = () => ({
  type: actions.GET_DIVISIONS,
});

export const getDivisionsSuccess = (divisionsList, parentDivisionsList, divisions) => ({
  type: actions.GET_DIVISIONS_SUCCESS,
  divisionsList,
  parentDivisionsList,
  divisions,
});

export const getDivisionsError = error => ({
  type: actions.GET_DIVISIONS_ERROR,
  error,
});
