import * as actions from './actionConstants';

export const userLogin = (email, password) => ({
  type: actions.USER_LOGIN,
  email,
  password
});

export const userLoginSuccess = (token, user, data) => ({
  type: actions.USER_LOGIN_SUCCESS,
  token,
  user,
  data
});

export const userLoginFailure = error => ({
  type: actions.USER_LOGIN_ERROR,
  error
});

export const userLogout = () => ({
  type: actions.USER_LOGOUT
});

export const checkJWT = token => ({
  type: actions.USER_CHECK_JWT,
  token
});

export const updateUserProfile = (name, phone) => ({
  type: actions.USER_PROFILE_UPDATE,
  name,
  phone
});

export const updateUserProfileSuccess = (name, phone) => ({
  type: actions.USER_PROFILE_UPDATE_SUCCESS,
  name,
  phone
});

export const updateUserProfileError = error => ({
  type: actions.USER_PROFILE_UPDATE_ERROR,
  error
});

export const updatePassword = password => ({
  type: actions.USER_PASSWORD_CHANGE,
  password
});

export const updatePasswordSuccess = () => ({
  type: actions.USER_PASSWORD_CHANGE_SUCCESS
});

export const updatePasswordError = error => ({
  type: actions.USER_PASSWORD_CHANGE_ERROR,
  error
});

export const userRoleUpdate = (memberId, email, isAdmin) => ({
  type: actions.UPDATE_USER_ROLE,
  memberId,
  email,
  isAdmin
});

export const userRoleUpdateSuccess = user => ({
  type: actions.UPDATE_USER_ROLE_SUCCESS,
  user
});

export const userRoleUpdateError = error => ({
  type: actions.UPDATE_USER_ROLE_ERROR,
  error
});
