import * as actions from './actionConstants';

export const clearErrors = () => ({
  type: actions.CLEAR_ERRORS,
});
