import * as actions from './actionConstants';

export const getMembersList = () => ({
  type: actions.GET_MEMBERS_LIST
});

export const getMembersListSuccess = membersList => ({
  type: actions.GET_MEMBERS_LIST_SUCCESS,
  membersList
});

export const getMembersListError = error => ({
  type: actions.GET_MEMBERS_LIST_ERROR,
  error
});

export const getPlayersList = (seasonId = null) => ({
  type: actions.GET_PLAYERS_LIST,
  seasonId
});

export const getPlayersListSuccess = playersList => ({
  type: actions.GET_PLAYERS_LIST_SUCCESS,
  playersList
});

export const getPlayersListError = error => ({
  type: actions.GET_PLAYERS_LIST_ERROR,
  error
});

export const getMentorsList = seasonId => ({
  type: actions.GET_MENTORS_LIST,
  seasonId
});

export const getMentorsListSuccess = (mentorsList, seasonId) => ({
  type: actions.GET_MENTORS_LIST_SUCCESS,
  mentorsList,
  seasonId
});

export const getMentorsListError = (error, seasonId) => ({
  type: actions.GET_MENTORS_LIST_ERROR,
  error,
  seasonId
});
