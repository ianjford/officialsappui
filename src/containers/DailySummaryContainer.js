import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getDailySummary } from "../actions/gamesActions";
import DailySummary from "../components/DailySummary";

const mapDispatchToProps = dispatch => {
  return {
    getDailySummary: () => {
      dispatch(getDailySummary());
    }
  };
};

const mapStateToProps = state => ({
  dailySummary: state.lists.dailySummary,
  fetchingDailySummary: state.lists.fetchingDailySummary
});

const DailySummaryContainer = withRouter(
  connect(mapStateToProps, mapDispatchToProps)(DailySummary)
);
export default DailySummaryContainer;
