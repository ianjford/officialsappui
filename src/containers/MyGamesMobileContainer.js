import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import MyGamesMobile from './../components/MyGamesMobile';
import {
  getMyGames,
  getUpdatedMyGames,
} from '../actions/gamesActions';
import { getUmpInfo } from '../actions/umpireActions';
import { getDivisions } from '../actions/divisionsActions';
import { getSeasonsByActive } from '../actions/seasonActions';
import { getCoachesTeams } from '../actions/coachActions';
import { clearErrors } from '../actions/errorActions';
import { updateMobileState } from '../actions/mobileActions';

const mapDispatchToProps = dispatch => {
  return {
    getMyGames: (startDate, numRows, lastStartDate, lastGameId) => {
      dispatch(getMyGames(startDate, numRows, lastStartDate, lastGameId));
    },
    getUmpire: memberId => {
      dispatch(getUmpInfo(memberId));
    },
    getDivisions: () => {
      dispatch(getDivisions());
    },
    getSeasonsByActive: activeSeason => {
      dispatch(getSeasonsByActive(activeSeason));
    },
    clearErrors: () => {
      dispatch(clearErrors());
    },
    getCoachesTeams: (memberId, seasonId) => {
      dispatch(getCoachesTeams(memberId, seasonId));
    },
    getUpdatedMyGames: (startDate, numRows, lastStartDate, lastGameId) => {
      dispatch(getUpdatedMyGames(startDate, numRows, lastStartDate, lastGameId));
    },
    updateMobileState: (mobile) =>{
      dispatch(updateMobileState(mobile));
    }
  };
};
const mapStateToProps = state => {
  return {
    divisions: state.divisions.divisions,
    divisionsList: state.divisions.divisionsList,
    fetchingDivisionsStatus: state.divisions.fetchingDivisionsStatus,
    seasons: state.seasons.seasons,
    seasonsList: state.seasons.seasonsList,
    fetchingSeasonsStatus: state.seasons.fetchingSeasonsStatus,
    games: state.games.games,
    myGamesList: state.games.myGamesList,
    fetchingMyGamesStatus: state.games.fetchingMyGamesStatus,
    userType: state.user.userType,
    memberId: state.user.memberId,
    umpires: state.umpires.umpires,
    umpiresList: state.umpires.umpiresList,
    umpiresFetchStatus: state.umpires.umpiresFetchStatus,
    error: state.errors.error,
    teamsCoaching: state.user.teamsCoaching,
    fetchingCoachTeamsStatus: state.user.fetchingCoachTeamsStatus,
    updateUmpireStatus: state.games.updateUmpireStatus,
    umpireLevel: state.user.umpireLevel,
    mobile: state.mobile.mobile
  };
};

const MyGamesMobileContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(MyGamesMobile));
export default MyGamesMobileContainer;