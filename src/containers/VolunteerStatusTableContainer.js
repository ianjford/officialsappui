import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import VolunteerStatusTable from './../components/VolunteerStatusTable';
import {
  getAllUmpireData,
  getAllVolunteerData,
} from '../actions/reportsActions';
import {
  getDivisions
} from '../actions/divisionsActions';

const mapDispatchToProps = dispatch => {
  return {
    getAllUmpireData: () => {
      dispatch(getAllUmpireData());
    },
    getAllVolunteerData: () => {
      dispatch(getAllVolunteerData());
    },
    getDivisions: () => {
      dispatch(getDivisions());
    },
  };
};

const mapStateToProps = state => ({
  umipres: state.reports.umpires,
  umpiresList: state.reports.umpiresList,
  volunteers: state.reports.volunteers,
  volunteersList: state.reports.volunteersList,
  fetchingAllUmpireDataStatus: state.reports.fetchingAllUmpireDataStatus,
  fetchingAllVolunteerDataStatus: state.reports.fetchingAllVolunteerDataStatus,
  divisions: state.divisions.divisions,
  divisionsList: state.divisions.divisionsList,
  fetchingDivisionsStatus: state.divisions.fetchingDivisionsStatus
});

const VolunteerStatusTableContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(VolunteerStatusTable));
export default VolunteerStatusTableContainer;
