import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getMentorsList } from '../actions/listActions';
import { getUmpInfo, updateMentorsMentees } from '../actions/umpireActions';
import MentorTable from '../components/MentorTable';

const mapDispatchToProps = dispatch => {
  return {
    getMentorsList: seasonId => {
      dispatch(getMentorsList(seasonId));
    },
    getUmpInfo: memberId => {
      dispatch(getUmpInfo(memberId));
    },
    updateMentorsMentees: (mentorId, menteeId, column, seasonId, operation) => {
      dispatch(updateMentorsMentees(mentorId, menteeId, column, seasonId, operation));
    }
  };
};

const mapStateToProps = state => ({
  fetchingMentorsList: state.lists.fetchingMentorsList,
  mentorsList: state.lists.mentorsList,
  seasons: state.seasons.seasons,
  seasonsList: state.seasons.seasonsList,
  umpires: state.umpires.umpires,
  umpiresList: state.umpires.umpiresList,
  fetchingUmpireList: state.umpires.fetchingStatus
});

const MentorTableContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(MentorTable));
export default MentorTableContainer;
