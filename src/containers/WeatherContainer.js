import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getDailyWeatherForecast } from '../actions/reportsActions';
import WeatherWidget from './../components/WeatherWidget';

const mapDispatchToProps = dispatch => {
  return {
    getDailyWeatherForecast: () => {
      dispatch(getDailyWeatherForecast());
    }
  };
};

const mapStateToProps = state => ({
  forecastsList: state.reports.forecastsList,
  forecasts: state.reports.forecasts,
  fetchingWeatherData: state.reports.fetchingWeatherData
});

const WeatherContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(WeatherWidget));
export default WeatherContainer;
