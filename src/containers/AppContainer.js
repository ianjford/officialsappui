import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { userLoginSuccess } from './../actions/userActions.js';
import App from './../components/App';

const mapDispatchToProps = dispatch => {
  return {
    handleLoginSuccess: (token, user, data) => {
      dispatch(userLoginSuccess(token, user, data));
    }
  };
};

const mapStateToProps = state => {
  return {
    isLoggedIn: state.user.isLoggedIn,
    checkingJWT: state.user.checkingToken
  };
};

const AppContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(App));

export default AppContainer;
