import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getPlayersList } from '../actions/listActions';
import PlayersList from '../components/PlayersList';

const mapDispatchToProps = dispatch => {
  return {
    getPlayersList: seasonId => {
      dispatch(getPlayersList(seasonId));
    }
  };
};

const mapStateToProps = state => ({
  playersList: state.lists.playersList,
  fetchingPlayersList: state.lists.fetchingPlayersList,
  seasonsList: state.seasons.seasonsList
});

const PlayersListContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(PlayersList));
export default PlayersListContainer;
