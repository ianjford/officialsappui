import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import SelectedGameMobile from '../components/SelectedGameMobile';
import { assignUmpToGame, removeUmp } from '../actions/umpireActions';
import { clearErrors } from '../actions/errorActions';

const mapDispatchToProps = dispatch => {
  return {
    assignUmpToGame: (umpireId, gameId, plate, updatedGame) => {
      dispatch(assignUmpToGame(umpireId, gameId, plate, updatedGame));
    },
    removeUmp: (umpireId, gameId, plate, updatedGame) => {
      dispatch(removeUmp(umpireId, gameId, plate, updatedGame));
    },
    clearErrors: () => {
      dispatch(clearErrors());
    }
  };
};

const mapStateToProps = (state, ownProps) => {
  return {
    memberId: state.user.memberId,
    selectedGame: state.games.games[ownProps.match.params.gameId],
    umpires: state.umpires.umpires,
    umpiresList: state.umpires.umpiresList,
    userType: state.user.userType,
    umpiresFetchStatus: state.umpires.fetchingStatus,
    games: state.games.games,
    error: state.errors.error,
    mobile: state.mobile.mobile,
  };
};

const SelectedGameMobileContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(SelectedGameMobile));
export default SelectedGameMobileContainer;
