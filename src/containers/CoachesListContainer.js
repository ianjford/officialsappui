import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getCoachesList } from '../actions/coachActions';
import CoachesList from '../components/CoachesList';

const mapDispatchToProps = dispatch => {
  return {
    getCoachesList: () => {
      dispatch(getCoachesList());
    }
  };
};

const mapStateToProps = state => ({
  coachesList: state.lists.coachesList,
  fetchingCoachesList: state.lists.fetchingCoachesList
});

const CoachesListContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(CoachesList));
export default CoachesListContainer;
