import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getUmpiresList, updateUmp } from '../actions/umpireActions';
import UmpiresList from '../components/UmpiresList';

const mapDispatchToProps = dispatch => {
  return {
    getUmpiresList: seasonId => {
      dispatch(getUmpiresList(seasonId));
    },
    updateUmp: (memberId, umpireLevel, isMentor, isActive, seasonId) => {
      dispatch(updateUmp(memberId, umpireLevel, isMentor, isActive, seasonId));
    }
  };
};

const mapStateToProps = state => ({
  seasonsList: state.seasons.seasonsList,
  seasons: state.seasons.seasons,
  umpires: state.umpires.umpires,
  umpiresList: state.lists.umpiresList,
  fetchingUmpiresList: state.lists.fetchingUmpiresList
});

const UmpiresListContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(UmpiresList));
export default UmpiresListContainer;
