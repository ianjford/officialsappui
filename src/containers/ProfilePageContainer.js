import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { addCoachToTeam, getCoachesTeams, removeCoachFromTeam } from '../actions/coachActions';
import { getTeams, getTeamsVolunteering, updateTeamVolunteer } from '../actions/teamActions';
import { getDivisions } from '../actions/divisionsActions';
import ProfilePage from '../components/ProfilePage';

const mapDispatchToProps = dispatch => {
  return {
    addCoachToTeam: (seasonId, teamId, teamName, divisionName) => {
      dispatch(addCoachToTeam(seasonId, teamId, teamName, divisionName));
    },
    getCoachesTeams: (memberId, seasonId) => {
      dispatch(getCoachesTeams(memberId, seasonId));
    },
    getTeams: (divisionId, seasonId) => {
      dispatch(getTeams(divisionId, seasonId));
    },
    getTeamsVolunteering: (memberId, seasonId) => {
      dispatch(getTeamsVolunteering(memberId, seasonId));
    },
    getDivisions: () => {
      dispatch(getDivisions());
    },
    updateTeamVolunteer: (memberId, seasonId, teamId, column, value, operation) => {
      dispatch(updateTeamVolunteer(memberId, seasonId, teamId, column, value, operation));
    },
    removeCoachFromTeam: (memberId, teamId, seasonId) => {
      dispatch(removeCoachFromTeam(memberId, teamId, seasonId));
    }
  };
};

const mapStateToProps = state => ({
  divisions: state.divisions.divisions,
  divisionsList: state.divisions.divisionsList,
  parentDivisionsList: state.divisions.parentDivisionsList,
  email: state.user.email,
  memberId: state.user.memberId,
  name: state.user.userName,
  phone: state.user.phone,
  teamsCoaching: state.user.teamsCoaching,
  teamsVolunteering: state.user.teamsVolunteering,
  seasonsList: state.seasons.seasonsList,
  teams: state.teams.teams,
  teamsList: state.teams.teamsList,
  userType: state.user.userType
});

const ProfilePageContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfilePage));
export default ProfilePageContainer;
