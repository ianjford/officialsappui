import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Main from './../components/Main';

const mapDispatchToProps = dispatch => {
  return {
    
  };
};

const mapStateToProps = state => ({
  userType: state.user.userType,
  umpireLevel: state.user.umpireLevel
});

const MainContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
export default MainContainer;
