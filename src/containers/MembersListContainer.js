import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getMembersList } from '../actions/listActions';
import { updateUmp } from '../actions/umpireActions';
import { userRoleUpdate } from '../actions/userActions';
import MembersList from '../components/MembersList';

const mapDispatchToProps = dispatch => {
  return {
    getMembersList: () => {
      dispatch(getMembersList());
    },
    updateUmp: (memberId, umpireLevel, isMentor, isActive, seasonId) => {
      dispatch(updateUmp(memberId, umpireLevel, isMentor, isActive, seasonId));
    },
    userRoleUpdate: (memberId, email, isAdmin) => {
      dispatch(userRoleUpdate(memberId, email, isAdmin));
    }
  };
};

const mapStateToProps = state => ({
  membersList: state.lists.membersList,
  fetchingMembersList: state.lists.fetchingMembersList
});

const MembersListContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(MembersList));
export default MembersListContainer;
