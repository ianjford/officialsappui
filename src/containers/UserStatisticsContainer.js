import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getAllUmpireData } from "./../actions/reportsActions";
import UserStatistics from "../components/UserStatistics";

const mapDispatchToProps = dispatch => {
  return {
    getAllUmpireData: () => {
      dispatch(getAllUmpireData());
    }
  };
};

const mapStateToProps = state => ({
  umpires: state.reports.umpires,
  umpiresList: state.reports.umpiresList,
  fetchingAllUmpireDataStatus: state.reports.fetchingAllUmpireDataStatus,
  memberId: state.user.memberId,
  umpireLevel: state.user.umpireLevel
});

const UserStatisticsContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(UserStatistics));
export default UserStatisticsContainer;
