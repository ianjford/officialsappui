import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Home from '../components/Home';
import {
  getGames,
  getMyGames,
  filterGames,
  getUpdatedGames,
  getUpdatedMyGames,
} from '../actions/gamesActions';
import { getUmpInfo, assignUmpToGame, removeUmp } from '../actions/umpireActions';
import { getDivisions } from '../actions/divisionsActions';
import { getSeasons, getSeasonsByActive } from '../actions/seasonActions';
import { getTeams } from '../actions/teamActions';
import { getCoachesTeams } from '../actions/coachActions';
import { clearErrors } from '../actions/errorActions';

const mapDispatchToProps = dispatch => {
  return {
  	getGames: (startDate, numRows, lastStartDate, lastGameId) => {
      dispatch(getGames(startDate, numRows, lastStartDate, lastGameId));
    },
    getMyGames: (season, team, umpire, startDate) => {
      dispatch(getMyGames(season, team, umpire, startDate));
    },
    filterGames: (startDate, endDate, season, division, team, umpire, showAllGames, limitResults) => {
      dispatch(filterGames(startDate, endDate, season, division, team, umpire, showAllGames, limitResults));
    },
    getUmpire: memberId => {
      dispatch(getUmpInfo(memberId));
    },
    getDivisions: () => {
      dispatch(getDivisions());
    },
    getSeasons: () => {
      dispatch(getSeasons());
    },
    getSeasonsByActive: activeSeason => {
      dispatch(getSeasonsByActive(activeSeason));
    },
    getTeams: (divisionId, seasonId) => {
      dispatch(getTeams(divisionId, seasonId));
    },
    assignUmpToGame: (umpireId, gameId, plate, updatedGame) => {
      dispatch(assignUmpToGame(umpireId, gameId, plate, updatedGame));
    },
    removeUmp: (umpireId, gameId, plate, updatedGame) => {
      dispatch(removeUmp(umpireId, gameId, plate, updatedGame));
    },
    clearErrors: () => {
      dispatch(clearErrors());
    },
    getCoachesTeams: (memberId, seasonId) => {
      dispatch(getCoachesTeams(memberId, seasonId));
    },
    getUpdatedMyGames: (season, team, umpire, startDate) => {
      dispatch(getUpdatedMyGames(season, team, umpire, startDate));
    },
    getUpdatedGames: (startDate, numRows, lastStartDate, lastGameId) => {
      dispatch(getUpdatedGames(startDate, numRows, lastStartDate, lastGameId));
    }
  };
};
const mapStateToProps = state => {
  return {
    divisions: state.divisions.divisions,
    divisionsList: state.divisions.divisionsList,
    fetchingDivisionsStatus: state.divisions.fetchingDivisionsStatus,
    seasons: state.seasons.seasons,
    seasonsList: state.seasons.seasonsList,
    fetchingSeasonsStatus: state.seasons.fetchingSeasonsStatus,
    teams: state.teams.teams,
    teamsList: state.teams.teamsList,
    fetchingTeamsStatus: state.teams.fetchingTeamsStatus,
    games: state.games.games,
    filteredGamesList: state.games.filteredGamesList,
    fetchingFilteredGamesStatus: state.games.fetchingFilteredGamesStatus,
    gamesList: state.games.gamesList,
    fetchingGamesStatus: state.games.fetchingGamesStatus,
    myGamesList: state.games.myGamesList,
    fetchingMyGamesStatus: state.games.fetchingMyGamesStatus,
    userType: state.user.userType,
    memberId: state.user.memberId,
    umpires: state.umpires.umpires,
    umpiresList: state.umpires.umpiresList,
    umpiresFetchStatus: state.umpires.umpiresFetchStatus,
    error: state.errors.error,
    teamsCoaching: state.user.teamsCoaching,
    fetchingCoachTeamsStatus: state.user.fetchingCoachTeamsStatus,
    updateUmpireStatus: state.games.updateUmpireStatus,
    umpireLevel: state.user.umpireLevel
  };
};

const HomeContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
export default HomeContainer;