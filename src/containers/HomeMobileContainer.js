import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import HomeMobile from './../components/HomeMobile';
import {
  getGames,
  filterGames,
  getUpdatedGames
} from '../actions/gamesActions';
import { getUmpInfo } from '../actions/umpireActions';
import { getDivisions } from '../actions/divisionsActions';
import { getSeasonsByActive } from '../actions/seasonActions';
import { getCoachesTeams } from '../actions/coachActions';
import { clearErrors } from '../actions/errorActions';
import { updateMobileState } from '../actions/mobileActions';

const mapDispatchToProps = dispatch => {
  return {
    getGames: (startDate, numRows, lastStartDate, lastGameId) => {
      dispatch(getGames(startDate, numRows, lastStartDate, lastGameId));
    },
    getUmpire: memberId => {
      dispatch(getUmpInfo(memberId));
    },
    filterGames: (startDate, endDate, season, division, team, umpire, showAllGames, limitResults) => {
      dispatch(filterGames(startDate, endDate, season, division, team, umpire, showAllGames, limitResults));
    },
    getDivisions: () => {
      dispatch(getDivisions());
    },
    getSeasonsByActive: activeSeason => {
      dispatch(getSeasonsByActive(activeSeason));
    },
    clearErrors: () => {
      dispatch(clearErrors());
    },
    getCoachesTeams: (memberId, seasonId) => {
      dispatch(getCoachesTeams(memberId, seasonId));
    },
    getUpdatedGames: (startDate, numRows, lastStartDate, lastGameId) => {
      dispatch(getUpdatedGames(startDate, numRows, lastStartDate, lastGameId));
    },
    updateMobileState: (mobile) =>{
      dispatch(updateMobileState(mobile));
    }
  };
};
const mapStateToProps = state => {
  return {
    divisions: state.divisions.divisions,
    divisionsList: state.divisions.divisionsList,
    fetchingDivisionsStatus: state.divisions.fetchingDivisionsStatus,
    seasons: state.seasons.seasons,
    seasonsList: state.seasons.seasonsList,
    fetchingSeasonsStatus: state.seasons.fetchingSeasonsStatus,
    games: state.games.games,
    gamesList: state.games.gamesList,
    fetchingGamesStatus: state.games.fetchingGamesStatus,
    filteredGamesList: state.games.filteredGamesList,
    fetchingFilteredGamesStatus: state.games.fetchingFilteredGamesStatus,
    userType: state.user.userType,
    memberId: state.user.memberId,
    umpires: state.umpires.umpires,
    umpiresList: state.umpires.umpiresList,
    umpiresFetchStatus: state.umpires.umpiresFetchStatus,
    error: state.errors.error,
    teamsCoaching: state.user.teamsCoaching,
    fetchingCoachTeamsStatus: state.user.fetchingCoachTeamsStatus,
    updateUmpireStatus: state.games.updateUmpireStatus,
    umpireLevel: state.user.umpireLevel,
    mobile: state.mobile.mobile
  };
};

const HomeMobileContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(HomeMobile));
export default HomeMobileContainer;