import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Welcome from '../components/Welcome';
import { getDivisions } from '../actions/divisionsActions';
import { getSeasons, getSeasonsByActive } from '../actions/seasonActions';
import { getTeams } from '../actions/teamActions';
import { addCoachToTeam, addCoachToTeamSuccess, addCoachToTeamError } from '../actions/coachActions';
import { getMyGames } from '../actions/gamesActions';

const mapDispatchToProps = dispatch => {
  return {
    getDivisions: () => {
      dispatch(getDivisions());
    },
    getSeasons: () => {
      dispatch(getSeasons());
    },
    getSeasonsByActive: activeSeason => {
      dispatch(getSeasonsByActive(activeSeason));
    },
    getMyGames: (season, team, umpire, startDate) => {
      dispatch(getMyGames(season, team, umpire, startDate));
    },
    addCoachToTeam: (seasonId, teamId) => {
      dispatch(addCoachToTeam(seasonId, teamId));
    },
    addCoachToTeamSuccess: teamId => {
      dispatch(addCoachToTeamSuccess(teamId));
    },
    addCoachToTeamError: error => {
      dispatch(addCoachToTeamError(error));
    },
    getTeams: (divisionId, seasonId) => {
      dispatch(getTeams(divisionId, seasonId));
    },
  };
};
const mapStateToProps = state => {
  return {
    myGamesList: state.games.myGamesList,
  	fetchingMyGamesStatus: state.games.fetchingMyGamesStatus,
  	divisions: state.divisions.divisions,
  	divisionsList: state.divisions.divisionsList,
  	fetchingDivisionsStatus: state.divisions.fetchingDivisionsStatus,
  	seasons: state.seasons.seasons,
  	seasonsList: state.seasons.seasonsList,
  	fetchingSeasonsStatus: state.seasons.fetchingSeasonsStatus,
  	teams: state.teams.teams,
  	teamsList: state.teams.teamsList,
  	fetchingTeamsStatus: state.teams.fetchingTeamsStatus,
  	userName: state.user.userName,
  	memberId: state.user.memberId,
  	userType: state.user.userType
  };
};

const WelcomeContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(Welcome));
export default WelcomeContainer;