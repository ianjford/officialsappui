import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import YTDSummary from './../components/YTDSummary';
import {
  getYtdSummaryData
} from '../actions/reportsActions';
import {
	getSeasonsByActive
} from '../actions/seasonActions';

const mapDispatchToProps = dispatch => {
  return {
  	getYtdSummaryData: () => {
  		dispatch(getYtdSummaryData());
  	},
  	getSeasonsByActive: activeSeason => {
      dispatch(getSeasonsByActive(activeSeason));
    }
  };
};

const mapStateToProps = state => ({
	reportSeasons: state.reports.seasons,
	reportSeasonsList: state.reports.seasonsList,
	fetchingYtdSummaryDataStatus: state.reports.fetchingYtdSummaryDataStatus,
	seasons: state.seasons.seasons,
	seasonsList: state.seasons.seasonsList,
	fetchingSeasonsStatus: state.seasons.fetchingSeasonsStatus
});

const YTDSummaryContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(YTDSummary));
export default YTDSummaryContainer;
