import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { filterGames, updateUmpAvailableForGame } from '../actions/gamesActions';
import { getSeasonsByActive } from '../actions/seasonActions';
import { getUmpInfo, assignUmpToGame, removeUmp } from '../actions/umpireActions';
import { getDivisions } from '../actions/divisionsActions';
import { getTeams } from '../actions/teamActions';
import GamesList from '../components/GamesList';

const mapDispatchToProps = dispatch => {
  return {
    filterGames: (startDate, endDate, season, division, team, umpire, showAllGames, limitResults) => {
      dispatch(filterGames(startDate, endDate, season, division, team, umpire, showAllGames, limitResults));
    },
    getSeasonsByActive: activeSeason => {
      dispatch(getSeasonsByActive(activeSeason));
    },
    updateUmpAvailableForGame: (gameId, umpAvailable) => {
      dispatch(updateUmpAvailableForGame(gameId, umpAvailable));
    },
    removeUmp: (umpireId, gameId, plate, updatedGame) => {
      dispatch(removeUmp(umpireId, gameId, plate, updatedGame));
    },
    assignUmpToGame: (umpireId, gameId, plate, updatedGame) => {
      dispatch(assignUmpToGame(umpireId, gameId, plate, updatedGame));
    },
    getUmpInfo: memberId => {
      dispatch(getUmpInfo(memberId));
    },
    getDivisions: () => {
      dispatch(getDivisions());
    },
    getTeams: (divisionId, seasonId) => {
      dispatch(getTeams(divisionId, seasonId));
    }
  };
};

const mapStateToProps = state => ({
  games: state.games.games,
  filteredGamesList: state.games.filteredGamesList,
  fetchingFilteredGamesStatus: state.games.fetchingFilteredGamesStatus,
  memberId: state.user.memberId,
  userType: state.user.userType,
  umpires: state.umpires.umpires,
  umpiresList: state.umpires.umpiresList,
  updatedGame: state.games.updatedGame,
  seasons: state.seasons.seasons,
  seasonsList: state.seasons.seasonsList,
  fetchingSeasonsStatus: state.seasons.fetchingSeasonsStatus,
  teams: state.teams.teams,
  teamsList: state.teams.teamsList,
  fetchingTeamsStatus: state.teams.fetchingTeamsStatus,
  divisions: state.divisions.divisions,
  divisionsList: state.divisions.divisionsList,
  fetchingDivisionsStatus: state.divisions.fetchingDivisionsStatus,
});

const GamesListContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(GamesList));
export default GamesListContainer;
