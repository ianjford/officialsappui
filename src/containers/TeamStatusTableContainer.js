import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import TeamStatusTable from './../components/TeamStatusTable';
import {
  getAllTeamData
} from '../actions/reportsActions';
import {
	getDivisions
} from '../actions/divisionsActions';

const mapDispatchToProps = dispatch => {
  return {
    getAllTeamData: () => {
      dispatch(getAllTeamData());
    },
    getDivisions: () => {
      dispatch(getDivisions());
    },
  };
};

const mapStateToProps = state => ({
  teams: state.reports.teams,
  teamsList: state.reports.teamsList,
  fetchingAllTeamDataStatus: state.reports.fetchingAllTeamDataStatus,
  divisions: state.divisions.divisions,
  divisionsList: state.divisions.divisionsList,
  parentDivisionsList: state.divisions.parentDivisionsList,
  fetchingDivisionsStatus: state.divisions.fetchingDivisionsStatus
});

const TeamStatusTableContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(TeamStatusTable));
export default TeamStatusTableContainer;
