import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { getMentorMenteeInfo, getUmpsNextGame } from '../actions/umpireActions';
import MentorMenteeCard from '../components/MentorMenteeCard';

const mapDispatchToProps = dispatch => {
  return {
    getMentorMenteeInfo: (memberId, seasonId) => {
      dispatch(getMentorMenteeInfo(memberId, seasonId));
    },
    getUmpsNextGame: (memberId, umpType) => {
      dispatch(getUmpsNextGame(memberId, umpType));
    }
  };
};

const mapStateToProps = state => ({
  isMentor: state.user.isMentor,
  isMentee: state.user.isMentee,
  mentorInfo: state.user.mentorInfo,
  mentees: state.user.menteesInfo,
  memberId: state.user.memberId,
  seasonsList: state.seasons.seasonsList,
  userType: state.user.userType,
  umpires: state.umpires.umpires,
  umpireFetchStatus: state.umpires.fetchingStatus,
  mentorMenteeInfoStatus: state.user.mentorMenteeInfoStatus,
});

const MentorMenteeCardContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(MentorMenteeCard));
export default MentorMenteeCardContainer;
