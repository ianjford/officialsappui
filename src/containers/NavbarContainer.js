import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { userLogout } from './../actions/userActions.js';
import Navbar from '../components/Navbar';

const mapDispatchToProps = dispatch => {
  return {
    handleLogout: () => {
      dispatch(userLogout());
    },
  };
};
const mapStateToProps = state => {
  return {
    userName: state.user.userName,
    userType: state.user.userType,
  };
};

const NavbarContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(Navbar));
export default NavbarContainer;
