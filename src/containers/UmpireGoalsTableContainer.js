import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import UmpireGoalsTable from './../components/UmpireGoalsTable';
import {
  getAllUmpireData
} from '../actions/reportsActions';

const mapDispatchToProps = dispatch => {
  return {
    getAllUmpireData: () => {
      dispatch(getAllUmpireData());
    },
  };
};

const mapStateToProps = state => ({
  umpires: state.reports.umpires,
  umpiresList: state.reports.umpiresList,
  fetchingAllUmpireDataStatus: state.reports.fetchingAllUmpireDataStatus,
});

const UmpireGoalsTableContainer = withRouter(connect(mapStateToProps, mapDispatchToProps)(UmpireGoalsTable));
export default UmpireGoalsTableContainer;
