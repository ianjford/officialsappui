describe('Tests User Auth', function() {
  const testUser = { email: 'sean.gibson@kodaro.com', password: 'pass' };

  it('Should login user, logout user, and redirect to correct pages', function() {
    cy.visit('/login');

    cy.get('[data-test="email"]').type(testUser.email);
    cy.get('[data-test="password"]').type(testUser.password);
    cy.get('[data-test="submit"]').click();

    cy
      .url()
      .should('include', '/')
      .should(() => {
        expect(localStorage.getItem('kodaro-token')).to.not.be.null;
      });

    cy.get('[data-test="user-menu-btn"]').click();
    cy.get('.logout').click();

    cy
      .url()
      .should('include', '/login')
      .should(() => {
        expect(localStorage.getItem('kodaro-token')).to.be.null;
      });
  });

  it('Should display error message if unable to login in user', function() {
    cy.visit('/login');

    cy.get('[data-test="email"]').type(testUser.email);
    cy.get('[data-test="password"]').type('notUserPassword');
    cy.get('[data-test="submit"]').click();
    cy.get('.login-error p').contains('Unable to log in. Please retry or contact PHBA for support.');
  });

  it('Should validate a user has entered an email prior and show an error msg if not', function() {
    cy.visit('/login');

    cy.get('[data-test="password"]').type(testUser.password);
    cy.get('[data-test="submit"]').click();
    cy.get('.login-error p').contains('Please enter a valid email.');
  });

  it('Should validate a user has entered a password and show an error msg if not', function() {
    cy.visit('/login');

    cy.get('[data-test="email"]').type(testUser.email);
    cy.get('[data-test="submit"]').click();
    cy.get('.login-error p').contains('Please enter a password.');
  });

  it('Should redirect user to login page if they try to access protected route', function() {
    cy.visit('/profile');

    cy.url().should('include', '/login');
  });
});
